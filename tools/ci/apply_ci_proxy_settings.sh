#
# Copyright (c) 2024 BWI GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/env bash
echo "systemProp.http.proxyHost=dl-proxy.example.com" >> $CI_PROJECT_DIR/gradle.properties
echo "systemProp.http.proxyPort=3128" >> $CI_PROJECT_DIR/gradle.properties
echo "systemProp.http.nonProxyHosts=127.0.0.1|localhost|*.example.com|10.10.65.0/24" >> $CI_PROJECT_DIR/gradle.properties
echo "systemProp.https.proxyHost=dl-proxy.example.com" >> $CI_PROJECT_DIR/gradle.properties
echo "systemProp.https.proxyPort=3128" >> $CI_PROJECT_DIR/gradle.properties
echo "systemProp.https.nonProxyHosts=127.0.0.1|localhost|*.example.com|10.10.65.0/24" >> $CI_PROJECT_DIR/gradle.properties
