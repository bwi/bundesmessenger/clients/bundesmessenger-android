#!/usr/bin/env bash
./matrix.sh --homeserver=https://matrix.org --token=$MATRIX_GITLAB_BOT_TOKEN --room=$MATRIX_GITLAB_BOT_ROOM "$1"
