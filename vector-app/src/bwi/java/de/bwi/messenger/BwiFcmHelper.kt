/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bwi.messenger

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.messaging.FirebaseMessaging
import de.bwi.messenger.configuration.AppConfiguration
import im.vector.app.core.di.ActiveSessionHolder
import im.vector.app.core.di.DefaultPreferences
import im.vector.app.core.dispatchers.CoroutineDispatchers
import im.vector.app.core.pushers.FcmHelper
import im.vector.app.core.pushers.PushersManager
import im.vector.app.fdroid.BackgroundSyncStarter
import im.vector.app.fdroid.receiver.AlarmSyncBroadcastReceiver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

/**
 * This class store the FCM token in SharedPrefs and ensure this token is retrieved.
 * It has an alter ego in the fdroid variant.
 */
class BwiFcmHelper @Inject constructor(
        @DefaultPreferences
        private val sharedPrefs: SharedPreferences,
        private val context: Context,
        private val backgroundSyncStarter: BackgroundSyncStarter,
        private val appConfiguration: AppConfiguration,
        appScope: CoroutineScope,
        private val coroutineDispatchers: CoroutineDispatchers
) : FcmHelper {

    private val scope = CoroutineScope(appScope.coroutineContext + coroutineDispatchers.io)

    companion object {
        private const val PREFS_KEY_FCM_TOKEN = "FCM_TOKEN"
    }

    /**
     * Retrieves the FCM registration token.
     *
     * @return the FCM token or null if not received from FCM
     */
    override
    fun getFcmToken(): String? {
        return sharedPrefs.getString(PREFS_KEY_FCM_TOKEN, null)
    }

    /**
     * Store FCM token to the SharedPrefs
     * TODO Store in realm
     *
     * @param token the token to store
     */
    override
    fun storeFcmToken(token: String?) {
        sharedPrefs.edit {
            putString(PREFS_KEY_FCM_TOKEN, token)
        }
    }

    override fun ensureFcmTokenIsRetrieved(pushersManager: PushersManager, registerPusher: Boolean) {
        if (isFirebaseAvailable()) {
            try {
                FirebaseMessaging.getInstance().token
                        .addOnSuccessListener { token ->
                            storeFcmToken(token)
                            if (registerPusher && appConfiguration.pusherUrl.isNotEmpty()) {
                                scope.launch {
                                    pushersManager.enqueueRegisterPusherWithFcmKey(token)
                                }
                            }
                        }
                        .addOnFailureListener { e ->
                            Timber.e(e, "## ensureFcmTokenIsRetrieved() : failed")
                        }
            } catch (e: Throwable) {
                Timber.e(e, "## ensureFcmTokenIsRetrieved() : failed")
            }
        } else {
            Timber.e("No valid Google Play Services found. Cannot use FCM.")
        }
    }

    override
    fun isFirebaseAvailable(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(context)
        return resultCode == ConnectionResult.SUCCESS
    }

    override
    fun onEnterForeground(activeSessionHolder: ActiveSessionHolder) {
        if (isFirebaseAvailable()) {
            return
        }

        // use only if push not available

        // try to stop all regardless of background mode
        activeSessionHolder.getSafeActiveSession()?.syncService()?.stopAnyBackgroundSync()
        AlarmSyncBroadcastReceiver.cancelAlarm(context)
    }

    override
    fun onEnterBackground(activeSessionHolder: ActiveSessionHolder) {
        if (isFirebaseAvailable()) {
            return
        }

        // use only if push not available and there is an active session
        activeSessionHolder.getSafeActiveSession()?.let {
            backgroundSyncStarter.start(activeSessionHolder)
        }
    }
}
