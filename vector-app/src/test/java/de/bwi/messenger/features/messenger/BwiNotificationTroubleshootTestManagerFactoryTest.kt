/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.messenger

import androidx.fragment.app.Fragment
import de.bwi.messenger.BwiNotificationTroubleshootTestManagerFactory
import de.bwi.messenger.features.settings.troubleshoot.TestNotificationTimesSettings
import im.vector.app.core.di.ActiveSessionHolder
import im.vector.app.core.pushers.FcmHelper
import im.vector.app.core.pushers.UnifiedPushHelper
import im.vector.app.fdroid.features.settings.troubleshoot.TestAutoStartBoot
import im.vector.app.fdroid.features.settings.troubleshoot.TestBackgroundRestrictions
import im.vector.app.fdroid.features.settings.troubleshoot.TestBatteryOptimization
import im.vector.app.features.VectorFeatures
import im.vector.app.features.settings.troubleshoot.TestAccountSettings
import im.vector.app.features.settings.troubleshoot.TestAvailableUnifiedPushDistributors
import im.vector.app.features.settings.troubleshoot.TestCurrentUnifiedPushDistributor
import im.vector.app.features.settings.troubleshoot.TestDeviceSettings
import im.vector.app.features.settings.troubleshoot.TestEndpointAsTokenRegistration
import im.vector.app.features.settings.troubleshoot.TestNotification
import im.vector.app.features.settings.troubleshoot.TestPushFromPushGateway
import im.vector.app.features.settings.troubleshoot.TestPushRulesSettings
import im.vector.app.features.settings.troubleshoot.TestSystemSettings
import im.vector.app.features.settings.troubleshoot.TestUnifiedPushEndpoint
import im.vector.app.features.settings.troubleshoot.TestUnifiedPushGateway
import im.vector.app.gplay.features.settings.troubleshoot.TestFirebaseToken
import im.vector.app.gplay.features.settings.troubleshoot.TestPlayServices
import im.vector.app.gplay.features.settings.troubleshoot.TestTokenRegistration
import io.mockk.every
import io.mockk.mockk
import org.amshove.kluent.internal.assertEquals
import org.junit.Test
import org.matrix.android.sdk.api.session.Session

class BwiNotificationTroubleshootTestManagerFactoryTest {

    @Test
    fun `test BwiNotificationTroubleshootTestManagerFactory`() {
        val unifiedPushHelper: UnifiedPushHelper = mockk()

        val testSystemSettings: TestSystemSettings = mockk()
        val testAccountSettings: TestAccountSettings = mockk()
        val testDeviceSettings: TestDeviceSettings = mockk()
        val testPushRulesSettings: TestPushRulesSettings = mockk()
        val testCurrentUnifiedPushDistributor: TestCurrentUnifiedPushDistributor = mockk()
        val testUnifiedPushGateway: TestUnifiedPushGateway = mockk()
        val testUnifiedPushEndpoint: TestUnifiedPushEndpoint = mockk()
        val testAvailableUnifiedPushDistributors: TestAvailableUnifiedPushDistributors = mockk()
        val testEndpointAsTokenRegistration: TestEndpointAsTokenRegistration = mockk()
        val testPushFromPushGateway: TestPushFromPushGateway = mockk()
        val testAutoStartBoot: TestAutoStartBoot = mockk()
        val testBackgroundRestrictions: TestBackgroundRestrictions = mockk()
        val testBatteryOptimization: TestBatteryOptimization = mockk()
        val testNotification: TestNotification = mockk()
        val fcmHelper: FcmHelper = mockk()
        val testPlayServices: TestPlayServices = mockk()
        val testFirebaseToken: TestFirebaseToken = mockk()
        val testTokenRegistration: TestTokenRegistration = mockk()
        val vectorFeatures: VectorFeatures = mockk()
        val textNotificationTimesSettings: TestNotificationTimesSettings = mockk()
        val activeSessionHolder: ActiveSessionHolder = mockk()
        every { testSystemSettings.manager = any() } returns Unit
        every { testAccountSettings.manager = any() } returns Unit
        every { testDeviceSettings.manager = any() } returns Unit
        every { testPushRulesSettings.manager = any() } returns Unit
        every { testCurrentUnifiedPushDistributor.manager = any() } returns Unit
        every { testUnifiedPushGateway.manager = any() } returns Unit
        every { testUnifiedPushEndpoint.manager = any() } returns Unit
        every { testAvailableUnifiedPushDistributors.manager = any() } returns Unit
        every { testEndpointAsTokenRegistration.manager = any() } returns Unit
        every { testPushFromPushGateway.manager = any() } returns Unit
        every { testAutoStartBoot.manager = any() } returns Unit
        every { testBackgroundRestrictions.manager = any() } returns Unit
        every { testBatteryOptimization.manager = any() } returns Unit
        every { testNotification.manager = any() } returns Unit
        every { testBatteryOptimization.manager = any() } returns Unit
        every { testPlayServices.manager = any() } returns Unit
        every { testFirebaseToken.manager = any() } returns Unit
        every { testTokenRegistration.manager = any() } returns Unit
        every { textNotificationTimesSettings.manager = any() } returns Unit
        every { activeSessionHolder.getSafeActiveSession() } returns null
        every { vectorFeatures.allowExternalUnifiedPushDistributors() } returns false
        every { fcmHelper.isFirebaseAvailable() } returns true
        every { unifiedPushHelper.isEmbeddedDistributor() } returns false

        val bwiNotificationTroubleshootTestManagerFactory = BwiNotificationTroubleshootTestManagerFactory(
                unifiedPushHelper,
                testSystemSettings,
                testAccountSettings,
                testDeviceSettings,
                testPushRulesSettings,
                testCurrentUnifiedPushDistributor,
                testUnifiedPushGateway,
                testUnifiedPushEndpoint,
                testAvailableUnifiedPushDistributors,
                testEndpointAsTokenRegistration,
                testPushFromPushGateway,
                testAutoStartBoot,
                testBackgroundRestrictions,
                testBatteryOptimization,
                testNotification,
                fcmHelper,
                testPlayServices,
                testFirebaseToken,
                testTokenRegistration,
                vectorFeatures,
                textNotificationTimesSettings,
                activeSessionHolder
        )
        val fragment: Fragment = mockk()
        val bwiNotificationTroubleshootTestManager = bwiNotificationTroubleshootTestManagerFactory.create(fragment)
        assertEquals(8, bwiNotificationTroubleshootTestManager.testListSize)

        every { activeSessionHolder.getSafeActiveSession() } returns null
        every { vectorFeatures.allowExternalUnifiedPushDistributors() } returns true
        every { fcmHelper.isFirebaseAvailable() } returns true
        every { unifiedPushHelper.isEmbeddedDistributor() } returns false

        val bwiNotificationTroubleshootTestManager2 = bwiNotificationTroubleshootTestManagerFactory.create(fragment)
        assertEquals(10, bwiNotificationTroubleshootTestManager2.testListSize)

        every { activeSessionHolder.getSafeActiveSession() } returns null
        every { vectorFeatures.allowExternalUnifiedPushDistributors() } returns true
        every { fcmHelper.isFirebaseAvailable() } returns false
        every { unifiedPushHelper.isEmbeddedDistributor() } returns false
        every { unifiedPushHelper.isBackgroundSync() } returns false

        val bwiNotificationTroubleshootTestManager3 = bwiNotificationTroubleshootTestManagerFactory.create(fragment)
        assertEquals(10, bwiNotificationTroubleshootTestManager3.testListSize)

        every { activeSessionHolder.getSafeActiveSession() } returns null
        every { vectorFeatures.allowExternalUnifiedPushDistributors() } returns true
        every { fcmHelper.isFirebaseAvailable() } returns true
        every { unifiedPushHelper.isEmbeddedDistributor() } returns true
        every { unifiedPushHelper.isBackgroundSync() } returns false

        val bwiNotificationTroubleshootTestManager4 = bwiNotificationTroubleshootTestManagerFactory.create(fragment)
        assertEquals(10, bwiNotificationTroubleshootTestManager4.testListSize)

        val fakeSession = mockk<Session>()
        every { fakeSession.workingTimeService().isWorkingTimeEnabled() } returns false
        every { activeSessionHolder.getSafeActiveSession() } returns fakeSession
        every { vectorFeatures.allowExternalUnifiedPushDistributors() } returns true
        every { fcmHelper.isFirebaseAvailable() } returns true
        every { unifiedPushHelper.isEmbeddedDistributor() } returns true
        every { unifiedPushHelper.isBackgroundSync() } returns false

        val bwiNotificationTroubleshootTestManager5 = bwiNotificationTroubleshootTestManagerFactory.create(fragment)
        assertEquals(10, bwiNotificationTroubleshootTestManager5.testListSize)

        every { activeSessionHolder.getSafeActiveSession() } returns fakeSession
        every { vectorFeatures.allowExternalUnifiedPushDistributors() } returns true
        every { fcmHelper.isFirebaseAvailable() } returns false
        every { unifiedPushHelper.isEmbeddedDistributor() } returns false
        every { unifiedPushHelper.isBackgroundSync() } returns true

        val bwiNotificationTroubleshootTestManager6 = bwiNotificationTroubleshootTestManagerFactory.create(fragment)
        assertEquals(9, bwiNotificationTroubleshootTestManager6.testListSize)

        every { activeSessionHolder.getSafeActiveSession() } returns fakeSession
        every { fakeSession.workingTimeService().isWorkingTimeEnabled() } returns true
        every { vectorFeatures.allowExternalUnifiedPushDistributors() } returns true
        every { fcmHelper.isFirebaseAvailable() } returns false
        every { unifiedPushHelper.isEmbeddedDistributor() } returns false
        every { unifiedPushHelper.isBackgroundSync() } returns true

        val bwiNotificationTroubleshootTestManager7 = bwiNotificationTroubleshootTestManagerFactory.create(fragment)
        assertEquals(10, bwiNotificationTroubleshootTestManager7.testListSize)
    }
}
