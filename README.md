<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png?inline=false" alt="BundesMessenger Logo" width="128" height="128">
</div>

<div align="center">
  <h2 align="center">BundesMessenger-Android</h2>
</div>

----

Wir freuen uns, dass Du Dich für den BundesMessenger interessierst. 

Fangen wir mit dem Wichtigsten an. Hier findest Du die offizielle App für Android:

<p align="center">  
  <a href=https://play.google.com/store/apps/details?id=de.bwi.messenger>
  <img alt="Download Google Play Store" src="resources/img/google-play-badge.png" width=160>
  </a>
</p>

Wenn Dir die App gefällt, lass gerne eine positive Bewertung da.

BundesMessenger-Android ist ein Android Matrix Client basierend auf [Element Android](https://github.com/element-hq/element-android)
von [Element Software](https://element.io/).

Allgemeine Infos zum Thema BundesMessenger und was dahinter steckt findet ihr [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info).


## Grundsätzliches

BundesMessenger ist ein Artefakt, welches durch die BWI im Herstellungsprozess für den BwMessenger alle 4 Wochen  hergestellt wird.

Hierzu durchlaufen wir folgenden Prozess:

<p align="center">  
  <img alt="Download Google Play Store" src="resources/img/element-bum-bwm.png" width="600">
</p>

Aufgrund der starken Bindung an *Element Android* ist es aktuell nicht vorgesehen, dass ihr über das Repository Einfluss auf den BundesMessenger nehmen könnt. Wenn ihr euch beteiligen wollt, müsst ihr eure Contribution direkt in [Element Android](https://github.com/element-hq/element-android) einfließen lassen. Diese werden in der Regel im Anschluss in den BundesMessenger übernommen. 

Wenn ihr euch unsicher seid, haltet hierzu gerne [Rücksprache mit uns](#kontakt).

> Übrigens: Diesen Prozess leben wir selber auch für bestimmte Features.

**Warum veröffentlichen wir hier den Quellcode?**

Wir möchten 100% transparent sein und euch die Möglichkeit geben den Source Code einzusehen.

Wir freuen uns über euer Feedback. Öffnet gerne neue Issues für eure Fragen oder Probleme hier im GitLab.

## Repo

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-android

## Fehler und Verbesserungsvorschläge

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-android/-/issues


## Abhängigkeiten

[Element Android](https://github.com/element-hq/element-android)


## Für Entwickler

Weiterführende Dokumentation zum Projekt, dem Betrieb und der Architektur findest Du [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info) und [in diesem Dokument](./docs/_developer_onboarding.md)



## Updates

In der Regel werden wir hier alle 4 Wochen ein Update veröffentlichen. In etwa zeitgleich erfolgt das Update der Apps in den App Stores (https://play.google.com/store/apps/details?id=de.bwi.messenger).

Sollte es zu Sicherheitsvorfällen kommen, stellen wir hier und im App Store auch kurzfristig Hotfixes zur Verfügung.

## Forks

Du hast die Möglichkeit einen Fork von diesem Projekt zu machen. Bitte beachte, dass die [kontinuierliche Pflege](#updates) sehr viel Aufwand und Entwicklungsressourcen benötigt. 

Wir nehmen euch diese Arbeit ab, da wir dies für den BwMessenger ohnehin machen müssen. Daher empfehlen wir  euch für den produktiven Einsatz in der öffentlichen Verwaltung die gepflegten BundesMessenger Apps aus den App Stores zu verwenden. Ziel ist es nicht euch auszuschließen, sondern eine stetige hohe Qualität und Sicherheit zur Verfügung zu stellen.

| :warning: Wichtig: Dieses Repository ist ein Mirror eines internen BWI Repos. Die aktive Entwicklung findet nicht in diesem Repository statt. Es müssen BWI spezifische Build Anteile entfernt werden (z.B. GitLab CI Informationen). Um dies zu gewährleisten wird die Git History beim Mirroring neu geschrieben. Wenn ihr einen Fork von diesem Repository erstellt, müsst ihr mit erhöhten Aufwänden bei Updates rechnen. |
| --- |

## Nutzung

Um die BundesMessenger App aus dem Play Store nutzen zu können, müsst ihr euer Backend registrieren lassen. Weitere Infos dazu [hier](https://messenger.bwi.de/ich-will-bum).

Wenn Du Dein Backend noch nicht erfolgreich aufgebaut hast, aber trotzdem schon einen Blick in die App werfen möchtest, bieten wir Dir eine Demo Umgebung an. Bitte kontaktiere uns per [Email](mailto:bundesmessenger@bwi.de&subject=Ich%20will%20testen).

## Rechtliches

**BundesMessenger** ist sowohl als Logo- und als Textmarke durch die [BWI GmbH](https://www.bwi.de) geschützt.

Eine Nutzung derselben ist nur mit Freigabe durch die BWI möglich.

| :warning: Wichtig: Falls ihr einen Fork erstellt, müssen alle Verweise (Texte & Bilder) auf BundesMessenger und die BWI zwingend entfernt werden! Das betrifft nicht die Git History, sondern die Releases der jeweiligen neuen Apps. |
| --- |

Die Lizenz des BundesMessenger Android ist die [AGPLv3](./LICENSE).

### Copyright

- [BWI GmbH](https://messenger.bwi.de/copyright)
- [Element](https://element.io/copyright)

## Kontakt

Für den Austausch zum BundesMessenger haben wir einen [Matrix Raum](https://matrix.to/#/#opencodebum:matrix.org) erstellt.

<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/qr_matrix_room.png" alt="QR Code Matrix" width="128" height="128"/>
</div>

Kein Matrix Client zur Hand, dann auch gerne über unser [Email Postfach](mailto:bundesmessenger@bwi.de).

Wir freuen uns auf den Austausch.
