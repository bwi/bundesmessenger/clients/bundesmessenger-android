/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.configuration

import android.content.Context
import android.content.res.Resources
import de.bwi.messenger.configuration.appconfig.MdmService
import de.bwi.messenger.configuration.providers.PermalinkConfigurationProvider
import io.mockk.every
import io.mockk.mockk
import org.amshove.kluent.internal.assertEquals
import org.junit.Before
import org.junit.Test

class AppConfigurationTest {

    lateinit var resources: Resources
    lateinit var context: Context
    lateinit var mdmService: MdmService
    lateinit var permalinkConfigurationProvider: PermalinkConfigurationProvider

    lateinit var underTest: MessengerConfiguration

    @Before
    fun setUp() {
        resources = mockk()
        every { resources.getStringArray(any()) } returns emptyArray()
        context = mockk()
        every { context.resources } returns resources

        mdmService = mockk()
        every { mdmService.isPinEnabled() } returns false
        every { mdmService.getHomeServerUrl() } returns ""

        permalinkConfigurationProvider = mockk()
        every { permalinkConfigurationProvider.getValue(any(), any()) } returns ""

        underTest = MessengerConfiguration(context, mdmService, permalinkConfigurationProvider)
    }

    @Test
    fun `test server whitelist`() {
        assertEquals(true, underTest.isServerWhitelistEnabled)
        assertEquals(true, MessengerBetaConfiguration(context, mdmService, permalinkConfigurationProvider).isServerWhitelistEnabled)
        assertEquals(false, MessengerOpenConfiguration(context, mdmService, permalinkConfigurationProvider).isServerWhitelistEnabled)
    }
}
