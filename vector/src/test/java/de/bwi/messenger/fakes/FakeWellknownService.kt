/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.fakes


import de.bwi.messenger.features.location_sharing.BwiWellknownService
import im.vector.app.features.settings.VectorPreferences
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import org.matrix.android.sdk.api.raw.RawService

class FakeWellknownService {
    val A_PRIVACY_URL = "https://privacy_policy_url"
    val A_VALID_WELLKNOWN = "{\n" +
            "  \"m.homeserver\": {\n" +
            "    \"base_url\": \"https://bwm.example.com\"\n" +
            "  },\n" +
            "  \"org.matrix.msc3575.proxy\": {\n" +
            "    \"url\": \"https://bwm-sync.example.com:443\"\n" +
            "  },\n" +
            "  \"m.tile_server\": {\n" +
            "    \"map_style_url\": \"https://bwm.example.com/style.json\"\n" +
            "  },\n" +
            "  \"io.element.rendezvous\": {\n" +
            "      \"server\": \"https://bwm.example.com/_synapse/rendezvous\"\n" +
            "  },\n" +
            "  \"io.element.e2ee\": {\n" +
            "    \"default\": true,\n" +
            "    \"secure_backup_required\": true,\n" +
            "    \"secure_backup_setup_methods\": [\"passphrase\"],\n" +
            "    \"outbound_keys_pre_sharing_mode\": \"on_room_opening\"\n" +
            "  },\n" +
            "  \"de.bwi\": {\n" +
            "    \"data_privacy_url\": \"$A_PRIVACY_URL\",\n" +
            "    \"imprint_url\": \"https://www.bwi.de/impressum\",\n" +
            "    \"federation\": {\n" +
            "        \"show_announcement\": true,\n" +
            "        \"show_introduction\": true,\n" +
            "        \"enable\": true\n" +
            "    }\n" +
            "  }\n" +
            "}"

    val A_VALID_WELLKNOWN_WITH_DISABLED_FEDERATION = "{\n" +
            "  \"m.homeserver\": {\n" +
            "    \"base_url\": \"https://bwm.example.com\"\n" +
            "  },\n" +
            "  \"org.matrix.msc3575.proxy\": {\n" +
            "    \"url\": \"https://bwm-sync.example.com:443\"\n" +
            "  },\n" +
            "  \"m.tile_server\": {\n" +
            "    \"map_style_url\": \"https://bwm.example.com/style.json\"\n" +
            "  },\n" +
            "  \"io.element.rendezvous\": {\n" +
            "      \"server\": \"https://bwm.example.com/_synapse/rendezvous\"\n" +
            "  },\n" +
            "  \"io.element.e2ee\": {\n" +
            "    \"default\": true,\n" +
            "    \"secure_backup_required\": true,\n" +
            "    \"secure_backup_setup_methods\": [\"passphrase\"],\n" +
            "    \"outbound_keys_pre_sharing_mode\": \"on_room_opening\"\n" +
            "  },\n" +
            "  \"de.bwi\": {\n" +
            "    \"data_privacy_url\": \"$A_PRIVACY_URL\",\n" +
            "    \"imprint_url\": \"https://www.bwi.de/impressum\",\n" +
            "    \"federation\": {\n" +
            "       \"enable\": false\n" +
            "    }\n" +
            "  }\n" +
            "}"

    val A_VALID_WELLKNOWN_WITHOUT_FEDERATION = "{\n" +
            "  \"m.homeserver\": {\n" +
            "    \"base_url\": \"https://bwm.example.com\"\n" +
            "  },\n" +
            "  \"org.matrix.msc3575.proxy\": {\n" +
            "    \"url\": \"https://bwm-sync.example.com:443\"\n" +
            "  },\n" +
            "  \"m.tile_server\": {\n" +
            "    \"map_style_url\": \"https://bwm.example.com/style.json\"\n" +
            "  },\n" +
            "  \"io.element.rendezvous\": {\n" +
            "      \"server\": \"https://bwm.example.com/_synapse/rendezvous\"\n" +
            "  },\n" +
            "  \"io.element.e2ee\": {\n" +
            "    \"default\": true,\n" +
            "    \"secure_backup_required\": true,\n" +
            "    \"secure_backup_setup_methods\": [\"passphrase\"],\n" +
            "    \"outbound_keys_pre_sharing_mode\": \"on_room_opening\"\n" +
            "  },\n" +
            "  \"de.bwi\": {\n" +
            "    \"data_privacy_url\": \"$A_PRIVACY_URL\",\n" +
            "    \"imprint_url\": \"https://www.bwi.de/impressum\"\n" +
            "  }\n" +
            "}"
    val fakeRawService = mockk<RawService>()
    val fakePreferences = mockk<VectorPreferences>().apply {
        every { isBwiFederationEnabled() } returns false
    }

    val instance = BwiWellknownService(fakeRawService, fakePreferences)

    fun givenWellknownSuccess() {
        coEvery { fakeRawService.getWellknown(any()) } returns A_VALID_WELLKNOWN
    }

    fun givenWellknownSuccessWithDisabledFederation() {
        coEvery { fakeRawService.getWellknown(any()) } returns A_VALID_WELLKNOWN_WITH_DISABLED_FEDERATION
    }

    fun givenWellknownSuccessWithoutFederation() {
        coEvery { fakeRawService.getWellknown(any()) } returns A_VALID_WELLKNOWN_WITHOUT_FEDERATION
    }

    fun givenWellknownError() {
        coEvery { fakeRawService.getWellknown(any()) } throws Exception("network error")
    }

    fun givenWellknown(wellknown: String) {
        coEvery { fakeRawService.getWellknown(any()) } returns wellknown
    }
}
