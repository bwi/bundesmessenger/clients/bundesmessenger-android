/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.fakes

import android.content.Context
import de.bwi.messenger.configuration.AppConfiguration
import de.bwi.messenger.configuration.MessengerConfiguration
import im.vector.app.test.fakes.FakeContext
import im.vector.app.test.fakes.FakeMdmService
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk

class FakeAppConfiguration(context: Context = FakeContext().instance) : AppConfiguration by spyk(MessengerConfiguration(context, FakeMdmService(), mockk())) {
    init {
        every { biometricKeyNeedsUserAuthentication } returns false
    }

    fun withNotificationTimeSetting(enabled: Boolean) = also {
        every { enableNotificationTimeSettings } returns enabled
    }

    fun givenEnabledLocationSharing(isEnabled: Boolean) = also {
        every { enableLocationSharing } returns isEnabled
    }

    fun givenEnabledServerSelection(isEnabled: Boolean) = also {
        every { enableServerSelection } returns isEnabled
    }

    fun givenAllowedHosts(isEnabled: Boolean) = also {
        every { isServerWhitelistEnabled } returns isEnabled
    }
}
