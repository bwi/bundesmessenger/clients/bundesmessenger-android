/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.roomdirectory

import com.airbnb.mvrx.test.MavericksTestRule
import de.bwi.messenger.fakes.FakeAppConfiguration
import de.bwi.messenger.features.location_sharing.BwiWellknownService
import im.vector.app.SpaceStateHandler
import im.vector.app.features.roomdirectory.createroom.CreateRoomAction
import im.vector.app.features.roomdirectory.createroom.CreateRoomArgs
import im.vector.app.features.roomdirectory.createroom.CreateRoomViewModel
import im.vector.app.features.roomdirectory.createroom.CreateRoomViewState
import im.vector.app.test.fakes.FakeSession
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.slot
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.internal.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.matrix.android.sdk.api.session.homeserver.HomeServerCapabilities
import org.matrix.android.sdk.api.session.room.model.RoomHistoryVisibility
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomParams

internal class CreateRoomViewModelTest {
    private val initialState = CreateRoomViewState(args = CreateRoomArgs("test"))
    private val fakeSession = FakeSession()
    private val fakeWellknownService = mockk<BwiWellknownService>()
    private val fakeAppConfiguration = FakeAppConfiguration()
    private val fakeSpaceStateHandler = mockk<SpaceStateHandler>()
    private val fakeHomeServerCapabilities = mockk<HomeServerCapabilities>()
    private val fakeRoomSummary = mockk<RoomSummary>()

    @get:Rule
    val mvrxTestRule = MavericksTestRule()

    private fun createViewModel(): CreateRoomViewModel {
        return CreateRoomViewModel(initialState, fakeSession, fakeWellknownService, fakeSpaceStateHandler, fakeAppConfiguration, mockk())
    }

    @Before
    fun setUp() {
        mockkStatic(HomeServerCapabilities::class)
        every { fakeSpaceStateHandler.getSafeActiveSpaceId() } returns "test"
        every { fakeHomeServerCapabilities.isFeatureSupported(any()) } returns HomeServerCapabilities.RoomCapabilitySupport.SUPPORTED
        every { fakeWellknownService.isBwiFederationEnabled() } returns false
    }

    @Test
    fun `on room creation the history visibility must be set to 'invited'`() = runTest {
        fakeSession.fakeHomeServerCapabilitiesService.givenCapabilities(fakeHomeServerCapabilities)
        fakeSession.fakeRoomService.getRoomSummaryReturns(fakeRoomSummary)
        val viewModel = createViewModel()
        val argumentCaptor = slot<CreateRoomParams>()
        viewModel.handle(CreateRoomAction.Create)
        coVerify { fakeSession.roomService().createRoom(capture(argumentCaptor)) }
        assertEquals(RoomHistoryVisibility.INVITED, argumentCaptor.captured.historyVisibility)
    }
}
