/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance

import de.bwi.messenger.features.maintenance.api.DowntimeInfo
import im.vector.app.R
import im.vector.app.test.fakes.FakeContext
import im.vector.lib.strings.CommonStrings
import org.amshove.kluent.internal.assertEquals
import org.junit.Test
import org.threeten.bp.ZonedDateTime

class DowntimeMessageFormatterTest {

    private val fakeContext = FakeContext()
    private val now = ZonedDateTime.now()

    @Test
    fun `test null value`() {
        val dmf = DowntimeMessageFormatter
        assertEquals(
                fakeContext.instance.getString(CommonStrings.general_maintenance_message),
                dmf.getFormattedDowntimeMessage(fakeContext.instance, null)
        )
    }

    @Test
    fun `stat and end on same day`() {
        assert(
                DowntimeMessageFormatter.getFormattedDowntimeMessage(fakeContext.instance,
                        DowntimeInfo(
                                startTime = now,
                                endTime = now,
                                warningStartTime = now,
                                type = "MAINTENANCE",
                                description = "description",
                                blocking = true
                        )
                )
                        .contains(fakeContext.instance.getString(CommonStrings.downtime_message_same_day))
        )
    }

    @Test
    fun `stat and end not on same day`() {
        assert(
                DowntimeMessageFormatter.getFormattedDowntimeMessage(fakeContext.instance,
                        DowntimeInfo(
                                startTime = now,
                                endTime = now.plusDays(2),
                                warningStartTime = now,
                                type = "MAINTENANCE",
                                description = "description",
                                blocking = true
                        )
                )
                        .contains(fakeContext.instance.getString(CommonStrings.downtime_message_range))
        )
    }
}
