/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.appconfig

import android.content.Context
import android.content.Intent
import android.content.RestrictionsManager
import android.os.Bundle
import androidx.core.content.ContextCompat.registerReceiver
import de.bwi.messenger.configuration.MessengerConfiguration
import de.bwi.messenger.configuration.appconfig.DefaultMdmService
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkStatic
import io.mockk.verify
import org.amshove.kluent.internal.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test

internal class MdmServiceTest {

    @Before
    fun setup() {
        mockkStatic("androidx.core.content.ContextCompat")
    }

    @After
    fun tearDown() {
        unmockkStatic("androidx.core.content.ContextCompat")
    }

    @Test
    fun `test remote configuration with MDM`() {
        val fakeContext = mockk<Context>()
        val fakeRestrictionsManager = mockk<RestrictionsManager>()
        val fakeRestartListener = mockk<()->Unit>()

        every { fakeRestartListener.invoke() } returns Unit
        every { fakeContext.getSystemService(Context.RESTRICTIONS_SERVICE) } returns fakeRestrictionsManager
        every { fakeContext.unregisterReceiver(any()) } returns Unit
        every { registerReceiver(any(), any(), any(), any(), any(), any()) } returns Intent()
        val fakeBundle = mockk<Bundle>()
        every { fakeBundle.getString("home_server_url") } returns "home_server"
        every { fakeBundle.getString("permalink_url") } returns "permalink"
        every { fakeBundle.getBoolean("no_pin_biometrics") } returns true

        val fakeBundle2 = mockk<Bundle>()
        every { fakeBundle2.getString("home_server_url") } returns "home_server2"
        every { fakeBundle2.getString("permalink_url") } returns "permalink"
        every { fakeBundle2.getBoolean("no_pin_biometrics") } returns true

        val fakeBundle3 = mockk<Bundle>()
        every { fakeBundle3.getString("home_server_url") } returns "home_server"
        every { fakeBundle3.getString("permalink_url") } returns "permalink2"
        every { fakeBundle3.getBoolean("no_pin_biometrics") } returns true

        val fakeBundle4 = mockk<Bundle>()
        every { fakeBundle4.getString("home_server_url") } returns "home_server"
        every { fakeBundle4.getString("permalink_url") } returns "permalink"
        every { fakeBundle4.getBoolean("no_pin_biometrics") } returns false

        every { fakeRestrictionsManager.applicationRestrictions } returns fakeBundle

        val mdmService = DefaultMdmService()

        assertEquals(null, mdmService.getHomeServerUrl())
        assertEquals(null, mdmService.getPermalinkBaseUrl())
        assertEquals(null, mdmService.isPinEnabled())

        mdmService.registerListener(fakeContext, fakeRestartListener)

        verify(exactly = 0) { fakeRestartListener.invoke() } // don't call the listener on initial setup

        mdmService.onReceive(fakeContext, Intent())
        verify(exactly = 0) { fakeRestartListener.invoke() }

        assertEquals("home_server", mdmService.getHomeServerUrl())
        assertEquals("permalink", mdmService.getPermalinkBaseUrl())
        assertEquals(false, mdmService.isPinEnabled())

        every { fakeRestrictionsManager.applicationRestrictions } returns fakeBundle2
        mdmService.onReceive(fakeContext, Intent())
        verify(exactly = 1) { fakeRestartListener.invoke() }

        every { fakeRestrictionsManager.applicationRestrictions } returns fakeBundle3
        mdmService.onReceive(fakeContext, Intent())
        verify(exactly = 2) { fakeRestartListener.invoke() }

        every { fakeRestrictionsManager.applicationRestrictions } returns fakeBundle4
        mdmService.onReceive(fakeContext, Intent())
        verify(exactly = 3) { fakeRestartListener.invoke() }

        assertEquals(true, mdmService.isPinEnabled())

        mdmService.unregisterListener(fakeContext)
        verify(exactly = 1) { fakeContext.unregisterReceiver(mdmService) }
    }

    @Test
    fun `test remote configuration with feature flag integration`()  {
        val fakeContext = mockk<Context>()
        val fakeRestartListener = mockk<()->Unit>()
        val fakeRestrictionsManager = mockk<RestrictionsManager>()

        every { fakeRestartListener.invoke() } returns Unit
        every { fakeContext.getSystemService(Context.RESTRICTIONS_SERVICE) } returns fakeRestrictionsManager
        every { registerReceiver(any(), any(), any(), any(), any(), any()) } returns Intent()
        val fakeBundle = mockk<Bundle>()
        every { fakeBundle.getString("home_server_url") } returns "home_server"
        every { fakeBundle.getBoolean("no_pin_biometrics") } returns true
        every { fakeRestrictionsManager.applicationRestrictions } returns fakeBundle

        val mdmService = DefaultMdmService()
        mdmService.registerListener(fakeContext, fakeRestartListener)

        val appConfiguration = MessengerConfiguration(fakeContext, mdmService, mockk())
        assertEquals(false, appConfiguration.usePinCodeLock)
        assertEquals("home_server", appConfiguration.homeServerUrl)
    }
}
