/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.quotation

import androidx.lifecycle.MutableLiveData
import com.airbnb.mvrx.test.MavericksTestRule
import dagger.Lazy
import de.bwi.messenger.fakes.FakeAppConfiguration
import im.vector.app.core.event.GetTimelineEventUseCase
import im.vector.app.features.home.room.detail.timeline.action.CheckIfCanRedactEventUseCase
import im.vector.app.features.home.room.detail.timeline.action.CheckIfCanReplyEventUseCase
import im.vector.app.features.home.room.detail.timeline.action.EventSharedAction
import im.vector.app.features.home.room.detail.timeline.action.MessageActionState
import im.vector.app.features.home.room.detail.timeline.action.MessageActionsViewModel
import im.vector.app.features.home.room.detail.timeline.action.TimelineEventFragmentArgs
import im.vector.app.features.home.room.detail.timeline.format.NoticeEventFormatter
import im.vector.app.features.home.room.detail.timeline.item.MessageInformationData
import im.vector.app.features.home.room.detail.timeline.item.ReactionsSummaryData
import im.vector.app.features.home.room.detail.timeline.style.TimelineMessageLayout
import im.vector.app.features.html.EventHtmlRenderer
import im.vector.app.features.html.PillsPostProcessor
import im.vector.app.features.html.VectorHtmlCompressor
import im.vector.app.test.fakes.FakeActiveSessionHolder
import im.vector.app.test.fakes.FakeErrorFormatter
import im.vector.app.test.fakes.FakeFlowLiveDataConversions
import im.vector.app.test.fakes.FakeSession
import im.vector.app.test.fakes.FakeStringProvider
import im.vector.app.test.fakes.FakeVectorPreferences
import im.vector.app.test.fakes.givenAsFlow
import im.vector.app.test.test
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.matrix.android.sdk.api.session.events.model.Event
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.room.model.EventAnnotationsSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageContent
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.send.SendState
import org.matrix.android.sdk.api.session.room.sender.SenderInfo
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.util.Optional

class CheckIfEventIsQuotationIsPossibleTest {


    @get:Rule
    val mavericksTestRule = MavericksTestRule(testDispatcher = UnconfinedTestDispatcher())

    private val A_ROOM_ID   = "room-id"
    private val AN_EVENT_ID = "event-id"
    private val A_SENDER_ID = "senderId"

    private val informationData = MessageInformationData(
            eventId          = AN_EVENT_ID,
            senderId         = A_SENDER_ID,
            ageLocalTS       = 0,
            avatarUrl        = "",
            sendState        = SendState.SENT,
            messageLayout    = TimelineMessageLayout.Default(showAvatar = true, showDisplayName = true, showTimestamp = true),
            reactionsSummary = ReactionsSummaryData(),
            sentByMe         = true,
    )
    private val initialState                = MessageActionState(TimelineEventFragmentArgs(
                                                    AN_EVENT_ID,
                                                    A_ROOM_ID,
                                                    informationData)
                                                    )
    private val fakeEventHtmlRenderer       = mockk<Lazy<EventHtmlRenderer>>()
    private val fakeSession                 = FakeSession()
    private val fakeEventFormatter          = mockk<NoticeEventFormatter>()
    private val fakeErrorFormatter          = FakeErrorFormatter()
    private val fakeStringProvider          = FakeStringProvider()
    private val fakePillsPostProcessor      = mockk<PillsPostProcessor.Factory>()
    private val fakeVectorPreferences       = FakeVectorPreferences()
    private val fakeAppConfiguration        = FakeAppConfiguration()
    private val fakeActiveSessionHolder     = FakeActiveSessionHolder()
    private val fakeFlowLiveDataConversions = FakeFlowLiveDataConversions()

    private fun createViewModel(): MessageActionsViewModel {
        return MessageActionsViewModel(
                initialState                 = initialState,
                eventHtmlRenderer            = fakeEventHtmlRenderer,
                htmlCompressor               = VectorHtmlCompressor(),
                session                      = fakeSession,
                noticeEventFormatter         = fakeEventFormatter,
                errorFormatter               = fakeErrorFormatter,
                stringProvider               = fakeStringProvider.instance,
                pillsPostProcessorFactory    = fakePillsPostProcessor,
                vectorPreferences            = fakeVectorPreferences.instance,
                appConfiguration             = fakeAppConfiguration,
                checkIfCanReplyEventUseCase  = CheckIfCanReplyEventUseCase(),
                checkIfCanRedactEventUseCase = CheckIfCanRedactEventUseCase(fakeActiveSessionHolder.instance)
        )
    }

    @Before
    fun setUp() {
        fakeFlowLiveDataConversions.setup()
        every { fakeVectorPreferences.instance.areThreadMessagesEnabled() } returns false
        every { fakeVectorPreferences.instance.developerMode() } returns false
        every { fakeAppConfiguration.permalinkBaseUrl } returns ""
        every { fakeActiveSessionHolder.instance.getActiveSession().myUserId } returns fakeSession.myUserId
        every { fakeActiveSessionHolder.instance.getActiveSession().roomService().getRoom(A_ROOM_ID) } returns null
        every { fakeSession.roomService().getRoom(A_ROOM_ID)
                .relationService().getEventAnnotationsSummaryLive(AN_EVENT_ID) } returns MutableLiveData(Optional(EventAnnotationsSummary()))
        every { fakeSession.roomService().getRoom(A_ROOM_ID)
                .relationService().getEventAnnotationsSummary(AN_EVENT_ID) } returns EventAnnotationsSummary()
    }

    @Test
    fun `check that quotation is never possible`() = runTest {

        val msgtypes = listOf(MessageType.MSGTYPE_TEXT,
                              MessageType.MSGTYPE_EMOTE,
                              MessageType.MSGTYPE_NOTICE,
                              MessageType.MSGTYPE_IMAGE,
                              MessageType.MSGTYPE_AUDIO,
                              MessageType.MSGTYPE_VIDEO,
                              MessageType.MSGTYPE_LOCATION,
                              MessageType.MSGTYPE_FILE)
        msgtypes.forEach {
            val content = mapOf(MessageContent.MSG_TYPE_JSON_KEY to it,
                    "body" to "testBody")
            val event = Event(
                    eventId  = AN_EVENT_ID,
                    type     = EventType.MESSAGE,
                    senderId = A_SENDER_ID,
                    roomId   = A_ROOM_ID,
                    content  = content
            )

            SendState.values().forEach { sendState ->
                event.sendState = sendState
                val timelineEvent = TimelineEvent(
                        root         = event,
                        localId      = 0,
                        eventId      = AN_EVENT_ID,
                        displayIndex = 0,
                        senderInfo   = SenderInfo(
                                userId              = "1",
                                displayName         = "1",
                                isUniqueDisplayName = false,
                                avatarUrl           = null
                        )
                )
                val timelineService = fakeSession
                        .roomService()
                        .getRoom(A_ROOM_ID)
                        .timelineService()
                timelineService.givenTimelineEventLiveReturns(AN_EVENT_ID, timelineEvent)
                        .givenAsFlow()
                timelineService.givenTimelineEventReturns(AN_EVENT_ID, timelineEvent)
                val getTimelineEventUseCase = GetTimelineEventUseCase(fakeActiveSessionHolder.instance)
                getTimelineEventUseCase.execute(A_ROOM_ID, AN_EVENT_ID).test(this)

                val viewModel = createViewModel()
                val viewModelTest = viewModel.test()
                viewModelTest
                        .assertLatestState {
                                            it.actions.find { it is EventSharedAction.Quote } == null
                                           }
                        .finish()
            }
        }

    }
}
