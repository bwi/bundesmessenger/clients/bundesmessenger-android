/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import im.vector.app.R
import im.vector.app.features.themes.ThemeProvider
import im.vector.app.test.fakes.FakeContext
import im.vector.app.test.fakes.FakeStringProvider
import io.mockk.every
import io.mockk.mockk
import org.amshove.kluent.internal.assertEquals
import org.junit.Test

class FederationIntroductionCarouselStateFactoryTest {
    @Test
    fun `test SplashCarouselState items`() {
        val fakeContext = FakeContext()
        fakeContext.givenColor( 0xFFFFFF)
        val fakeStringProvider = FakeStringProvider()
        val fakeThemeProvider = mockk<ThemeProvider>()

        every { fakeThemeProvider.isLightTheme() } returns true
        val splashCarouselStateLight = FederationIntroductionCarouselStateFactory(
                fakeContext.instance,
                fakeStringProvider.instance,
                fakeThemeProvider
                )
                .create()
        assertEquals(3, splashCarouselStateLight.items.size)
        assertEquals(R.drawable.ic_federation_intro0_light, splashCarouselStateLight.items[0].image)
        assertEquals(R.drawable.ic_federation_intro1_light, splashCarouselStateLight.items[1].image)
        assertEquals(R.drawable.ic_federation_intro2, splashCarouselStateLight.items[2].image)

        every { fakeThemeProvider.isLightTheme() } returns false
        val splashCarouselStateDark = FederationIntroductionCarouselStateFactory(
                fakeContext.instance,
                fakeStringProvider.instance,
                fakeThemeProvider
                )
                .create()
        assertEquals(3, splashCarouselStateDark.items.size)
        assertEquals(R.drawable.ic_federation_intro0_dark, splashCarouselStateDark.items[0].image)
        assertEquals(R.drawable.ic_federation_intro1_dark, splashCarouselStateDark.items[1].image)
        assertEquals(R.drawable.ic_federation_intro2, splashCarouselStateDark.items[2].image)
    }
}
