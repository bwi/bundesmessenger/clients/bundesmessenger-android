/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.wellknown

import de.bwi.messenger.fakes.FakeWellknownService
import im.vector.app.test.fakes.FakeSession
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.internal.assertEquals
import org.junit.Test

class WellknownServiceTest {
    private val wellknownService = FakeWellknownService()

    @Test
    fun `test not yet loaded wellknown federation flat`() = runTest {
        assertEquals(false, wellknownService.instance.isBwiFederationEnabled())
    }

    @Test
    fun `test failed wellknown request presets`() = runTest {
        wellknownService.givenWellknownError()
        val wellknown = wellknownService.instance.getElementWellknown("test")
        assertEquals(true, wellknown.elementE2E?.e2eDefault)
        assertEquals(true, wellknown.elementE2E?.secureBackupRequired)
        assertEquals(false, wellknownService.instance.isBwiFederationEnabled())
        assert(wellknownService.instance.getMapStyleUrl().isBlank())

        wellknownService.instance.withElementWellKnown(this, FakeSession().sessionParams) {
            assertEquals(true, it?.elementE2E?.e2eDefault)
            assertEquals(true, it?.elementE2E?.secureBackupRequired)
        }
    }

    @Test
    fun `test succeeded wellknown request`() = runTest {
        wellknownService.givenWellknownSuccess()
        var wellknown = wellknownService.instance.getElementWellknown("test")
        assertEquals(true, wellknown.elementE2E?.e2eDefault)
        assertEquals(true, wellknown.elementE2E?.secureBackupRequired)
        assertEquals(true, wellknownService.instance.isBwiFederationEnabled())
        assertEquals("https://bwm.example.com/style.json", wellknownService.instance.getMapStyleUrl())

        wellknownService.instance.withElementWellKnown(this, FakeSession().sessionParams) {
            assertEquals(true, it?.elementE2E?.e2eDefault)
            assertEquals(true, it?.elementE2E?.secureBackupRequired)
        }
    }

    @Test
    fun `test succeeded wellknown request with disabled federation`() = runTest {
        wellknownService.givenWellknownSuccessWithDisabledFederation()
        wellknownService.instance.getElementWellknown("test")
        assertEquals(false, wellknownService.instance.isBwiFederationEnabled())
    }

    @Test
    fun `test succeeded wellknown request without federation`() = runTest {
        wellknownService.givenWellknownSuccessWithoutFederation()
        wellknownService.instance.getElementWellknown("test")
        assertEquals(false, wellknownService.instance.isBwiFederationEnabled())
    }
}
