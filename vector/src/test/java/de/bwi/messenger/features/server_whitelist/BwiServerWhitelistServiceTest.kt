/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.server_whitelist

import android.content.res.AssetManager
import im.vector.app.test.fakes.FakeContext
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.internal.assertEquals
import org.junit.Before
import org.junit.Test
import org.matrix.android.sdk.api.failure.Failure
import org.matrix.android.sdk.api.raw.RawService
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream

class BwiServerWhitelistServiceTest {
    private val fakeContext = FakeContext()
    private val fakeRawService = mockk<RawService>()
    private val fakeAssetManager = mockk<AssetManager>()
    private val fakeConnectionDetectionHelper = mockk<ConnectionDetectionHelper>()

    private val whitelistService = BwiServerWhitelistService(fakeRawService, fakeContext.instance, fakeConnectionDetectionHelper)

    private val INVALID_WHITELIST = "invalid"
    private val VALID_JWS =
            "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJmcm9tX3NlcnZlciIsIm5hbWUiOiJKb2huIERvZSIsImFkbWluIjp0cnVlLCJpYXQiOjE1MTYyMzkwMjJ9.qhuO0XGFMN0pMptxgLPSUjpkI2vk7-AtbG62O21DPdl_rnryJUeFPcE3Iv5PMOquk8A2N8PDcAJb5sHY6D2T0aALNuUa-q1ToEXhr-BXK5uP1ZbFsxzOcuBy0XJTn85NhahOq5MYGg4RviUlzeug9_-WO5mfzcd2gkJS9AohRPEPD6xY4bMrbCihwUKUj4NId2qyHIaJdY53fSpdJtm8J3QpsCZdoNo4qRsQ2UxSPufU34ZMOvDDrnnhftAmU-HFTBgnxgztKXWSrDHNNRglgKOsF0kNCFclO_2tBpYbPSEdJ0uUZLKG2CYeqPF0dx6DKVy4Wd7j8-kIob-ZVZvoLQ"
    private val INVALID_JWS =
            "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.tyh-VfuzIxCyGYDlkBA7DfyjrqmSHu6pQ2hoZuFqUSLPNY2N0mpHb3nk5K17HWP_3cYHBw7AhHale5wky6-sVA"
    private val VALID_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1SU1LfVLPHCozMxH2Mo\n" +
            "4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0/IzW7yWR7QkrmBL7jTKEn5u\n" +
            "+qKhbwKfBstIs+bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyeh\n" +
            "kd3qqGElvW/VDL5AaWTg0nLVkjRo9z+40RQzuVaE8AkAFmxZzow3x+VJYKdjykkJ\n" +
            "0iT9wCS0DRTXu269V264Vf/3jvredZiKRkgwlL9xNAwxXFg0x/XFw005UWVRIkdg\n" +
            "cKWTjpBP2dPwVZ4WWC+9aGVd+Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbc\n" +
            "mwIDAQAB\n" +
            "-----END PUBLIC KEY-----"
    private val INVALID_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1SU1LfVLPHCozMxH2Mo\n" +
            "4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0/IzW7yWR7QkrmBL7jTKEn5u\n" +
            "+qKhbwKfBstIs+bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyeh\n" +
            "kd3qqGElvW/VDL5AaWTg0nLVkjRo9z+40RQzuVaE8AkAFmxZzow3x+VJYKdjykkJ\n" +
            "0iT9wCS0DRTXu269V264Vf/3jvredZiKRkgwlL9xNAwxXFg0x/XFw005UWVRIkdg\n" +
            "cKWTjpBP2dPwVZ4WWC+9aGVd+Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbc\n" +
            "mwIDAQAB\n" +
            "-----END PUBLIC KEY-----"
    private val VALID_RESPONSE = "[\"$VALID_JWS\"]"
    private val INVALID_RESPONSE = "[\"$INVALID_JWS\"]"
    private val VALID_RESPONSE_TWO_VALID_TOKEN = "[\"$VALID_JWS\", \"$VALID_JWS\"]"
    private val VALID_RESPONSE_VALID_INVALID_TOKEN = "[\"$VALID_JWS\", \"$INVALID_JWS\"]"
    private val VALID_RESPONSE_INVALID_VALID_TOKEN = "[\"$INVALID_JWS\", \"$VALID_JWS\"]"
    private val INVALID_RESPONSE_TWO_TOKEN = "[\"$INVALID_JWS\", \"$INVALID_JWS\"]"
    private val INVALID_RESPONSE_EMPTY_LIST = "[]"

    @Before
    fun setUp() {
        mockkStatic(InputStream::class)
        every { fakeContext.instance.assets } returns fakeAssetManager
        every { fakeAssetManager.open(any()) } returns ByteArrayInputStream(VALID_PUBLIC_KEY.toByteArray())
        fakeContext.givenAllowedHosts(listOf("from_assets"))
    }

    @Test
    fun `test successful server request`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns VALID_RESPONSE
        assertEquals(WhiteListState.IsAllowed, whitelistService.getWhitelistState("from_server"))
    }

    @Test
    fun `test successful server request with two valid jwt token`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns VALID_RESPONSE_TWO_VALID_TOKEN
        assertEquals(WhiteListState.IsAllowed, whitelistService.getWhitelistState("from_server"))
    }

    @Test
    fun `test successful server request with one valid and one invalid jwt token`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns VALID_RESPONSE_VALID_INVALID_TOKEN
        assertEquals(WhiteListState.IsAllowed, whitelistService.getWhitelistState("from_server"))
    }

    @Test
    fun `test successful server request with one invalid and one valid jwt token`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns VALID_RESPONSE_INVALID_VALID_TOKEN
        assertEquals(WhiteListState.IsAllowed, whitelistService.getWhitelistState("from_server"))
    }

    @Test
    fun `test successful server request with two invalid jwt token`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns INVALID_RESPONSE_TWO_TOKEN
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_server"))
    }

    @Test
    fun `test failed server request`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } throws Failure.OtherServerError("", 404)
        assertEquals(WhiteListState.IsAllowed, whitelistService.getWhitelistState("from_assets"))
    }

    @Test
    fun `test not trusted jws token`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns INVALID_RESPONSE
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_server"))
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_assets"))
    }

    @Test
    fun `test response with two not trusted jws token`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns INVALID_RESPONSE_TWO_TOKEN
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_server"))
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_assets"))
    }

    @Test
    fun `test response with empty list`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns INVALID_RESPONSE_EMPTY_LIST
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_server"))
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_assets"))
    }

    @Test
    fun `test missing key file`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns ""
        every { fakeAssetManager.open(any()) } throws IOException()
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_assets"))
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_server"))
    }

    @Test
    fun `test invalid format`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns INVALID_WHITELIST
        every { fakeAssetManager.open(any()) } returns ByteArrayInputStream(INVALID_WHITELIST.toByteArray())
        assertEquals(WhiteListState.IsAllowed, whitelistService.getWhitelistState("from_assets"))
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_server"))
    }

    @Test
    fun `test malformed url`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns INVALID_WHITELIST
        assertEquals(WhiteListState.IsAllowed, whitelistService.getWhitelistState("from_assets"))
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("from_server"))
    }

    @Test
    fun `test not allowed url from assets`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns INVALID_RESPONSE
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("not_allowed"))
    }

    @Test
    fun `test not allowed url from server`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns true
        coEvery { fakeRawService.getUrl(any(), any()) } returns VALID_RESPONSE
        assertEquals(WhiteListState.NotAllowed, whitelistService.getWhitelistState("not_allowed"))
    }

    @Test
    fun `test no connection`() = runTest {
        every { fakeConnectionDetectionHelper.isNetworkConnected() } returns false
        coEvery { fakeRawService.getUrl(any(), any()) } returns VALID_RESPONSE
        assertEquals(WhiteListState.NoConnection, whitelistService.getWhitelistState("not_allowed"))
    }
}
