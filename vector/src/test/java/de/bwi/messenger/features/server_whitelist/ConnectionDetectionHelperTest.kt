/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.server_whitelist

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import androidx.core.content.getSystemService
import im.vector.app.test.fakes.FakeContext
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.internal.assertEquals
import org.junit.Before
import org.junit.Test

class ConnectionDetectionHelperTest {
    private val fakeContext = FakeContext()
    private val fakeConnectivityManager: ConnectivityManager = mockk<ConnectivityManager>()
    private val fakeActiveNetwork: Network = mockk<Network>()
    private val fakeNetworkCapabilities = mockk<NetworkCapabilities>()

    private val connectionDetectionHelper = ConnectionDetectionHelper(fakeContext.instance)

    @Before
    fun setUp() {
        coEvery { fakeConnectivityManager.activeNetwork } returns fakeActiveNetwork
    }

    @Test
    fun `test successful connection`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(any()) } returns true
        assertEquals(true, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test failed connection`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(any()) } returns false
        assertEquals(false, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test capable of connection but no transport`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(any()) } returns false
        assertEquals(false, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test no connection manager should fail`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns null
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(any()) } returns false
        assertEquals(false, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test capabilities are null should fail`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns null
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(any()) } returns true
        assertEquals(false, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test capabilities validated false should fail`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) } returns true
        coEvery { fakeNetworkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(any()) } returns true
        assertEquals(false, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test cellular transport should be true then connection should be there`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) } returns false
        assertEquals(true, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test wifi transport should be true then connection should be there`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) } returns false
        assertEquals(true, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test vpn transport should be true then connection should be there`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) } returns false
        assertEquals(true, connectionDetectionHelper.isNetworkConnected())
    }

    @Test
    fun `test ethernet transport should be true then connection should be there`() = runTest {
        coEvery { fakeContext.instance.getSystemService<ConnectivityManager>() } returns fakeConnectivityManager
        coEvery { fakeConnectivityManager.getNetworkCapabilities(any()) } returns fakeNetworkCapabilities
        coEvery { fakeNetworkCapabilities.hasCapability(any()) } returns true
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN) } returns false
        coEvery { fakeNetworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) } returns true
        assertEquals(true, connectionDetectionHelper.isNetworkConnected())
    }
}
