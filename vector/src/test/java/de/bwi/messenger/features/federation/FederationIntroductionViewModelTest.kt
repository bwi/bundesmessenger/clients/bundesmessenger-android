/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import com.airbnb.mvrx.test.MavericksTestRule
import im.vector.app.test.fakes.FakeActiveSessionHolder
import im.vector.app.test.fakes.FakeSession
import im.vector.app.test.test
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test

class FederationIntroductionViewModelTest {

    @get:Rule
    val mavericksTestRule = MavericksTestRule(testDispatcher = UnconfinedTestDispatcher())

    @Test
    fun `test visibility of Got-it button`() = runTest {
        val initialState = FederationIntroductionState()
        val session = FakeSession()
        val fakeActiveSession = FakeActiveSessionHolder(session)
        val activeSessionHolder = fakeActiveSession.instance
        val viewModel = FederationIntroductionViewModel(initialState, activeSessionHolder)
        val viewModelTest = viewModel.test()
        viewModelTest
                .assertLatestState {
                    !it.gotItButtonVisible
                }
                .assertNoEvents()
                .finish()

        viewModel.handle(FederationIntroductionAction.PageSelected(0, 3))
        val viewModelTest2 = viewModel.test()
        viewModelTest2
                .assertLatestState {
                    !it.gotItButtonVisible
                }
                .assertNoEvents()
                .finish()

        viewModel.handle(FederationIntroductionAction.PageSelected(1, 3))
        val viewModelTest3 = viewModel.test()
        viewModelTest3
                .assertLatestState {
                    !it.gotItButtonVisible
                }
                .assertNoEvents()
                .finish()

        viewModel.handle(FederationIntroductionAction.PageSelected(2, 3))
        val viewModelTest4 = viewModel.test()
        viewModelTest4
                .assertLatestState {
                    it.gotItButtonVisible
                }
                .assertNoEvents()
                .finish()


        viewModel.handle(FederationIntroductionAction.GotIt)
        val viewModelTest5 = viewModel.test()
        viewModelTest5
                .assertLatestState {
                    it.gotItButtonVisible
                }
                .assertEvents(FederationIntroductionViewEvents.Finish)
                .finish()


        fakeActiveSession.givenGetSafeActiveSessionReturns(null)
        val viewModel6 = FederationIntroductionViewModel(initialState, fakeActiveSession.instance)
        viewModel6.handle(FederationIntroductionAction.GotIt)
        val viewModelTest6 = viewModel.test()
        viewModelTest6
                .assertLatestState {
                    it.gotItButtonVisible
                }
                .finish()

    }
}
