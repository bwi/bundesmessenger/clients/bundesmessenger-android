/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.workingtime

import android.os.Looper
import androidx.lifecycle.MutableLiveData
import com.airbnb.mvrx.test.MavericksTestRule
import de.bwi.messenger.fakes.FakeAppConfiguration
import im.vector.app.features.roomprofile.RoomProfileArgs
import im.vector.app.features.roomprofile.notifications.RoomNotificationSettingsViewModel
import im.vector.app.features.roomprofile.notifications.RoomNotificationSettingsViewState
import im.vector.app.test.fakes.FakeRoomPushRuleService
import im.vector.app.test.fakes.FakeSession
import im.vector.app.test.fixtures.RoomSummaryFixture
import im.vector.app.test.test
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataEvent
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.getRoom
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.util.Optional

private const val ROOM_ID = "123"

internal class WorkingTimeTest {
    private val fakeAppConfig = FakeAppConfiguration()
    private val roomSummary = RoomSummaryFixture.aRoomSummary(ROOM_ID, true)
    private val fakeSession = FakeSession.withRoomSummary(roomSummary)
    private val fakeRoom = fakeSession.getRoom(ROOM_ID)!!
    private val fakePushRuleService = fakeRoom.roomPushRuleService() as FakeRoomPushRuleService
    private val roomNotificationStateLiveData = MutableLiveData<RoomNotificationState>()
    private val roomSummaryLiveData = MutableLiveData<Optional<RoomSummary>>()
    private val workingTimeService = FakeWorkingTimeService().withWorkingTimeConfig(emptyMap())
    private val userAccountDataLiveData = MutableLiveData<Optional<UserAccountDataEvent>>()

    @get:Rule
    val mvrxTestRule = MavericksTestRule()

    @Before
    fun setUp() {
        mockkStatic(Looper::class)
        val looper = mockk<Looper> {
            every { thread } returns Thread.currentThread()
        }
        every { Looper.getMainLooper() } returns looper

        every { fakePushRuleService.getLiveRoomNotificationState() } returns roomNotificationStateLiveData
        fakeRoom.apply {
            every { roomSummary() } returns roomSummary
            every { getRoomSummaryLive() } returns roomSummaryLiveData
        }
        fakeSession.apply {
            every { workingTimeService() } returns workingTimeService
        }
        fakeSession.fakeSessionAccountDataService.apply {
            every { getLiveUserAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns userAccountDataLiveData
        }
    }

    @Test
    fun `test passing notification times feature flag to the RoomNotificationSettingsViewModel`() = runTest {
        workingTimeService
                .withWorkingTimeSetting(false)
                .withWorkingTimeSettingForRoom(false)

        val viewModel = RoomNotificationSettingsViewModel(
                RoomNotificationSettingsViewState(RoomProfileArgs(roomId = ROOM_ID)),
                fakeSession,
                fakeAppConfig.withNotificationTimeSetting(false)
        )

        val test = viewModel.test()
        test
                .assertState(
                        viewModel.awaitState().copy(
                                isWorkingTimeFeatureEnabled = false,
                                isWorkingTimeSettingEnabled = false,
                                isWorkingTimeEnabledForRoom = false
                        )
                )
                .finish()
    }

    @Test
    fun `test applying notification times settings on new account data event`() = runTest {
        workingTimeService
                .withWorkingTimeSetting(false)
                .withWorkingTimeSettingForRoom(false)

        val viewModel = RoomNotificationSettingsViewModel(
                RoomNotificationSettingsViewState(RoomProfileArgs(roomId = ROOM_ID)),
                fakeSession,
                fakeAppConfig.withNotificationTimeSetting(true)
        )

        val test = viewModel.test()

        workingTimeService
                .withWorkingTimeSetting(true)
                .withWorkingTimeSettingForRoom(true)

        userAccountDataLiveData.value = Optional.from(UserAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES, emptyMap()))

        verify { workingTimeService.isWorkingTimeEnabledForRoom(ROOM_ID, true) }
        test
                .assertStates(
                        viewModel.awaitState().copy(
                                isWorkingTimeFeatureEnabled = true,
                                isWorkingTimeSettingEnabled = false,
                                isWorkingTimeEnabledForRoom = false
                        ),
                        viewModel.awaitState().copy(
                                isWorkingTimeFeatureEnabled = true,
                                isWorkingTimeSettingEnabled = true,
                                isWorkingTimeEnabledForRoom = true
                        )
                )
                .finish()
    }

    @Test
    fun `test global notification times setting overrides room setting`() = runTest {
        workingTimeService
                .withWorkingTimeSetting(false)
                .withWorkingTimeSettingForRoom(true)
        val viewModel = RoomNotificationSettingsViewModel(
                RoomNotificationSettingsViewState(RoomProfileArgs(roomId = ROOM_ID)),
                fakeSession,
                fakeAppConfig.withNotificationTimeSetting(true))

        val test = viewModel.test()
        test
                .assertState(
                        viewModel.awaitState().copy(
                                isWorkingTimeFeatureEnabled = true,
                                isWorkingTimeSettingEnabled = false,
                                isWorkingTimeEnabledForRoom = false
                        )
                )
                .finish()
    }
}
