/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.features.share

import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test

class GeoUriParserTest {

    @Test
    fun `parse Munich`() {
        val result = "geo:48.137154,11.576124;u=30".parseGeoUri()!!
        result.latitude shouldBeEqualTo 48.137154
        result.longitude shouldBeEqualTo 11.576124
        result.uncertainty shouldBeEqualTo 30.0
    }

    @Test
    fun `parse FFM`() {
        val result = "geo:50.110924,8.682127".parseGeoUri()!!
        result.latitude shouldBeEqualTo 50.110924
        result.longitude shouldBeEqualTo 8.682127
        result.uncertainty shouldBe null
    }
    @Test
    fun `invalid geo uri`() {
        "invalid".parseGeoUri() shouldBe null
    }
}
