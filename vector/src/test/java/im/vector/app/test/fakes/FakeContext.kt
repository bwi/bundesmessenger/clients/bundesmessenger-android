/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.test.fakes

import android.content.ClipboardManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.Uri
import android.os.ParcelFileDescriptor
import androidx.annotation.ColorInt
import im.vector.app.R
import io.mockk.InternalPlatformDsl.toStr
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.matrix.android.sdk.api.util.sha256
import java.io.OutputStream

class FakeContext(
        private val contentResolver: ContentResolver = mockk(),
        private val resources: Resources = mockk()
) {

    val instance = mockk<Context>()

    init {
        every { instance.contentResolver } returns contentResolver
        every { instance.applicationContext } returns instance
        every { instance.resources } returns resources
        every { resources.getStringArray(im.vector.app.config.R.array.allowed_matrix_hosts) } returns emptyArray<String>()
        every { instance.getString(any()) } answers {
            "test-${args[0]}"
        }
        every { instance.getString(any(), any()) } answers {
            "test-${args[0]}-${args[1].toStr()}"
        }
        every { instance.getString(any(), any(), any()) } answers {
            "test-${args[0]}-${args[1].toStr()}"
        }
        every { instance.getString(any(), any(), any(), any()) } answers {
            "test-${args[0]}-${args[1].toStr()}"
        }
        every { instance.getString(any(), any(), any(), any(), any()) } answers {
            "test-${args[0]}-${args[1].toStr()}"
        }
        every { instance.getString(any(), any(), any(), any(), any(), any()) } answers {
            "test-${args[0]}-${args[1].toStr()}"
        }
        every { instance.getString(any(), any(), any(), any(), any(), any(), any()) } answers {
            "test-${args[0]}-${args[1].toStr()}"
        }
    }

    fun givenColor(@ColorInt color: Int) {
        @Suppress("DEPRECATION")
        every { instance.resources.getColor(any()) } returns color
        every { instance.resources.getColor(any() ,any()) } returns color
    }

    fun givenFileDescriptor(uri: Uri, mode: String, factory: () -> ParcelFileDescriptor?) {
        val fileDescriptor = factory()
        every { contentResolver.openFileDescriptor(uri, mode, null) } returns fileDescriptor
    }

    fun givenSafeOutputStreamFor(uri: Uri): OutputStream {
        val outputStream = mockk<OutputStream>(relaxed = true)
        every { contentResolver.openOutputStream(uri, "wt") } returns outputStream
        return outputStream
    }

    fun givenMissingSafeOutputStreamFor(uri: Uri) {
        every { contentResolver.openOutputStream(uri, "wt") } returns null
    }

    fun givenNoConnection() {
        val connectivityManager = FakeConnectivityManager()
        connectivityManager.givenNoActiveConnection()
        givenService(Context.CONNECTIVITY_SERVICE, ConnectivityManager::class.java, connectivityManager.instance)
    }

    fun <T> givenService(name: String, klass: Class<T>, service: T) {
        every { instance.getSystemService(name) } returns service
        every { instance.getSystemService(klass) } returns service
    }

    fun givenHasConnection() {
        val connectivityManager = FakeConnectivityManager()
        connectivityManager.givenHasActiveConnection()
        givenService(Context.CONNECTIVITY_SERVICE, ConnectivityManager::class.java, connectivityManager.instance)
    }

    fun givenAllowedHosts(hosts: List<String>) {
        every { instance.resources.getStringArray(any()) } returns hosts.map { it.sha256() }.toTypedArray()
    }

    fun givenStartActivity(intent: Intent) {
        justRun { instance.startActivity(intent) }
    }

    fun verifyStartActivity(intent: Intent) {
        verify { instance.startActivity(intent) }
    }

    fun givenClipboardManager(): FakeClipboardManager {
        val fakeClipboardManager = FakeClipboardManager()
        givenService(Context.CLIPBOARD_SERVICE, ClipboardManager::class.java, fakeClipboardManager.instance)
        return fakeClipboardManager
    }

    fun givenPackageName(name: String) {
        every { instance.packageName } returns name
    }
}
