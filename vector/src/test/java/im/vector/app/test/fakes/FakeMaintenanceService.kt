/*
 * Copyright (c) 2022 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.test.fakes

import de.bwi.messenger.features.maintenance.MaintenanceService
import de.bwi.messenger.features.maintenance.api.DowntimeInfo
import de.bwi.messenger.features.maintenance.api.VersionsInfo
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

class FakeMaintenanceService {
    val instance = mockk<MaintenanceService>()
    private val activeDowntime = DowntimeInfo(ZonedDateTime.of(LocalDateTime.MIN, ZoneId.systemDefault()),
            ZonedDateTime.of(LocalDateTime.MAX, ZoneId.systemDefault()),
            ZonedDateTime.of(LocalDateTime.MIN, ZoneId.systemDefault()),
            DowntimeInfo.TYPE_MAINTENANCE, null, true)

    init {
        coEvery { instance.fetchMaintenanceData(any()) } returns Unit
        coEvery { instance.getCurrentDowntimeInfo() } returns null
        coEvery { instance.getVersionsInfo() } returns null
    }

    fun givenNotSupportedAppVersion() {
        val version = "99.99.99"
        every { instance.getVersionsInfo() } returns VersionsInfo(version, version)
    }

    fun givenAnActiveDowntime() {
        every { instance.getCurrentDowntimeInfo() } returns activeDowntime
        every { instance.shouldBlockOnBlockingDowntime() } returns true
    }
}
