/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.configuration

import android.content.Context
import de.bwi.messenger.configuration.appconfig.MdmService
import de.bwi.messenger.configuration.providers.HomeServerConfigurationProvider
import de.bwi.messenger.configuration.providers.InvitableUserDomainsConfiguration
import de.bwi.messenger.configuration.providers.PermalinkConfigurationProvider
import de.bwi.messenger.configuration.providers.PinCodeConfigurationProvider
import de.bwi.messenger.configuration.providers.PusherConfigurationProvider

interface AppConfiguration : SDKConfiguration {
    val showChatEffectsSettings: Boolean
    val showActionsFromUserList: Boolean
    val showDrawerActions: Boolean
    val showLowPriority: Boolean
    val enableNightModeInPdfViewer: Boolean
    val resetUnreadMessageCounterOnBarClick: Boolean
    val enableRoomShortcuts: Boolean
    val showEnableAllNotificationSetting: Boolean
    val showResetBackup: Boolean
    val showAllRecoveryKeyOptions: Boolean
    val showBackgroundSyncSettings: Boolean
    val showAVTrustedInfo: Boolean
    val showElementSpecificVerificationInfo: Boolean
    val showWebClientVerification: Boolean
    val showSecurityDeviceAndKeyInfoSettings: Boolean
    val showSendToUnverifiedDevicesSetting: Boolean
    val compressAllImages: Boolean
    val compressProfileAvatar: Boolean
    val openSettingsOnAvatarClick: Boolean
    val showLanguageSetting: Boolean
    val showNotImplementedActions: Boolean
    val stopOnNotTrustedCertificate: Boolean
    val showSendByBotSetting: Boolean
    val showCustomRole: Boolean
    val enablePrivateLockscreenNotifications: Boolean
    val matrixAppsEnabled: Boolean
    val showNerdyKeys: Boolean
    val enableAudioVideo: Boolean
    val enableAddFromPhonebook: Boolean
    val enableSimplifiedRoomPrivacySettings: Boolean
    val enableImageCropLibrary: Boolean
    val showTac: Boolean
    val showSupportSettings: Boolean
    val showDevSettingsRageShake: Boolean
    val enableSpecialImagePickerIntent: Boolean
    val enableSelfUserVerification: Boolean
    val enableServerSelection: Boolean
    val showExpliciteContentRooms: Boolean
    val enableSimplifiedRoomCreation: Boolean
    val showDisclaimerDialog: Boolean
    val enableWarnOnUnknownDevices: Boolean
    val enableNotificationTimeSettings: Boolean
    val enableVoiceMessages: Boolean
    val enableJavascript: Boolean
    val enableScreenshots: Boolean
    val enablePolls: Boolean
    val useInternalFileViewer: Boolean
    val enableOutgoingShare: Boolean
    val enableRootedDeviceDetector: Boolean
    val usePinCodeLock: Boolean
    val blockInviteInDM: Boolean
    val showContentInNotification: Boolean
    val usePinCodeGracePeriod: Boolean
    val showRoomDirectory: Boolean
    val invitableUserDomains: InvitableUserDomainsConfiguration
    val enablePasswordChange: Boolean
    val showLabs: Boolean
    val showAdvancedSettings: Boolean
    val encryptToVerifiedSessionsOnly: Boolean
    val enableKeyManagement: Boolean
    val enableClearMessagesNotificationSettings: Boolean
    val showReleaseNotesHint: Boolean
    val showFederationReleaseNotesHint: Boolean
    val permalinkBaseUrl: String
    val homeServerUrl: String
    val pusherUrl: String
    val enablePresence: Boolean
    val enableStatusMessage: Boolean
    val enableLocationSharing: Boolean
    val enableLiveLocationSharing: Boolean
    val enableThreads: Boolean
    val showNotificationsForEditedMessages: Boolean
    val enablePersonalNotesRoom: Boolean
    val showHappyBirthdayDialog2025: Boolean
    val biometricKeyNeedsUserAuthentication: Boolean
    val showUnsafeMessageWarning: Boolean // by default, Element shows a warning if the decryption keys for the message were fetched from key backup rather than being sent to you directly from another device
    val enableVerifyInRoomMemberProfile: Boolean
    val isServerWhitelistEnabled: Boolean
}

open class MessengerConfiguration(
        protected val context: Context,
        protected val mdmService: MdmService,
        permalinkConfigurationProvider: PermalinkConfigurationProvider,
) :
        AppConfiguration, BwiSDKConfiguration() {
    override val showChatEffectsSettings = false
    override val showActionsFromUserList = false
    override val showDrawerActions = false
    override val showLowPriority = false
    override val enableNightModeInPdfViewer = false
    override val resetUnreadMessageCounterOnBarClick = true
    override val enableRoomShortcuts = false
    override val showEnableAllNotificationSetting = false
    override val showResetBackup = false
    override val showAllRecoveryKeyOptions = false
    override val showBackgroundSyncSettings = false
    override val showAVTrustedInfo = false
    override val showElementSpecificVerificationInfo = false
    override val showWebClientVerification = false
    override val showSecurityDeviceAndKeyInfoSettings = false
    override val showSendToUnverifiedDevicesSetting = false
    override val compressAllImages = true
    override val compressProfileAvatar = true
    override val openSettingsOnAvatarClick = true
    override val showLanguageSetting = true
    override val showNotImplementedActions = false
    override val stopOnNotTrustedCertificate = true
    override val showSendByBotSetting = false
    override val showCustomRole = false
    override val enablePrivateLockscreenNotifications = true
    override val matrixAppsEnabled = false
    override val showNerdyKeys = false
    override val enableAudioVideo = false
    override val enableAddFromPhonebook = false
    override val enableSimplifiedRoomPrivacySettings = true
    override val enableImageCropLibrary = true
    override val showTac = false
    override val showSupportSettings = true
    override val showDevSettingsRageShake = false
    override val enableSpecialImagePickerIntent = false
    override val enableSelfUserVerification = true
    override val showExpliciteContentRooms = false
    override val enableSimplifiedRoomCreation = true
    override val showDisclaimerDialog = false
    override val enableWarnOnUnknownDevices = false
    override val enableNotificationTimeSettings = true
    override val enableVoiceMessages = true
    override val enableJavascript = false
    override val enableScreenshots = false
    override val enablePolls = true
    override val useInternalFileViewer = true
    override val enableOutgoingShare = false
    override val enableRootedDeviceDetector = true
    override val usePinCodeLock: Boolean by PinCodeConfigurationProvider(mdmService, true)
    override val blockInviteInDM = true
    override val showContentInNotification = false
    override val usePinCodeGracePeriod = true
    override val showRoomDirectory = true
    override val invitableUserDomains = InvitableUserDomainsConfiguration(listOf())
    override val enablePasswordChange = true
    override val showLabs = false
    override val showAdvancedSettings = false
    override val encryptToVerifiedSessionsOnly = false
    override val enableKeyManagement = false
    override val enableClearMessagesNotificationSettings = false
    override val showReleaseNotesHint = false
    override val showFederationReleaseNotesHint = false
    override val permalinkBaseUrl: String by permalinkConfigurationProvider
    override val homeServerUrl: String by HomeServerConfigurationProvider(context, mdmService)
    override val pusherUrl: String by PusherConfigurationProvider(context)
    override val enablePresence = false
    override val enableStatusMessage = false
    override val enableLocationSharing = true // if activated, remove location related permissions filtering in AndroidManifest
    override val enableLiveLocationSharing = false
    override val enableThreads = false
    override val showNotificationsForEditedMessages = false
    override val enablePersonalNotesRoom = false
    override val showHappyBirthdayDialog2025 = true
    override val biometricKeyNeedsUserAuthentication = false // deactivated due to related issues https://github.com/android/security-samples/issues/20
    override val showUnsafeMessageWarning = false
    override val enableVerifyInRoomMemberProfile = false
    override val enableServerSelection = false
    override val isServerWhitelistEnabled = true
}

class MessengerBetaConfiguration(context: Context, mdmService: MdmService, permalinkConfigurationProvider: PermalinkConfigurationProvider) : MessengerConfiguration(
        context,
        mdmService,
        permalinkConfigurationProvider
) {
    override val enableScreenshots = true
    override val enableRootedDeviceDetector = false
    override val showLabs = true
    override val showAdvancedSettings = true
}

class MessengerOpenConfiguration(context: Context, mdmService: MdmService, permalinkConfigurationProvider: PermalinkConfigurationProvider) : MessengerConfiguration(
        context,
        mdmService,
        permalinkConfigurationProvider
) {
    override val isServerWhitelistEnabled = false
}
