/*
 * Copyright (c) 2020 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.configuration.appconfig

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.RestrictionsManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DefaultMdmService @Inject constructor() : BroadcastReceiver(), MdmService {

    private val HOME_SERVER_URL = "home_server_url"
    private val PERMALINK_URL = "permalink_url"
    private val NO_PIN_BIOMETRICS = "no_pin_biometrics"
    private var onShouldRestartAppAfterConfigurationChangeListener: (() -> Unit)? = null

    private var appRestrictions: Bundle? = null
        set(value) {
            // unit test
            val sameConfig = field?.getString(HOME_SERVER_URL).equals(value?.getString(HOME_SERVER_URL)) &&
                    field?.getBoolean(NO_PIN_BIOMETRICS)?.equals(value?.getBoolean(NO_PIN_BIOMETRICS)) == true &&
                    field?.getString(PERMALINK_URL).equals(value?.getString(PERMALINK_URL))
            field = value
            if (!sameConfig) {
                onShouldRestartAppAfterConfigurationChangeListener?.invoke()
            }
        }

    override fun registerListener(context: Context, configurationChangedListener: () -> Unit) {
        val restrictionsManager = context.getSystemService(Context.RESTRICTIONS_SERVICE) as RestrictionsManager
        appRestrictions = restrictionsManager.applicationRestrictions
        onShouldRestartAppAfterConfigurationChangeListener = configurationChangedListener
        val restrictionsFilter = IntentFilter(Intent.ACTION_APPLICATION_RESTRICTIONS_CHANGED)
        ContextCompat.registerReceiver(context, this, restrictionsFilter, ContextCompat.RECEIVER_NOT_EXPORTED)
    }

    override fun unregisterListener(context: Context) {
        context.unregisterReceiver(this)
    }

    override fun getHomeServerUrl(): String? {
        return appRestrictions?.getString(HOME_SERVER_URL)
    }

    override fun getPermalinkBaseUrl(): String? {
        return appRestrictions?.getString(PERMALINK_URL)
    }

    override fun isPinEnabled(): Boolean? {
        return appRestrictions?.getBoolean(NO_PIN_BIOMETRICS)?.not()
    }

    override fun onReceive(context: Context, intent: Intent) {
        val restrictionsManager = context.getSystemService(Context.RESTRICTIONS_SERVICE) as RestrictionsManager
        appRestrictions = restrictionsManager.applicationRestrictions
    }
}
