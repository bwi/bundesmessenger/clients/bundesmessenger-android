/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.configuration.providers

import android.content.Context
import de.bwi.messenger.configuration.AppConfiguration
import de.bwi.messenger.configuration.appconfig.MdmService
import im.vector.app.BuildConfig
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class HomeServerConfigurationProvider(val context: Context, val mdmService: MdmService) : ReadOnlyProperty<AppConfiguration, String> {
    override fun getValue(thisRef: AppConfiguration, property: KProperty<*>): String {

        return mdmService.getHomeServerUrl()
                ?: context.getString(im.vector.app.config.R.string.matrix_org_server_url).takeIf { it.isNotEmpty() }
                ?: BuildConfig.HOME_SERVER_URL
    }
}
