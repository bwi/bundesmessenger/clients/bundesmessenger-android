/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.tracking

import android.content.Context
import de.bwi.messenger.features.tracking.AnalyticsConsentService
import org.matomo.sdk.Matomo
import org.matomo.sdk.Tracker
import org.matomo.sdk.TrackerBuilder
import org.matomo.sdk.extra.CustomDimension
import org.matomo.sdk.extra.TrackHelper
import timber.log.Timber

data class MatomoConfig(
        val url: String,
        val siteId: Int
)

class MatomoTracker(val configMap: MutableMap<String, MatomoConfig?>, val context: Context) : BasePerformanceTracker() {
    private var analyticsConsentService: AnalyticsConsentService? = null
    private var deviceId: String? = null
    private var tracker: Tracker? = null

    override fun configure(deviceId: String?, hostName: String?, analyticsConsentService: AnalyticsConsentService?) {
        this.deviceId = deviceId
        this.analyticsConsentService = analyticsConsentService
        kotlin.runCatching {
            if (configMap.containsKey(hostName) || configMap.containsKey("*")) {
                val matomoUrl = configMap[hostName]?.url ?: configMap["*"]?.url
                val matomoSiteId = configMap[hostName]?.siteId ?: configMap["*"]?.siteId!!
                tracker = TrackerBuilder.createDefault(matomoUrl, matomoSiteId)
                        .build(Matomo.getInstance(context))
                        .setDispatchInterval(500)
            }
        }
    }

    override fun track(trackingEvent: TrackingEventType, customDimensionValues: Map<AppTracker.Dimension, String>) {
        if (analyticsConsentService?.isTrackingEnabled() == true || trackingEvent is TrackingEventType.TrackingEvent && trackingEvent.action == AppTracker.Action.ConsentGiven) {
            when (trackingEvent) {
                is TrackingEventType.TrackingEvent -> {
                    val event = TrackHelper.track().event(trackingEvent.category.categoryName, trackingEvent.action.actionName).name(
                            trackingEvent.name
                    ).build()
                    tracker?.track(event)
                }
                is TrackingEventType.PageViewEvent -> trackPageView(trackingEvent, customDimensionValues)
                is TrackingEventType.PerformanceEvent -> trackPerformance(trackingEvent, customDimensionValues)
            }
        }
    }

    override fun reset() {
        tracker?.reset()
    }

    private fun trackPageView(trackingEvent: TrackingEventType.PageViewEvent, customDimensionValues: Map<AppTracker.Dimension, String>) {
        val event = TrackHelper.track().screen("").title(trackingEvent.pageName).build()
        customDimensionValues.forEach { (key, value) -> CustomDimension.setDimension(event, key.id, value) }
        tracker?.track(event)
    }

    private fun trackPerformance(trackingEvent: TrackingEventType.PerformanceEvent, customDimensionValues: Map<AppTracker.Dimension, String>) {
        performanceEvents.find { it.action == trackingEvent.action }?.let {
            val duration = System.currentTimeMillis() - it.startTimeInMillis
            if (duration > it.threasholdInMillis) {
                val event = TrackHelper.track().event(AppTracker.Category.Performance.categoryName, trackingEvent.action.actionName)
                        .value(duration.toFloat()).build()
                customDimensionValues.forEach { (key, value) -> CustomDimension.setDimension(event, key.id, value) }
                Timber.i("Performance event ${trackingEvent.action} took ${System.currentTimeMillis() - it.startTimeInMillis}ms")
                tracker?.track(event)
            }
            performanceEvents.remove(it)
        }
    }
}
