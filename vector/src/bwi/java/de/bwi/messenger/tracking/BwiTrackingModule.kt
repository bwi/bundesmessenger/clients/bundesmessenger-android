/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.tracking

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import im.vector.app.R
import org.xmlpull.v1.XmlPullParser
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object BwiTrackingModule {

    @Provides
    @Singleton
    fun providesAppTracker(
            context: Context,
    ): AppTracker {
        val parser = context.resources.getXml(R.xml.matomo_config)
        val configMap: MutableMap<String, MatomoConfig?> = mutableMapOf()

        kotlin.runCatching {
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }
                // Starts by looking for the entry tag.
                if (parser.name == "entry") {
                    readEntry(parser, configMap)
                }
            }
        }

        return if (configMap.isNotEmpty()) {
            MatomoTracker(configMap, context)
        } else {
            EmptyTracker()
        }
    }

    private fun readEntry(parser: XmlPullParser, configMap: MutableMap<String, MatomoConfig?>) {
        parser.require(XmlPullParser.START_TAG, null, "entry")
        var host: String? = null
        var url: String? = null
        var siteId: Int? = null
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "homeserver" -> host = readConfigPart(parser, "homeserver")
                "matomo_url" -> url = readConfigPart(parser, "matomo_url")
                "matomo_site_id" -> siteId = readConfigPart(parser, "matomo_site_id").toIntOrNull()
            }
        }
        if (host != null && url != null && siteId != null) {
            configMap[host] = MatomoConfig(url, siteId)
        }
    }

    private fun readConfigPart(parser: XmlPullParser, partName: String): String {
        parser.require(XmlPullParser.START_TAG, null, partName)
        val result = readText(parser)
        parser.require(XmlPullParser.END_TAG, null, partName)
        return result
    }

    private fun readText(parser: XmlPullParser): String {
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return result
    }
}
