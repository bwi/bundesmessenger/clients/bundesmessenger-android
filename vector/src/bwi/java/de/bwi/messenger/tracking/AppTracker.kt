/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http=//www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.tracking

import de.bwi.messenger.features.tracking.AnalyticsConsentService
import timber.log.Timber

interface TrackingEventType {
    data class TrackingEvent(val category: AppTracker.Category, val action: AppTracker.Action, val name: String? = null) : TrackingEventType
    class PerformanceEvent(val action: AppTracker.Action, val threasholdInMillis: Long = 0) : TrackingEventType {
        val startTimeInMillis = System.currentTimeMillis()
    }

    data class PageViewEvent(val pageName: String) : TrackingEventType
}

interface AppTracker {
    enum class Action(val actionName: String) {
        OpenRoom("OpenRoom"),
        SendMessage("SendMessage"),
        ViewMessage("ViewMessage"),
        ConsentGiven("ConsentGiven"),
        Logout("Logout")
    }

    enum class Category(val categoryName: String) {
        General("General"),
        Performance("Performance")
    }

    enum class Dimension(val id: Int) {
        RoomMembersCount(2)
    }

    fun startMeasuringDurationForEvent(event: TrackingEventType.PerformanceEvent)

    fun stopMeasuringDurationForEvent(event: TrackingEventType.PerformanceEvent)

    // the user consent can be available later e.g. after the session is created
    fun configure(deviceId: String?, hostName: String?, analyticsConsentService: AnalyticsConsentService?)

    fun track(trackingEvent: TrackingEventType, customDimensionValues: Map<Dimension, String> = emptyMap())

    fun reset()
}

abstract class BasePerformanceTracker : AppTracker {
    val performanceEvents = mutableListOf<TrackingEventType.PerformanceEvent>()

    override fun startMeasuringDurationForEvent(event: TrackingEventType.PerformanceEvent) {
        performanceEvents.add(event)
    }

    override fun stopMeasuringDurationForEvent(event: TrackingEventType.PerformanceEvent) {
        performanceEvents.remove(event)
    }
}

class EmptyTracker : BasePerformanceTracker() {

    override fun track(trackingEvent: TrackingEventType, customDimensionValues: Map<AppTracker.Dimension, String>) {
        if (trackingEvent is TrackingEventType.PerformanceEvent) {
            performanceEvents.find { it.action == trackingEvent.action }?.let {
                performanceEvents.remove(it)
                Timber.i("Performance event ${trackingEvent.action} took ${System.currentTimeMillis() - it.startTimeInMillis}ms")
            }
        }
    }

    override fun reset() {
        // no op
    }

    override fun configure(deviceId: String?, hostName: String?, analyticsConsentService: AnalyticsConsentService?) {
        // no op
    }
}
