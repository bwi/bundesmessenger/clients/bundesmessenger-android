/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.multibindings.IntoMap
import de.bwi.messenger.features.federation.FederationIntroductionViewModel
import de.bwi.messenger.features.poll.PollParticipantsViewModel
import im.vector.app.core.di.MavericksAssistedViewModelFactory
import im.vector.app.core.di.MavericksViewModelComponent
import im.vector.app.core.di.MavericksViewModelKey

@InstallIn(MavericksViewModelComponent::class)
@Module
interface BwiViewModelModule {
    @Binds
    @IntoMap
    @MavericksViewModelKey(PollParticipantsViewModel::class)
    fun newHomeDetailViewModelFactory(factory: PollParticipantsViewModel.Factory): MavericksAssistedViewModelFactory<*, *>

    @Binds
    @IntoMap
    @MavericksViewModelKey(FederationIntroductionViewModel::class)
    fun FederationIntroductionViewModelFactory(factory: FederationIntroductionViewModel.Factory): MavericksAssistedViewModelFactory<*, *>
}
