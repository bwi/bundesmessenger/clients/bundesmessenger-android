/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.file_viewer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.airbnb.mvrx.args
import im.vector.app.core.platform.VectorBaseFragment
import im.vector.app.databinding.FragmentBwiViewerTxtBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BwiTxtViewerFragment : VectorBaseFragment<FragmentBwiViewerTxtBinding>() {

    private val fileViewerArgs: BwiFileViewerActivityArgs by args()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch(Dispatchers.IO) {
            context?.contentResolver?.openInputStream(fileViewerArgs.file)?.bufferedReader()?.use {
                val content = it.readText()
                withContext(Dispatchers.Main) {
                    views.txtWebView.loadDataWithBaseURL(null, content, "text/plain", "utf-8", null)
                }
            }
        }
    }

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentBwiViewerTxtBinding {
        return FragmentBwiViewerTxtBinding.inflate(inflater, container, false)
    }
}
