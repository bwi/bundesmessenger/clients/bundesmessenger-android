/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.version_check

import android.content.Context
import android.content.Intent
import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.platform.VectorBaseActivity
import im.vector.app.core.utils.openPlayStore
import im.vector.app.databinding.ActivityOutdatedVersionBinding
import im.vector.app.features.MainActivity
import im.vector.app.features.MainActivityArgs

@AndroidEntryPoint
class OutdatedVersionActivity : VectorBaseActivity<ActivityOutdatedVersionBinding>() {

    override fun getBinding() = ActivityOutdatedVersionBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        views.goToPlayStore.debouncedClicks {
            openPlayStore(this, buildMeta.applicationId)
        }

        views.logout.debouncedClicks {
            MainActivity.restartApp(
                this,
                    MainActivityArgs(
                            clearCredentials = true,
                            clearCache = true
                    )
            )
        }
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, OutdatedVersionActivity::class.java)
        }
    }
}
