/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.poll

import com.airbnb.mvrx.MavericksViewModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import im.vector.app.core.di.MavericksAssistedViewModelFactory
import im.vector.app.core.di.hiltMavericksViewModelFactory
import im.vector.app.core.extensions.getVectorLastMessageContent
import im.vector.app.core.platform.EmptyViewEvents
import im.vector.app.core.platform.VectorViewModel
import im.vector.app.features.home.room.detail.timeline.helper.PollResponseDataFactory
import im.vector.app.features.roomprofile.polls.list.data.RoomPollRepository
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.transform
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.getRoom
import org.matrix.android.sdk.api.session.room.getTimelineEvent
import org.matrix.android.sdk.api.session.room.members.roomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.message.MessagePollContent
import org.matrix.android.sdk.api.session.room.timeline.getRelationContent
import org.matrix.android.sdk.flow.flow
import org.matrix.android.sdk.flow.unwrap

class PollParticipantsViewModel @AssistedInject constructor(
        @Assisted val initialState: BwiPollParticipantsViewState,
        private val session: Session,
        private val roomPollRepository: RoomPollRepository,
        private val pollResponseDataFactory: PollResponseDataFactory,
) :
        VectorViewModel<BwiPollParticipantsViewState, PollParticipantsAction, EmptyViewEvents>(initialState) {

    @AssistedFactory
    interface Factory : MavericksAssistedViewModelFactory<PollParticipantsViewModel, BwiPollParticipantsViewState> {
        override fun create(initialState: BwiPollParticipantsViewState): PollParticipantsViewModel
    }

    companion object : MavericksViewModelFactory<PollParticipantsViewModel, BwiPollParticipantsViewState> by hiltMavericksViewModelFactory()

    init {
        observePolls()
    }

    override fun handle(action: PollParticipantsAction) {
        when (action) {
            is PollParticipantsAction.ToggleAllVoters -> {
                setState {
                    copy(
                            answers = answers?.map { answer ->
                                if (answer.answerId == action.answerId) {
                                    answer.copy(showAllVoters = !answer.showAllVoters)
                                } else {
                                    answer
                                }
                            }
                    )
                }
            }
        }
    }

    private fun observePolls() {
        val roomMemberQueryParams = roomMemberQueryParams {
            displayName = QueryStringValue.IsNotEmpty
            memberships = Membership.all()
        }
        session.getRoom(initialState.roomId)?.let { room ->
            combine(
                    room.flow().liveRoomMembers(roomMemberQueryParams),
                    room.flow()
                            .liveTimelineEvent(initialState.pollId)
                            .unwrap()
                            .transform {
                                if (it.getRelationContent() != null) {
                                    val event = room.getTimelineEvent(it.getRelationContent()?.eventId ?: "")
                                    if (event != null) emit(event)
                                } else {
                                    emit(it)
                                }
                            }
            ) { roomMembers, poll ->
                val pollContent = poll.getVectorLastMessageContent() as MessagePollContent
                val pollCreationInfo = pollContent.getBestPollCreationInfo()
                val question = pollCreationInfo?.question?.getBestQuestion().orEmpty()
                val pollResponseData = pollResponseDataFactory.create(poll)
                val winnerVoteCount = pollResponseData?.winnerVoteCount

                val answerVotes = pollCreationInfo?.answers?.map { answer ->
                    val voteSummary = pollResponseData?.getVoteSummaryOfAnOption(answer.id ?: "")
                    PollAnswerVotes(
                            answerId = answer.id.orEmpty(),
                            answer = answer.getBestAnswer().orEmpty(),
                            votesCount = voteSummary?.total ?: 0,
                            votedUsers = poll.annotations?.pollResponseSummary?.aggregatedContent?.votes
                                    ?.filter { vote -> vote.option == answer.id }
                                    ?.mapNotNull { vote ->
                                        val member = roomMembers.firstOrNull { it.userId == vote.userId }
                                        if (member != null) {
                                            VotedUser(member, vote.voteTimestamp)
                                        } else {
                                            null
                                        }
                                     }
                                    .orEmpty(),
                            isWinner = pollResponseData?.isClosed == true && (winnerVoteCount != 0 && voteSummary?.total == winnerVoteCount)
                    )
                }.orEmpty()
                setState {
                    copy(
                            question = question,
                            answers = answerVotes.sortedByDescending { it.votesCount }
                    )
                }
            }
            .launchIn(viewModelScope)
        }
    }
}
