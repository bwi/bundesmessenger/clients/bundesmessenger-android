/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.happy_birthday

import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.R
import im.vector.app.core.di.ActiveSessionHolder
import im.vector.app.core.epoxy.onClick
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
internal class HappyBirthdayDialog : DialogFragment(R.layout.happy_birthday_dialog) {

    @Inject lateinit var activeSessionHolder: ActiveSessionHolder

    override fun onStart() {
        super.onStart()
        isCancelable = false

        dialog?.apply {
            findViewById<View>(R.id.rateButton)?.onClick {
                activity?.application?.packageName?.let { appId ->
                    try {
                        "market://details?id=$appId".startActivityIntent()
                    } catch (e: ActivityNotFoundException) {
                        "https://play.google.com/store/apps/details?id=$appId".startActivityIntent()
                    }
                    dismiss()
                    setShouldNotShowDialogAgain()
                }
            }

            findViewById<View>(R.id.closeButton)?.onClick {
                dismiss()
                setShouldNotShowDialogAgain()
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if (activity is DialogInterface.OnDismissListener) {
            (activity as DialogInterface.OnDismissListener).onDismiss(dialog)
        }
    }

    private fun setShouldNotShowDialogAgain() {
        activity?.lifecycleScope?.launch {
            activeSessionHolder
                    .getSafeActiveSession()
                    ?.happyBirthdayService()
                    ?.setShouldShowHappyBirthday(
                            value = false,
                            year = 2025
                    )
        }
    }

    private fun String.startActivityIntent() =
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(this)))
}
