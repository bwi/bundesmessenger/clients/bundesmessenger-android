/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance

import android.content.Context
import de.bwi.messenger.features.maintenance.api.DowntimeInfo
import im.vector.lib.strings.CommonStrings
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter

class DowntimeMessageFormatter {
    companion object {
        private val dateFormatter = DateTimeFormatter.ofPattern("EEEE, dd.MM.yyyy").withZone(ZoneId.systemDefault())
        private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm").withZone(ZoneId.systemDefault())

        fun getFormattedDowntimeMessage(context: Context, downtimeInfo: DowntimeInfo?): String {
            return if (downtimeInfo != null) {
                if (downtimeInfo.isStartAndEndOnSameDay()) {
                    context.getString(
                        CommonStrings.downtime_message_same_day,
                            dateFormatter.format(downtimeInfo.localizedStartTime()),
                            timeFormatter.format(downtimeInfo.localizedStartTime()),
                            timeFormatter.format(downtimeInfo.localizedEndTime()),
                            ZonedDateTime.now().offset.id
                    )
                } else {
                    context.getString(
                        CommonStrings.downtime_message_range,
                            dateFormatter.format(downtimeInfo.localizedStartTime()),
                            timeFormatter.format(downtimeInfo.localizedStartTime()),
                            dateFormatter.format(downtimeInfo.localizedEndTime()),
                            timeFormatter.format(downtimeInfo.localizedEndTime()),
                            ZonedDateTime.now().offset.id,
                            ZonedDateTime.now().offset.id
                    )
                }
            } else {
                context.getString(CommonStrings.general_maintenance_message)
            }
        }
    }
}
