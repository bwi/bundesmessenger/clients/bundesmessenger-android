/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.status_message

import android.content.Context
import android.util.AttributeSet
import androidx.preference.EditTextPreference
import im.vector.app.R

class StatusMessagePreference : EditTextPreference {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    init {
        dialogLayoutResource = R.layout.dialog_presence_status_message
        // Set to false to remove the space when there is no icon
        isIconSpaceReserved = true
    }

    companion object {
        val PREFERENCE_KEY = "SETTINGS_STATUS_MESSAGE_PREFERENCE_KEY"
    }
}
