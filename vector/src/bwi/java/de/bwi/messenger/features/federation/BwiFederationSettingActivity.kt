/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.airbnb.mvrx.Mavericks
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.epoxy.onClick
import im.vector.app.core.platform.VectorBaseActivity
import im.vector.app.databinding.ActivityBwiFederationSettingBinding
import im.vector.app.features.home.AvatarRenderer
import im.vector.lib.strings.CommonStrings
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.util.toMatrixItem
import javax.inject.Inject

@AndroidEntryPoint
class BwiFederationSettingActivity : VectorBaseActivity<ActivityBwiFederationSettingBinding>() {

    @Inject lateinit var session: Session
    @Inject lateinit var avatarRenderer: AvatarRenderer

    override fun getBinding() = ActivityBwiFederationSettingBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent.extras?.getString(Mavericks.KEY_ARG)?.let { roomId ->
            session.roomService().getRoomSummary(roomId)?.let { roomSummary ->
                avatarRenderer.render(roomSummary.toMatrixItem(), views.federationSettingAvatar)
                views.federationSettingTitle.text = getString(CommonStrings.federation_setting_dialog_title, roomSummary.name)
                views.federationSettingActionFederate.onClick {
                    setFederationForRoom(roomId, true)
                }
                views.federationSettingActionDontFederate.onClick {
                    setFederationForRoom(roomId, false)
                }
            }
        }
    }

    private fun setFederationForRoom(roomId: String, enabled: Boolean) {
        showWaitingView()
        lifecycleScope.launch {
            try {
                session.bwiFederationService().setFederationForRoom(roomId, enabled)
                if (enabled) {
                    Toast.makeText(
                        this@BwiFederationSettingActivity,
                            getString(CommonStrings.room_is_federated),
                            Toast.LENGTH_LONG
                    )
                         .show()
                }
            } catch (failure: Throwable) {
                // we may set a dynamic error message here later
                intent.putExtra(EXTRAS_KEY_ERRORMSG, getString(CommonStrings.alert_message_set_acl_error))
                setResult(Activity.RESULT_OK, intent)
            } finally {
                finish()
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    @Suppress("OVERRIDE_DEPRECATION")
    override fun onBackPressed() {
        // user needs to decide whether the room is federated or not
    }

    companion object {
        fun newIntent(context: Context, roomId: String): Intent {
            return Intent(context, BwiFederationSettingActivity::class.java).apply {
                putExtra(Mavericks.KEY_ARG, roomId)
            }
        }

        const val EXTRAS_KEY_ERRORMSG = "EXTRAS_KEY_ERRORMSG"
    }
}
