/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.file_viewer

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.extensions.addFragment
import im.vector.app.core.platform.VectorBaseActivity
import im.vector.app.databinding.ActivitySimpleBinding
import im.vector.lib.core.utils.compat.getParcelableExtraCompat
import kotlinx.parcelize.Parcelize
import org.matrix.android.sdk.api.util.MimeTypes

@AndroidEntryPoint
class BwiFileViewerActivity : VectorBaseActivity<ActivitySimpleBinding>() {
    companion object {
        private const val INTENT_ARGS = "INTENT_ARGS"

        fun getIntent(context: Context, file: Uri, mimeType: String?): Intent {
            return Intent(context, BwiFileViewerActivity::class.java).apply {
                putExtra(INTENT_ARGS, BwiFileViewerActivityArgs(file, mimeType))
            }
        }
    }

    override fun initUiAndData() {
        if (!isFirstCreation() || intent?.hasExtra(INTENT_ARGS) == false) return

        intent?.getParcelableExtraCompat<BwiFileViewerActivityArgs>(INTENT_ARGS)?.let {
            when (it.mimeType) {
                MimeTypes.Pdf -> addFragment(views.simpleFragmentContainer, BwiPdfViewerFragment::class.java, it)
                MimeTypes.Text -> addFragment(views.simpleFragmentContainer, BwiTxtViewerFragment::class.java, it)
                else -> finish()
            }
        }
    }

    override fun getBinding() = ActivitySimpleBinding.inflate(layoutInflater)
}

@Parcelize
data class BwiFileViewerActivityArgs(
        val file: Uri,
        val mimeType: String?
) : Parcelable
