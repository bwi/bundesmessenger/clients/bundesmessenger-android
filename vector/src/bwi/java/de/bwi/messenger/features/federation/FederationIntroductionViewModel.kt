/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import com.airbnb.mvrx.MavericksViewModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import im.vector.app.core.di.ActiveSessionHolder
import im.vector.app.core.di.MavericksAssistedViewModelFactory
import im.vector.app.core.di.hiltMavericksViewModelFactory
import im.vector.app.core.platform.VectorViewModel
import kotlinx.coroutines.launch

class FederationIntroductionViewModel @AssistedInject constructor(
        @Assisted initialState: FederationIntroductionState,
        private val activeSessionHolder: ActiveSessionHolder
) :
        VectorViewModel<FederationIntroductionState, FederationIntroductionAction, FederationIntroductionViewEvents>(initialState) {

    @AssistedFactory
    interface Factory : MavericksAssistedViewModelFactory<FederationIntroductionViewModel, FederationIntroductionState> {
        override fun create(initialState: FederationIntroductionState): FederationIntroductionViewModel
    }

    companion object : MavericksViewModelFactory<FederationIntroductionViewModel, FederationIntroductionState> by hiltMavericksViewModelFactory()

    override fun handle(action: FederationIntroductionAction) {
         when (action) {
            is FederationIntroductionAction.PageSelected -> handlePageSelected(action)
            is FederationIntroductionAction.GotIt -> handleGotIt()
        }
    }

    private fun handlePageSelected(action: FederationIntroductionAction.PageSelected) {
        setState {
            copy(gotItButtonVisible = action.pageIndex == action.totalPages - 1)
        }
    }

    private fun handleGotIt() {
        activeSessionHolder.getSafeActiveSession()?.let { session ->
            viewModelScope.launch {
                 session.releaseNotesService().setShouldShowFederationIntroduction(false)
                _viewEvents.post(FederationIntroductionViewEvents.Finish)
            }
        }
    }
}
