/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.poll

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.extensions.configureWith
import im.vector.app.core.platform.VectorBaseFragment
import im.vector.app.databinding.FragmentPollParticipantsBinding
import kotlinx.parcelize.Parcelize
import javax.inject.Inject

@Parcelize
data class BwiPollParticipantsArgs(
        val roomId: String,
        val pollId: String,
) : Parcelable

@AndroidEntryPoint
class BwiPollParticipantsFragment : VectorBaseFragment<FragmentPollParticipantsBinding>(), BwiPollParticipantsControllerCallback {

    @Inject lateinit var controller: BwiPollParticipantsController

    val viewModel: PollParticipantsViewModel by fragmentViewModel()

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentPollParticipantsBinding {
        return FragmentPollParticipantsBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        views.pollParticipantsRecyclerView.configureWith(controller, hasFixedSize = true)
        controller.callback = this
    }

    override fun invalidate() = withState(viewModel) { state ->
        super.invalidate()
        controller.setData(state)
    }

    override fun onToggleAllVotersClicked(answerId: String) {
        viewModel.handle(PollParticipantsAction.ToggleAllVoters(answerId))
    }
}
