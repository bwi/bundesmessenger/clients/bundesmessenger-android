/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import android.content.Context
import androidx.annotation.AttrRes
import androidx.annotation.DrawableRes
import im.vector.app.R
import im.vector.app.core.resources.StringProvider
import im.vector.app.core.utils.colorTerminatingFullStop
import im.vector.app.features.onboarding.ftueauth.SplashCarouselState
import im.vector.app.features.themes.ThemeProvider
import im.vector.app.features.themes.ThemeUtils
import im.vector.lib.core.utils.epoxy.charsequence.EpoxyCharSequence
import im.vector.lib.core.utils.epoxy.charsequence.toEpoxyCharSequence
import im.vector.lib.strings.CommonStrings
import javax.inject.Inject

class FederationIntroductionCarouselStateFactory @Inject constructor(
        private val context: Context,
        private val stringProvider: StringProvider,
        private val themeProvider: ThemeProvider,
) {

    fun create(): SplashCarouselState {
        val lightTheme = themeProvider.isLightTheme()
        fun hero(@DrawableRes lightDrawable: Int, @DrawableRes darkDrawable: Int) = if (lightTheme) lightDrawable else darkDrawable
        return SplashCarouselState(
                listOf(
                        SplashCarouselState.Item(
                                CommonStrings.federation_intro_title0.colorTerminatingFullStop(com.google.android.material.R.attr.colorAccent),
                                CommonStrings.federation_intro_body0,
                                hero(R.drawable.ic_federation_intro0_light, R.drawable.ic_federation_intro0_dark),
                                im.vector.lib.ui.styles.R.drawable.bwi_welcome_background,
                                useBottomSpace = false
                        ),
                        SplashCarouselState.Item(
                                CommonStrings.federation_intro_title1.colorTerminatingFullStop(com.google.android.material.R.attr.colorAccent),
                                CommonStrings.federation_intro_body1,
                                hero(R.drawable.ic_federation_intro1_light, R.drawable.ic_federation_intro1_dark),
                                im.vector.lib.ui.styles.R.drawable.bwi_welcome_background,
                                useBottomSpace = false
                        ),
                        SplashCarouselState.Item(
                                CommonStrings.federation_intro_title2.colorTerminatingFullStop(com.google.android.material.R.attr.colorAccent),
                                CommonStrings.federation_intro_body2,
                                R.drawable.ic_federation_intro2,
                                im.vector.lib.ui.styles.R.drawable.bwi_welcome_background,
                                useBottomSpace = false,
                                expandImageToTop = true
                        )
                )
        )
    }

    private fun Int.colorTerminatingFullStop(@AttrRes color: Int): EpoxyCharSequence {
        return stringProvider.getString(this)
                .colorTerminatingFullStop(ThemeUtils.getColor(context, color))
                .toEpoxyCharSequence()
    }
}
