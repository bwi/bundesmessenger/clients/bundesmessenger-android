/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.camera

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.video.MediaStoreOutputOptions
import androidx.camera.video.VideoRecordEvent
import androidx.camera.view.CameraController
import androidx.camera.view.CameraController.IMAGE_ANALYSIS
import androidx.camera.view.CameraController.IMAGE_CAPTURE
import androidx.camera.view.CameraController.VIDEO_CAPTURE
import androidx.camera.view.LifecycleCameraController
import androidx.camera.view.video.AudioConfig
import androidx.camera.view.video.ExperimentalVideo
import androidx.core.view.isVisible
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import dagger.hilt.android.AndroidEntryPoint
import de.bwi.messenger.configuration.AppConfiguration
import im.vector.app.core.platform.VectorBaseFragment
import im.vector.app.core.utils.checkPermissions
import im.vector.app.core.utils.registerForPermissionsResult
import im.vector.app.databinding.FragmentCameraBinding
import im.vector.lib.multipicker.CameraPicker
import im.vector.lib.strings.CommonStrings
import org.threeten.bp.Duration
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ExperimentalVideo // Implementation based on https://github.com/android/camera-samples/blob/main/CameraXBasic/app/src/main/java/com/android/example/cameraxbasic/fragments/CameraFragment.kt
@AndroidEntryPoint
class CameraFragment : VectorBaseFragment<FragmentCameraBinding>() {

    @Inject lateinit var appConfiguration: AppConfiguration

    private val cameraViewModel: CameraViewModel by activityViewModel()
    private var maxVideoDuratonInMillis = TimeUnit.SECONDS.toMillis(30).toInt()

    private lateinit var cameraExecutor: ExecutorService

    private val permissionVideoLauncher = registerForPermissionsResult { allGranted, _ ->
        cameraViewModel.enableAudio(allGranted)
        captureVideo(allGranted)
    }

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentCameraBinding {
        return FragmentCameraBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cameraExecutor = Executors.newSingleThreadExecutor()

        val cameraController = LifecycleCameraController(requireContext())
        cameraController.bindToLifecycle(this)
        views.cameraPreview.controller = cameraController
        setupCameraUI()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cameraExecutor.shutdown()
    }

    override fun invalidate() {
        super.invalidate()
        withState(cameraViewModel) {
            views.cameraModePhotoButton.isVisible = activity?.intent?.action != CameraActivity.ACTION_PHOTO_ONLY
            views.cameraModeVideoButton.isVisible = activity?.intent?.action != CameraActivity.ACTION_PHOTO_ONLY
            views.audioWarning.isVisible = it.isCapturing && it.mode == CameraMode.VIDEO && !it.isAudioEnabled
            views.cameraProgress.visibility = if (it.isCapturing) View.VISIBLE else View.INVISIBLE
            views.cameraProgress.isIndeterminate = it.mode == CameraMode.PHOTO
            views.capturePhotoButton.isEnabled = !it.isCapturing
            views.captureVideoButton.isEnabled = !it.isCapturing
            views.stopCaptureVideoButton.visibility = if (it.isCapturing && it.mode == CameraMode.VIDEO) View.VISIBLE else View.INVISIBLE
            views.captureVideoButton.visibility = if (!it.isCapturing && it.mode == CameraMode.VIDEO) View.VISIBLE else View.INVISIBLE
            views.capturePhotoButton.visibility = if (!it.isCapturing && it.mode == CameraMode.PHOTO) View.VISIBLE else View.INVISIBLE
            if (!it.isCapturing) {
                refreshFlashModeIcon(it.flash)
                if (it.mode == CameraMode.PHOTO) {
                    configureCameraForImageCapture()
                } else {
                    configureCameraForVideoCapture()
                }
            }
            views.cameraPreview.controller?.enableTorch(it.flash)
            views.videoTimer.visibility = if (it.mode == CameraMode.VIDEO) View.VISIBLE else View.INVISIBLE
        }
    }

    private fun configureCameraForImageCapture() {
        views.cameraPreview.controller?.setEnabledUseCases(IMAGE_CAPTURE.or(IMAGE_ANALYSIS))
    }

    private fun configureCameraForVideoCapture() {
        views.cameraPreview.controller?.setEnabledUseCases(VIDEO_CAPTURE)
    }

    private fun toggleCamera(cameraController: CameraController) {
        if (cameraController.cameraSelector == CameraSelector.DEFAULT_BACK_CAMERA && cameraController.hasCamera(CameraSelector.DEFAULT_FRONT_CAMERA)) {
            cameraController.cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA
        } else if (cameraController.cameraSelector == CameraSelector.DEFAULT_FRONT_CAMERA && cameraController.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA)) {
            cameraController.cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
        }
    }

    private fun setupCameraUI() {
        views.cameraProgress.max = maxVideoDuratonInMillis

        views.cameraFlipButton.setOnClickListener {
            views.cameraPreview.controller?.let { controller ->
                toggleCamera(controller)
            }
        }

        views.cameraFlashButton.setOnClickListener {
            cameraViewModel.switchFlash()
        }

        views.cameraModeSwitch.setOnCheckedStateChangeListener { _, checkedIds ->
            if (checkedIds.contains(im.vector.app.R.id.cameraModePhotoButton)) {
                cameraViewModel.switchMode(CameraMode.PHOTO)
            } else if (checkedIds.contains(im.vector.app.R.id.cameraModeVideoButton)) {
                cameraViewModel.switchMode(CameraMode.VIDEO)
            }
        }

        views.capturePhotoButton.setOnClickListener {
            takePicture()
        }

        views.captureVideoButton.setOnClickListener {
            if (checkPermissions(listOf(Manifest.permission.RECORD_AUDIO), requireActivity(), permissionVideoLauncher, CommonStrings.permissions_rationale_msg_record_audio_for_video) { _, _ ->
                        cameraViewModel.enableAudio(false)
                        captureVideo(false)
                    }
            ) {
                cameraViewModel.enableAudio(true)
                captureVideo(true)
            }
        }

        views.stopCaptureVideoButton.setOnClickListener {
            configureCameraForImageCapture()
        }
        val duration = Duration.ofMillis(maxVideoDuratonInMillis.toLong())
        views.videoTimer.text = String.format("%02d:%02d", duration.toMinutesPart(), duration.toSecondsPart())
    }

    @SuppressLint("MissingPermission")
    private fun captureVideo(isAudioEnabled: Boolean) {
        cameraViewModel.startCapture()
        val outputOptions = MediaStoreOutputOptions.Builder(
                requireContext().contentResolver,
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
                .setDurationLimitMillis(maxVideoDuratonInMillis.toLong())
                .build()

        views.cameraPreview.controller?.startRecording(
                outputOptions, if (isAudioEnabled) AudioConfig.create(true) else AudioConfig.AUDIO_DISABLED, cameraExecutor
        ) { videoRecordEvent ->
            if (videoRecordEvent is VideoRecordEvent.Finalize) {
                cameraViewModel.stopCapture()

                if (!videoRecordEvent.hasError() || videoRecordEvent.error == VideoRecordEvent.Finalize.ERROR_DURATION_LIMIT_REACHED) {
                    activity?.setResult(CameraActivity.RESULT_OK_VIDEO, Intent().apply { data = videoRecordEvent.outputResults.outputUri })
                    activity?.finish()
                } else {
                    videoRecordEvent.cause?.let { showErrorInSnackbar(it) }
                }
            } else if (videoRecordEvent is VideoRecordEvent.Status) {
                requireActivity().runOnUiThread {
                    val progressInMillis = TimeUnit.NANOSECONDS.toMillis(videoRecordEvent.recordingStats.recordedDurationNanos)
                    views.cameraProgress.progress = progressInMillis.toInt()
                    val progressDuration = Duration.ofMillis(maxVideoDuratonInMillis.toLong()).minus(Duration.ofMillis(progressInMillis))
                    views.videoTimer.text = String.format("%02d:%02d", progressDuration.toMinutesPart(), progressDuration.toSecondsPart())
                }
            }
        }
    }

    private fun refreshFlashModeIcon(isFlashOn: Boolean) {
        views.cameraFlashButton.setImageResource(
                if (isFlashOn) im.vector.app.R.drawable.ic_camera_flash_on else im.vector.app.R.drawable.ic_camera_flash_off
        )
    }

    private fun takePicture() {
        cameraViewModel.startCapture()
        val photoUri = CameraPicker.createPhotoUri(requireContext())

        val metadata = ImageCapture.Metadata().apply {
            isReversedHorizontal = false
        }

        // Use 'let' instead of 'use' to prevent closing OutputStream before ImageCapture finishes it's job.
        requireContext().contentResolver.openOutputStream(photoUri)?.let { outputStream ->
            val outputOptions = ImageCapture.OutputFileOptions.Builder(outputStream)
                    .setMetadata(metadata)
                    .build()

            // views.cameraPreview.controller?.targetRotation = currentOrientation
            views.cameraPreview.controller?.takePicture(
                    outputOptions, cameraExecutor,
                        object : ImageCapture.OnImageSavedCallback {
                override fun onError(e: ImageCaptureException) {
                    cameraViewModel.stopCapture()
                    outputStream.close()
                    showErrorInSnackbar(e)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    outputStream.close()
                    cameraViewModel.stopCapture()
                    val savedUri = output.savedUri ?: photoUri
                    activity?.setResult(CameraActivity.RESULT_OK_PICTURE, Intent().apply { data = savedUri })
                    activity?.finish()
                }
            }
            )
        }
    }
}
