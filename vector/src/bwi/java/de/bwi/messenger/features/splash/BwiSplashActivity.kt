/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.splash

import android.animation.Animator
import android.animation.Animator.AnimatorListener
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.lottie.LottieAnimationView
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.R
import im.vector.app.core.di.ActiveSessionHolder
import im.vector.app.features.MainActivity
import javax.inject.Inject

@AndroidEntryPoint
class BwiSplashActivity : AppCompatActivity() {

    @Inject lateinit var activeSessionHolder: ActiveSessionHolder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_bwi_splash)

        if (activeSessionHolder.hasActiveSession()) {
            launchMainActivity()
        } else {
            val lottieView = findViewById<LottieAnimationView>(R.id.animationView)
            if (lottieView == null) {
                launchMainActivity()
            }

            lottieView.setOnClickListener { v ->
                (v as LottieAnimationView).cancelAnimation()
            }

            lottieView.addAnimatorListener(object : AnimatorListener {
                override fun onAnimationStart(p0: Animator) {
                }

                override fun onAnimationEnd(p0: Animator) {
                    if (!isFinishing) {
                        launchMainActivity()
                    }
                }

                override fun onAnimationCancel(p0: Animator) {
                    if (!isFinishing) {
                        launchMainActivity()
                    }
                }

                override fun onAnimationRepeat(p0: Animator) {
                }
            })
            lottieView.playAnimation()
        }
    }

    private fun launchMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
