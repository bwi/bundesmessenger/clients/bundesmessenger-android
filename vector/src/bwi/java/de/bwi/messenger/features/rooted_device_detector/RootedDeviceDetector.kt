/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.rooted_device_detector

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.scottyab.rootbeer.RootBeer
import de.bwi.messenger.configuration.AppConfiguration
import im.vector.app.R
import im.vector.app.core.di.ActiveSessionHolder
import im.vector.app.core.di.DefaultPreferences
import im.vector.app.core.utils.deleteAllFiles
import im.vector.lib.strings.CommonStrings
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.ref.WeakReference
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.system.exitProcess

interface RootedDeviceDetector {
    suspend fun performRootDetectionIfEnabled()
    fun setCurrentActivity(activity: Activity)
}

@Singleton
class RootBeerDetector @Inject constructor(
    private val context: Context,
                                           private val sessionHolder: ActiveSessionHolder,
                                           @DefaultPreferences
                                           private val sharedPreferences: SharedPreferences,
                                           private val appConfiguration: AppConfiguration
) : RootedDeviceDetector {

    private var weakCurrentActivity: WeakReference<Activity>? = null
    private var isRooted: Boolean = false

    override fun setCurrentActivity(activity: Activity) {
        weakCurrentActivity = WeakReference(activity)
        if (isRooted) {
            (activity as LifecycleOwner).lifecycleScope.launch {
                showDialog(activity)
            }
        }
    }

    override suspend fun performRootDetectionIfEnabled() {
        if (appConfiguration.enableRootedDeviceDetector) {
            isRooted = RootBeer(context).isRooted
            if (isRooted) {
                weakCurrentActivity?.get()?.let {
                    (it as LifecycleOwner).lifecycleScope.launch {
                        showDialog(it)
                    }
                }
            }
        }
    }

    private suspend fun showDialog(activity: Activity) {
        withContext(Dispatchers.Main) {
            AlertDialog.Builder(activity)
                    .setView(R.layout.dialog_bwi_rooted_device_detected)
                    .setNegativeButton(CommonStrings.action_close) { _, _ ->
                        activity.finishAndRemoveTask()
                        (activity as LifecycleOwner).lifecycleScope.launch {
                            signout()
                            removeTemporaryData()
                        }
                    }
                    .setCancelable(false)
                    .create()
                    .show()
        }
    }

    private suspend fun removeTemporaryData() {
        // Cleanup local cache and files
        Glide.get(context).clearMemory()
        withContext(Dispatchers.IO) {
            sharedPreferences.edit().clear().commit()
            Glide.get(context).clearDiskCache()
            deleteAllFiles(context.cacheDir)
            deleteAllFiles(context.filesDir)
            deleteAllFiles(context.noBackupFilesDir)
            sharedPreferences.all.clear()
        }
        exitProcess(0)
    }

    private suspend fun signout() {
        sessionHolder.getSafeActiveSession()?.let {
            kotlin.runCatching {
                it.signOutService().signOut(true)
                sessionHolder.clearActiveSession()
            }
        }
    }
}
