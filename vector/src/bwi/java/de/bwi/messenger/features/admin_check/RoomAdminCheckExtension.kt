/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.admin_check

import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.members.roomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.PowerLevelsContent
import org.matrix.android.sdk.api.session.room.powerlevels.PowerLevelsHelper
import org.matrix.android.sdk.api.session.room.powerlevels.Role

fun Room.canBeLeft(myUserId: String): Boolean {
    val roomMemberQueryParams = roomMemberQueryParams {
        memberships = Membership.activeMemberships()
    }
    val members = membershipService().getRoomMembers(roomMemberQueryParams)
    val powerLevelsContent = stateService().getStateEvent(EventType.STATE_ROOM_POWER_LEVELS, QueryStringValue.IsEmpty)
            ?.content
            ?.toModel<PowerLevelsContent>()
            ?: PowerLevelsContent()
    val powerLevelsHelper = PowerLevelsHelper(powerLevelsContent)
    return powerLevelsHelper.getUserRole(myUserId) != Role.Admin || // I can leave if I'm not an admin
            members.filter { it.userId != myUserId }.any { powerLevelsHelper.getUserRole(it.userId) == Role.Admin } || // I can leave, if there are other admins in the room
            members.none { it.userId != myUserId } // I can leave, if I am the last user in the room
}

fun Room.canDowngradeUser(myUserId: String): Boolean {
    val roomMemberQueryParams = roomMemberQueryParams {
        memberships = Membership.activeMemberships()
    }
    val members = membershipService().getRoomMembers(roomMemberQueryParams)
    val powerLevelsContent = stateService().getStateEvent(EventType.STATE_ROOM_POWER_LEVELS, QueryStringValue.IsEmpty)
            ?.content
            ?.toModel<PowerLevelsContent>()
            ?: PowerLevelsContent()
    val powerLevelsHelper = PowerLevelsHelper(powerLevelsContent)
    return powerLevelsHelper.getUserRole(myUserId) != Role.Admin || // I can downgrade myself if I'm not an admin
            members.filter { it.userId != myUserId }.any { powerLevelsHelper.getUserRole(it.userId) == Role.Admin } // I can downgrade myself, if there are other admins in the room
}
