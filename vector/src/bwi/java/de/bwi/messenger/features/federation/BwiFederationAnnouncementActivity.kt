/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.epoxy.onClick
import im.vector.app.core.platform.VectorBaseActivity
import im.vector.app.databinding.ActivityBwiFederationAnnouncementBinding
import org.matrix.android.sdk.api.session.Session
import javax.inject.Inject

@AndroidEntryPoint
class BwiFederationAnnouncementActivity : VectorBaseActivity<ActivityBwiFederationAnnouncementBinding>() {

    @Inject lateinit var session: Session

    override fun getBinding() = ActivityBwiFederationAnnouncementBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        views.federationAnnouncementActionConfirm.onClick {
            finish()
        }
        views.federationAnnouncementCloseButton.onClick {
            finish()
        }
    }
}
