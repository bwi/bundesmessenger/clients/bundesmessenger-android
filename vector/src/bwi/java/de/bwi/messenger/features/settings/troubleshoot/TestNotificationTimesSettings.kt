/*
 * Copyright 2018 New Vector Ltd
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.bwi.messenger.features.settings.troubleshoot

import androidx.fragment.app.FragmentActivity
import im.vector.app.core.di.ActiveSessionHolder
import im.vector.app.core.resources.StringProvider
import im.vector.app.features.navigation.Navigator
import im.vector.app.features.settings.VectorSettingsActivity
import im.vector.app.features.settings.troubleshoot.TroubleshootTest
import im.vector.lib.strings.CommonStrings
import javax.inject.Inject

/**
 * Checks if notifications are enable in the system settings for this app.
 */
class TestNotificationTimesSettings @Inject constructor(
        private val activeSessionHolder: ActiveSessionHolder,
        private val navigator: Navigator,
        private val stringProvider: StringProvider,
        private val context: FragmentActivity,
) :
        TroubleshootTest(CommonStrings.settings_troubleshoot_test_notification_times_settings) {

    override fun perform(testParameters: TestParameters) {
        activeSessionHolder.getSafeActiveSession()?.let { session ->
            if (session.workingTimeService().isWorkingTimeEnabled()) {
                description = stringProvider.getString(CommonStrings.settings_troubleshoot_test_notification_times_active)
                quickFix = object : TroubleshootQuickFix(CommonStrings.settings_troubleshoot_test_notification_times_quickfix) {
                    override fun doFix() {
                        navigator.openSettings(context, directAccess = VectorSettingsActivity.EXTRA_DIRECT_ACCESS_WORKING_TIMES)
                    }
                }
                status = TestStatus.FAILED
            }
        }
    }
}
