/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.extensions.addFragment
import im.vector.app.core.platform.ScreenOrientationLocker
import im.vector.app.core.platform.VectorBaseActivity
import im.vector.app.databinding.ActivityBwiFederationIntroductionBinding
import javax.inject.Inject

@AndroidEntryPoint
class BwiFederationIntroductionActivity : VectorBaseActivity<ActivityBwiFederationIntroductionBinding>() {

    @Inject lateinit var orientationLocker: ScreenOrientationLocker
    override fun getBinding() = ActivityBwiFederationIntroductionBinding.inflate(layoutInflater)

    override fun initUiAndData() {
        if (isFirstCreation()) {
            addFragment(views.federationIntroFragmentContainer, BwiFederationIntroductionCarouselFragment::class.java)
            orientationLocker.lockPhonesToPortrait(this)
        }
    }

    @SuppressLint("MissingSuperCall")
    @Suppress("OVERRIDE_DEPRECATION")
    override fun onBackPressed() {
        // user needs press 'Got it' button to close the activity
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, BwiFederationIntroductionActivity::class.java)
        }
    }
}
