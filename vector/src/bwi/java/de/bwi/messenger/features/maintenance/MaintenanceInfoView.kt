/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.annotation.VisibleForTesting
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import im.vector.app.BuildConfig
import im.vector.app.R

class MaintenanceInfoView : Preference {
    private var message: String? = null
    private var backgroundColor: Int = 0
    private var textColor: Int = 0

    private var ignoreClickListener: View.OnClickListener? = null
    private var showIgnoreButton: Boolean = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        holder.itemView.findViewById<TextView>(R.id.maintenanceMessage)?.text = message
        holder.itemView.findViewById<CardView>(R.id.maintenanceBackground)?.setCardBackgroundColor(backgroundColor)
        holder.itemView.findViewById<TextView>(R.id.maintenanceMessage)?.setTextColor(textColor)
        holder.itemView.findViewById<Button>(R.id.maintenanceIgnore)?.apply {
            this.isVisible = showIgnoreButton
            this.setOnClickListener {
                showIgnoreButton = false
                ignoreClickListener?.onClick(it)
                notifyChanged()
            }
        }
    }

    fun configure(message: String, backgroundColor: Int, textColor: Int, showIgnoreButton: Boolean = false, ignoreClickListener: View.OnClickListener? = null) {
        this.message = message
        this.backgroundColor = backgroundColor
        this.textColor = textColor
        this.showIgnoreButton = showIgnoreButton && ignoreButtonEnabled(BuildConfig.FLAVOR)
        this.ignoreClickListener = ignoreClickListener
        notifyChanged()
    }

    companion object {
        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        fun ignoreButtonEnabled(flavor: String): Boolean = flavor.contains("Beta", true)
    }
}
