/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.poll

import com.airbnb.mvrx.MavericksState
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary

data class VotedUser(
        val userSummary: RoomMemberSummary,
        val voteTimeStamp: Long,
)
data class PollAnswerVotes(
        val answerId: String,
        val answer: String,
        val votesCount: Int,
        val votedUsers: List<VotedUser> = emptyList(),
        val isWinner: Boolean = false,
        val showAllVoters: Boolean = false
)

data class BwiPollParticipantsViewState(
        val roomId: String,
        val pollId: String,
        val question: String? = null,
        val answers: List<PollAnswerVotes>? = null,
        ) : MavericksState {
    constructor(args: BwiPollParticipantsArgs) : this(
            roomId = args.roomId,
            pollId = args.pollId,
    )
}
