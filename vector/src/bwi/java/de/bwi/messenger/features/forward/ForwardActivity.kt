/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.forward

import android.content.Context
import android.content.Intent
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.extensions.addFragment
import im.vector.app.core.platform.VectorBaseActivity
import im.vector.app.databinding.ActivitySimpleBinding
import im.vector.app.features.share.IncomingShareFragment
import im.vector.app.features.share.IncomingShareFragmentArgs

@AndroidEntryPoint
class ForwardActivity : VectorBaseActivity<ActivitySimpleBinding>() {
    override fun getBinding() = ActivitySimpleBinding.inflate(layoutInflater)

    override fun getCoordinatorLayout() = views.coordinatorLayout

    override fun initUiAndData() {
        if (isFirstCreation()) {
            addFragment(views.simpleFragmentContainer, IncomingShareFragment::class.java, IncomingShareFragmentArgs(true))
        }
    }

    companion object {
        fun createIntent(context: Context): Intent {
            val intent = Intent(context, ForwardActivity::class.java)
            intent.action = Intent.ACTION_SEND
            return intent
        }
    }
}
