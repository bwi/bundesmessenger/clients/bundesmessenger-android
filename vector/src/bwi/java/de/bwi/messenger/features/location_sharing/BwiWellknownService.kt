/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.location_sharing

import im.vector.app.features.raw.wellknown.E2EWellKnownConfig
import im.vector.app.features.raw.wellknown.ElementWellKnown
import im.vector.app.features.raw.wellknown.ElementWellKnownMapper
import im.vector.app.features.settings.VectorPreferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.auth.data.SessionParams
import org.matrix.android.sdk.api.extensions.tryOrNull
import org.matrix.android.sdk.api.raw.RawService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BwiWellknownService @Inject constructor(
        private val rawService: RawService,
        private val vectorPreferences: VectorPreferences
) {
    private var elementWellKnown: ElementWellKnown? = null

    // TODO temporary function for compatibility with RawService
    fun withElementWellKnown(
            coroutineScope: CoroutineScope,
            sessionParams: SessionParams,
            block: ((ElementWellKnown?) -> Unit)
    ) = with(coroutineScope) {
        launch {
            block(getElementWellknown(sessionParams.homeServerUrl))
        }
    }

    suspend fun getElementWellknown(baseUrl: String): ElementWellKnown {
        elementWellKnown = tryOrNull { rawService.getWellknown(baseUrl) }
                ?.let { ElementWellKnownMapper.from(it) }

        return elementWellKnown ?: ElementWellKnown(
                elementE2E = E2EWellKnownConfig(
                        e2eDefault = true,
                        secureBackupRequired = true,
                        secureBackupSetupMethods = listOf("passphrase"),
                        outboundsKeyPreSharingMode = "on_room_opening"
                )
        )
    }

    fun isBwiFederationEnabled(): Boolean {
        return vectorPreferences.isBwiFederationEnabled() || elementWellKnown?.bwiWellKnownConfig?.bwiFederationConfig?.isEnabled == true
    }

    fun getMapStyleUrl(): String {
        return elementWellKnown?.mapTileServerConfig?.mapStyleUrl ?: ""
    }
}
