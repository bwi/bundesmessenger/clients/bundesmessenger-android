/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.rooted_device_detector

import android.content.Context
import com.scottyab.rootbeer.RootBeer
import com.scottyab.rootbeer.util.QLog
import java.io.IOException
import java.util.HashMap
import java.util.NoSuchElementException
import java.util.Scanner

class BwiRootBeer(context: Context) : RootBeer(context) {
    override fun checkForDangerousProps(): Boolean {
        val dangerousProps: MutableMap<String, String> = HashMap()
        dangerousProps["ro.secure"] = "0"

        var result = false

        val lines = propsReader()
                ?: // Could not read, assume false;
                return false

        for (line in lines) {
            for (key in dangerousProps.keys) {
                if (line?.contains(key) == true) {
                    var badValue = dangerousProps[key]
                    badValue = "[$badValue]"
                    if (line.contains(badValue)) {
                        QLog.v("$key = $badValue detected!")
                        result = true
                    }
                }
            }
        }
        return result
    }

    private fun propsReader(): Array<String?>? {
        return try {
            val inputstream = Runtime.getRuntime().exec("getprop").inputStream ?: return null
            val propVal = Scanner(inputstream).useDelimiter("\\A").next()
            propVal.split("\n").toTypedArray()
        } catch (e: IOException) {
            QLog.e(e)
            null
        } catch (e: NoSuchElementException) {
            QLog.e(e)
            null
        }
    }
}
