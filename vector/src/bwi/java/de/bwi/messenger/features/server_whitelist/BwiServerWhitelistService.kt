/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.server_whitelist

import android.content.Context
import com.google.gson.Gson
import im.vector.app.core.utils.ensureProtocol
import im.vector.app.core.utils.ensureTrailingSlash
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.matrix.android.sdk.api.cache.CacheStrategy
import org.matrix.android.sdk.api.raw.RawService
import org.matrix.android.sdk.api.util.sha256
import timber.log.Timber
import java.io.InputStreamReader
import java.security.NoSuchAlgorithmException
import java.security.PublicKey
import java.security.spec.InvalidKeySpecException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BwiServerWhitelistService @Inject constructor(
        private val rawService: RawService,
        private val context: Context,
        private val connectionDetectionHelper: ConnectionDetectionHelper
) {
    private val publicKeys = listOf("public-key-b.pub", "public-key-k.pub", "testing.pub")

    suspend fun getWhitelistState(hostName: String): WhiteListState {
        try {
            var json: MutableList<String>?
            val isConnected = connectionDetectionHelper.isNetworkConnected()
            if (!isConnected) {
                return WhiteListState.NoConnection
            }
            withContext(Dispatchers.IO) {
                val response = rawService.getUrl("${hostName.ensureProtocol().ensureTrailingSlash()}_bum/client/v1/verify", CacheStrategy.NoCache)

                json = Gson().fromJson(response, mutableListOf<String>().javaClass)
            }

            val returnValue = (publicKeys).any { key ->
                json?.forEach { jwt ->
                    try {
                        val parser = Jwts.parser().provider(BouncyCastleProvider()).verifyWith(fileToPublicKey(key)).build()
                        val parsedJwt = (parser.parse(jwt.replace("\n", "")).payload as Claims)

                        val remoteHomeServerUrl = parsedJwt.subject
                        if (hostName.contains(remoteHomeServerUrl)) {
                            return WhiteListState.IsAllowed
                        }
                    } catch (e: Throwable) {
                        if (e is io.jsonwebtoken.ExpiredJwtException && json?.indexOf(jwt) == json?.lastIndex) {
                            return WhiteListState.TokenInvalid
                        }
                        Timber.e(e)
                    }
                }
                false
            }

            return returnValue.toWhiteListState()
        } catch (e: Throwable) {
            Timber.w("throwable: $e")
            val allowedServerHashes = context.resources.getStringArray(im.vector.app.config.R.array.allowed_matrix_hosts)
            val containsServerHashes = allowedServerHashes.contains(hostName.sha256()).toWhiteListState()
            return containsServerHashes
        }
    }

    @Throws(InvalidKeySpecException::class, NoSuchAlgorithmException::class)
    private fun fileToPublicKey(filename: String): PublicKey {
        val inputStream = context.assets.open(filename)
        val parser = PEMParser(InputStreamReader(inputStream))
        val converter = JcaPEMKeyConverter()
        val publicKeyInfo: SubjectPublicKeyInfo = SubjectPublicKeyInfo.getInstance(parser.readObject())
        inputStream.reset() // for unit-tests
        return converter.getPublicKey(publicKeyInfo)
    }

    // To Reduce the Cognitive Complexity
    private fun Boolean.toWhiteListState(): WhiteListState {
        return if (this) {
            WhiteListState.IsAllowed
        } else {
            WhiteListState.NotAllowed
        }
    }
}

sealed class WhiteListState {
    object IsAllowed : WhiteListState()
    object NotAllowed : WhiteListState()
    object TokenInvalid : WhiteListState()
    object NoConnection : WhiteListState()
}
