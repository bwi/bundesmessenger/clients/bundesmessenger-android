/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.status_message

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.core.view.isVisible
import androidx.preference.EditTextPreference
import androidx.preference.EditTextPreferenceDialogFragmentCompat
import im.vector.app.R
import im.vector.app.core.epoxy.onClick

class StatusMessageDialogFragment : EditTextPreferenceDialogFragmentCompat() {
    fun newInstance(key: String): StatusMessageDialogFragment {
        return StatusMessageDialogFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_KEY, key)
            }
        }
    }

    override fun onBindDialogView(view: View) {
        super.onBindDialogView(view)
        val statusCompletions = requireContext().resources.getStringArray(R.array.status_message_choices)
        view.findViewById<AutoCompleteTextView>(android.R.id.edit)?.apply {
            setAdapter(
                ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_dropdown_item_1line,
                    statusCompletions
                )
            )
        }
        view.findViewById<Button>(R.id.deleteButton)?.apply {
          isVisible = (preference as EditTextPreference).text != null
        }?.onClick {
            preference.callChangeListener(null)
            (preference as EditTextPreference).text = null
            dismiss()
        }
    }
}
