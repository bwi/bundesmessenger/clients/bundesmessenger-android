/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.poll

import com.airbnb.epoxy.TypedEpoxyController
import im.vector.app.core.date.VectorDateFormatter
import im.vector.app.core.epoxy.dividerItem
import im.vector.app.core.resources.StringProvider
import im.vector.app.core.ui.list.genericButtonItem
import im.vector.app.features.home.AvatarRenderer
import im.vector.lib.strings.CommonPlurals
import im.vector.lib.strings.CommonStrings
import org.matrix.android.sdk.api.util.toMatrixItem
import javax.inject.Inject

interface BwiPollParticipantsControllerCallback {
    fun onToggleAllVotersClicked(answerId: String)
}

class BwiPollParticipantsController @Inject constructor(
        val stringProvider: StringProvider,
        val avatarRenderer: AvatarRenderer,
        val vectorDateFormatter: VectorDateFormatter,
) : TypedEpoxyController<BwiPollParticipantsViewState>() {

    var callback: BwiPollParticipantsControllerCallback? = null

    override fun buildModels(data: BwiPollParticipantsViewState?) {
        data?.let { state ->
            pollQuestionItem {
                id("question")
                question(state.question ?: "")
            }

            state.answers?.forEach { answerInfo ->
                val formattedVotesCount = stringProvider.getQuantityString(CommonPlurals.poll_option_vote_count, answerInfo.votesCount, answerInfo.votesCount)
                pollAnswerItem {
                    id(answerInfo.answerId)
                    answer(answerInfo.answer)
                    winner(answerInfo.isWinner)
                    formattedVotesCount(formattedVotesCount)
                }
                val votersToShowCount = if (answerInfo.showAllVoters) answerInfo.votedUsers.size else 5
                val host = this
                if (answerInfo.votedUsers.isNotEmpty()) {
                    dividerItem { id("divider_${answerInfo.answerId}") }
                }
                answerInfo.votedUsers.take(votersToShowCount).forEach { user ->
                    pollVoterItem {
                        id(user.userSummary.userId)
                        matrixItem(user.userSummary.toMatrixItem())
                        avatarRenderer(host.avatarRenderer)
                        formattedTimestamp(host.vectorDateFormatter.formatPollParticipationDate(user.voteTimeStamp))
                    }
                }
                if ((!answerInfo.showAllVoters && answerInfo.votedUsers.size > votersToShowCount) || answerInfo.showAllVoters) {
                    genericButtonItem {
                        id("toggle_all_voters_${answerInfo.answerId}")
                        text(
                                if (answerInfo.showAllVoters)
                                    host.stringProvider.getString(CommonStrings.show_less_voters)
                                else
                                    host.stringProvider.getString(CommonStrings.show_all_voters, answerInfo.votedUsers.size - votersToShowCount)
                        )
                        buttonClickAction { host.callback?.onToggleAllVotersClicked(answerId = answerInfo.answerId) }
                    }
                }
            }
        }
    }
}
