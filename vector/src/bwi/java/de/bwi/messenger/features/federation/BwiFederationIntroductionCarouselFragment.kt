/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.viewpager2.widget.ViewPager2
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.platform.VectorBaseFragment
import im.vector.app.databinding.FragmentFederationIntroductionCarouselBinding
import im.vector.app.features.VectorFeatures
import im.vector.app.features.onboarding.ftueauth.SplashCarouselController
import im.vector.app.features.onboarding.ftueauth.SplashCarouselState
import im.vector.app.features.settings.VectorPreferences
import kotlinx.coroutines.Job
import javax.inject.Inject

@AndroidEntryPoint
class BwiFederationIntroductionCarouselFragment :
        VectorBaseFragment<FragmentFederationIntroductionCarouselBinding>() {

    private val viewModel: FederationIntroductionViewModel by fragmentViewModel()

    @Inject lateinit var vectorPreferences: VectorPreferences
    @Inject lateinit var vectorFeatures: VectorFeatures
    @Inject lateinit var carouselController: SplashCarouselController
    @Inject lateinit var carouselStateFactory: FederationIntroductionCarouselStateFactory
    private lateinit var carouselState: SplashCarouselState

    private var tabLayoutMediator: TabLayoutMediator? = null

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentFederationIntroductionCarouselBinding {
        return FragmentFederationIntroductionCarouselBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        viewModel.observeViewEvents {
            handleViewEvents(it)
        }
    }

    override fun onDestroyView() {
        tabLayoutMediator?.detach()
        tabLayoutMediator = null
        views.splashCarousel.adapter = null
        super.onDestroyView()
    }

    private fun setupViews() {
        val carouselAdapter = carouselController.adapter
        views.splashCarousel.adapter = carouselAdapter
        tabLayoutMediator = TabLayoutMediator(views.carouselIndicator, views.splashCarousel) { _, _ -> }
                .also { it.attach() }

        carouselState = carouselStateFactory.create()
        carouselController.setData(carouselState)

        views.buttonGotIt.debouncedClicks { buttonGotItClicked() }

        views.splashCarousel.registerAutomaticUntilInteractionTransitions()
    }

    private fun buttonGotItClicked() {
        viewModel.handle(FederationIntroductionAction.GotIt)
    }

    override fun invalidate() = withState(viewModel) { state ->
        views.buttonGotIt.isVisible = state.gotItButtonVisible
    }

    private fun handleViewEvents(event: FederationIntroductionViewEvents?) {
        when (event) {
            FederationIntroductionViewEvents.Finish -> requireActivity().finish()
        }
    }

    private fun ViewPager2.registerAutomaticUntilInteractionTransitions() {
        val scheduledTransition: Job? = null
        val pageChangingCallback = object : ViewPager2.OnPageChangeCallback() {
            private var hasUserManuallyInteractedWithCarousel: Boolean = false

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                hasUserManuallyInteractedWithCarousel = !isFakeDragging
            }

            override fun onPageSelected(position: Int) {
                viewModel.handle(
                    FederationIntroductionAction.PageSelected(
                        pageIndex = position,
                        totalPages = carouselState.items.size,
                )
                )
                scheduledTransition?.cancel()
            }
        }
        viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onCreate(owner: LifecycleOwner) {
                registerOnPageChangeCallback(pageChangingCallback)
            }

            override fun onDestroy(owner: LifecycleOwner) {
                unregisterOnPageChangeCallback(pageChangingCallback)
            }
        })
    }
}
