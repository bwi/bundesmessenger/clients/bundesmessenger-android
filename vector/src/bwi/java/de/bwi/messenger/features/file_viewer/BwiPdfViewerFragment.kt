/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.file_viewer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.airbnb.mvrx.args
import dagger.hilt.android.AndroidEntryPoint
import de.bwi.messenger.configuration.AppConfiguration
import im.vector.app.core.platform.VectorBaseFragment
import im.vector.app.core.utils.toast
import im.vector.app.databinding.FragmentBwiViewerPdfBinding
import im.vector.lib.strings.CommonStrings
import javax.inject.Inject

@AndroidEntryPoint
class BwiPdfViewerFragment : VectorBaseFragment<FragmentBwiViewerPdfBinding>() {

    @Inject
    lateinit var appConfiguration: AppConfiguration

    private val args: BwiFileViewerActivityArgs by args()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        kotlin.runCatching {
            views.pdfView.initWithUri(args.file)
        }.onFailure {
            if (it is SecurityException) {
                activity?.toast(getString(CommonStrings.pdf_open_password_error))
            } else {
                activity?.toast(getString(CommonStrings.pdf_open_error))
            }
            activity?.finish()
        }
    }

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentBwiViewerPdfBinding {
        return FragmentBwiViewerPdfBinding.inflate(inflater, container, false)
    }
}
