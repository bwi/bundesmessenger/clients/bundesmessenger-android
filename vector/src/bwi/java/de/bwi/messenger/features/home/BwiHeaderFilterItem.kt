/*
 * Copyright (c) 2022 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.home

import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import im.vector.app.R
import im.vector.app.core.epoxy.ClickListener
import im.vector.app.core.epoxy.VectorEpoxyHolder
import im.vector.app.core.epoxy.VectorEpoxyModel
import im.vector.app.core.epoxy.onClick

@EpoxyModelClass
abstract class BwiHeaderFilterItem : VectorEpoxyModel<BwiHeaderFilterItem.Holder>(R.layout.item_bwi_filter_header) {

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var filterClickListener: ClickListener? = null

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var searchClickListener: ClickListener? = null

    @EpoxyAttribute
    var filterOn: Boolean = false

    override fun bind(holder: Holder) {
        super.bind(holder)

        holder.filterButton.isActivated = filterOn
        holder.filterButton.onClick(filterClickListener)
        holder.searchButton.onClick(searchClickListener)
    }

    override fun unbind(holder: Holder) {
        holder.filterButton.setOnClickListener(null)
        holder.searchButton.setOnClickListener(null)

        super.unbind(holder)
    }

    class Holder : VectorEpoxyHolder() {
        val filterButton by bind<ImageView>(R.id.filterToggle)
        val searchButton by bind<TextView>(R.id.searchButton)
    }
}
