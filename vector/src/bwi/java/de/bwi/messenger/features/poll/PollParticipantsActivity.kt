/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.poll

import android.content.Context
import android.content.Intent
import com.airbnb.mvrx.Mavericks
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.core.extensions.addFragment
import im.vector.app.core.platform.SimpleFragmentActivity
import im.vector.lib.core.utils.compat.getParcelableCompat

@AndroidEntryPoint
class PollParticipantsActivity : SimpleFragmentActivity() {

    override fun initUiAndData() {
        super.initUiAndData()
        val args = intent.extras?.getParcelableCompat<BwiPollParticipantsArgs>(Mavericks.KEY_ARG)
        if (isFirstCreation()) {
            addFragment(
                    views.container,
                    BwiPollParticipantsFragment::class.java,
                    args
            )
        }
    }

    companion object {
        fun newIntent(context: Context, roomId: String, pollId: String): Intent {
            return Intent(context, PollParticipantsActivity::class.java).apply {
                putExtra(Mavericks.KEY_ARG, BwiPollParticipantsArgs(roomId, pollId))
            }
        }
    }
}
