/*
 * Copyright 2019 New Vector Ltd
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package im.vector.app.features.home.room.list.actions

import android.content.Context
import android.text.format.DateFormat
import androidx.annotation.StringRes
import com.airbnb.epoxy.TypedEpoxyController
import de.bwi.messenger.configuration.AppConfiguration
import im.vector.app.core.epoxy.bottomSheetDividerItem
import im.vector.app.core.epoxy.bottomsheet.bottomSheetActionItem
import im.vector.app.core.epoxy.bottomsheet.bottomSheetRoomPreviewItem
import im.vector.app.core.epoxy.profiles.notifications.radioButtonItem
import im.vector.app.core.resources.ColorProvider
import im.vector.app.core.resources.LocaleProvider
import im.vector.app.core.resources.StringProvider
import im.vector.app.features.home.AvatarRenderer
import im.vector.app.features.roomprofile.notifications.notificationOptions
import im.vector.app.features.roomprofile.notifications.notificationStateMapped
import im.vector.lib.strings.CommonStrings
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.util.toMatrixItem
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

/**
 * Epoxy controller for room list actions.
 */
class RoomListQuickActionsEpoxyController @Inject constructor(
        private val avatarRenderer: AvatarRenderer,
        private val colorProvider: ColorProvider,
        private val stringProvider: StringProvider,
        private val localeProvider: LocaleProvider,
        private val context: Context,
        private val appConfiguration: AppConfiguration,
        private val session: Session,
) : TypedEpoxyController<RoomListQuickActionViewState>() {

    var listener: Listener? = null
    private val hourFormatter by lazy {
        if (DateFormat.is24HourFormat(context)) {
            DateTimeFormatter.ofPattern("HH:mm", localeProvider.current())
        } else {
            DateTimeFormatter.ofPattern("h:mm a", localeProvider.current())
        }
    }

    override fun buildModels(state: RoomListQuickActionViewState) {
        val notificationViewState = state.notificationSettingsViewState
        val roomSummary = notificationViewState.roomSummary() ?: return
        val host = this

        val shouldNavigateToWorkingtimeSetting = !notificationViewState.isWorkingTimeSettingEnabled

        // Preview, favorite, settings
        bottomSheetRoomPreviewItem(appConfiguration) {
            id("room_preview")
            avatarRenderer(host.avatarRenderer)
            matrixItem(roomSummary.toMatrixItem())
            stringProvider(host.stringProvider)
            colorProvider(host.colorProvider)
            izLowPriority(roomSummary.isLowPriority)
            izFavorite(roomSummary.isFavorite)
            izPersonalNotesRoom(roomSummary.roomId == host.session.personalNotesService().getPersonalNotesRoomId())
            izWorkingTime(notificationViewState.isWorkingTimeEnabledForRoom)
            settingsClickListener { host.listener?.didSelectMenuAction(RoomListQuickActionsSharedAction.Settings(roomSummary.roomId)) }
            favoriteClickListener { host.listener?.didSelectMenuAction(RoomListQuickActionsSharedAction.Favorite(roomSummary.roomId)) }
            lowPriorityClickListener { host.listener?.didSelectMenuAction(RoomListQuickActionsSharedAction.LowPriority(roomSummary.roomId)) }
            workingTimeClickListener { host.listener?.didSelectMenuAction(RoomListQuickActionsSharedAction.WorkTime(roomSummary.roomId, !notificationViewState.isWorkingTimeEnabledForRoom, roomSummary.isDirect, shouldNavigateToWorkingtimeSetting )) }
        }

        // Notifications
        bottomSheetDividerItem {
            id("notifications_separator")
        }

        if(session.personalNotesService().getPersonalNotesRoomId() != roomSummary.roomId) {
            notificationViewState.notificationOptions.forEach { notificationState ->
                val value = StringBuilder().append(stringProvider.getString(titleForNotificationState(notificationState) ?: CommonStrings.room_settings_none))

                if (notificationViewState.isWorkingTimeEnabledForRoom && notificationState != RoomNotificationState.MUTE) {
                    val workingTime = notificationViewState.workingTimeConfiguration[LocalDateTime.now().dayOfWeek]
                    if (workingTime?.isEnabled == true) {
                        value.append(" (${hourFormatter.format(workingTime.fromTime)} - ${hourFormatter.format(workingTime.toTime)})")
                    } else {
                        value.append(" (${stringProvider.getString(CommonStrings.day_off)})")
                    }
                }

                radioButtonItem {
                    id(notificationState.name)
                    title(value.toString())
                    description(host.descriptionForNotificationState(notificationState))
                    selected(notificationViewState.notificationStateMapped() == notificationState)
                    listener {
                        host.listener?.didSelectRoomNotificationState(notificationState)
                    }
                }
            }
            RoomListQuickActionsSharedAction.Leave(roomSummary.roomId, showIcon = !true).toBottomSheetItem()
        } else {
            bottomSheetActionItem {
                id("action_hide_notes_room")
                selected(false)
                showIcon(false)
                textRes(CommonStrings.room_list_quick_actions_hide_notes_room)
                destructive(true)
                listener { host.listener?.didSelectMenuAction(RoomListQuickActionsSharedAction.HidePersonalNotesRoom()) }
            }
        }
    }

    @StringRes
    private fun titleForNotificationState(notificationState: RoomNotificationState): Int? = when (notificationState) {
        RoomNotificationState.ALL_MESSAGES       -> CommonStrings.room_settings_standard
        RoomNotificationState.ALL_MESSAGES_NOISY -> CommonStrings.room_settings_all_messages
        RoomNotificationState.MENTIONS_ONLY -> CommonStrings.room_settings_mention_and_keyword_only
        RoomNotificationState.MUTE -> CommonStrings.room_settings_none
        else -> null
    }

    private fun descriptionForNotificationState(notificationState: RoomNotificationState) : String? = when(notificationState) {
        RoomNotificationState.ALL_MESSAGES       -> stringProvider.getString(CommonStrings.room_settings_standard_description)
        else                                     -> null
    }

    private fun RoomListQuickActionsSharedAction.Leave.toBottomSheetItem() {
        val host = this@RoomListQuickActionsEpoxyController
        return bottomSheetActionItem {
            id("action_leave")
            selected(false)
            if (iconResId != null) {
                iconRes(iconResId)
            } else {
                showIcon(false)
            }
            textRes(titleRes)
            destructive(this@toBottomSheetItem.destructive)
            listener { host.listener?.didSelectMenuAction(this@toBottomSheetItem) }
        }
    }

    interface Listener {
        fun didSelectMenuAction(quickAction: RoomListQuickActionsSharedAction)
        fun didSelectRoomNotificationState(roomNotificationState: RoomNotificationState)
    }
}
