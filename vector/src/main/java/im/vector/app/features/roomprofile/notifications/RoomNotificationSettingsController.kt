/*
 * Copyright (c) 2021 New Vector Ltd
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.features.roomprofile.notifications

import android.content.Context
import android.text.format.DateFormat
import androidx.annotation.StringRes
import com.airbnb.epoxy.TypedEpoxyController
import im.vector.app.core.epoxy.profiles.notifications.notificationSettingsFooterItem
import im.vector.app.core.epoxy.profiles.notifications.radioButtonItem
import im.vector.app.core.epoxy.profiles.notifications.textHeaderItem
import im.vector.app.core.resources.LocaleProvider
import im.vector.app.core.resources.StringProvider
import im.vector.lib.strings.CommonStrings
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

class RoomNotificationSettingsController @Inject constructor(
        private val context: Context,
        private val stringProvider: StringProvider,
        private val localeProvider: LocaleProvider,
) : TypedEpoxyController<RoomNotificationSettingsViewState>() {

    interface Callback {
        fun didSelectRoomNotificationState(roomNotificationState: RoomNotificationState)
        fun didSelectAccountSettingsLink()
    }

    var callback: Callback? = null

    override fun buildModels(data: RoomNotificationSettingsViewState?) {
        val host = this
        data ?: return

        val hourFormatter by lazy {
            if (DateFormat.is24HourFormat(context)) {
                DateTimeFormatter.ofPattern("HH:mm", localeProvider.current())
            } else {
                DateTimeFormatter.ofPattern("h:mm a", localeProvider.current())
            }
        }

        textHeaderItem {
            id("roomNotificationSettingsHeader")
            textRes(CommonStrings.room_settings_room_notifications_notify_me)
        }
        data.notificationOptions.forEach {  notificationState ->
            val value = StringBuilder().append(stringProvider.getString(titleForNotificationState(notificationState) ?: CommonStrings.room_settings_none))

            if(data.isWorkingTimeEnabledForRoom && notificationState != RoomNotificationState.MUTE) {
                val workingTime = data.workingTimeConfiguration[LocalDateTime.now().dayOfWeek]
                if(workingTime?.isEnabled == true) {
                    value.append(" (${hourFormatter.format(workingTime.fromTime)} - ${hourFormatter.format(workingTime.toTime)})")
                } else {
                    value.append(" (${stringProvider.getString(CommonStrings.day_off)})")
                }
            }

            radioButtonItem {
                id(notificationState.name)
                title(value.toString())
                description(host.descriptionForNotificationState(notificationState))
                selected(data.notificationStateMapped() == notificationState)
                listener {
                    host.callback?.didSelectRoomNotificationState(notificationState)
                }
            }
        }

        // BWI
//        notificationSettingsFooterItem {
//            id("roomNotificationSettingsFooter")
//            encrypted(data.roomSummary()?.isEncrypted == true)
//            clickListener {
//                host.callback?.didSelectAccountSettingsLink()
//            }
//        }
    }

    @StringRes
    private fun titleForNotificationState(notificationState: RoomNotificationState): Int? = when (notificationState) {
        RoomNotificationState.ALL_MESSAGES       -> CommonStrings.room_settings_standard
        RoomNotificationState.ALL_MESSAGES_NOISY -> CommonStrings.room_settings_all_messages
        RoomNotificationState.MENTIONS_ONLY -> CommonStrings.room_settings_mention_and_keyword_only
        RoomNotificationState.MUTE -> CommonStrings.room_settings_none
        else -> null
    }

    private fun descriptionForNotificationState(notificationState: RoomNotificationState) : String? = when(notificationState) {
        RoomNotificationState.ALL_MESSAGES       -> stringProvider.getString(CommonStrings.room_settings_standard_description)
        else                                     -> null
    }
}
