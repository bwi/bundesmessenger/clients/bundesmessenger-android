/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.features.settings

import android.os.Bundle
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.asFlow
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import im.vector.app.R
import im.vector.app.core.preference.VectorSwitchPreference
import im.vector.app.core.preference.WorkingTimePreference
import im.vector.app.core.resources.LocaleProvider
import im.vector.app.core.utils.toast
import im.vector.lib.strings.CommonStrings
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.flow.unwrap
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

@AndroidEntryPoint
class VectorSettingsWorkingTimeFragment : VectorSettingsBaseFragment() {

    @Inject
    lateinit var localeProvider: LocaleProvider

    private val workingTimePreference by lazy {
        findPreference<WorkingTimePreference>("WORKING_TIME_CONFIG")
    }

    private val workingTimePreferenceSwitch by lazy {
        findPreference<VectorSwitchPreference>("SETTINGS_ENABLE_WORKING_TIMES")
    }

    private var showSaveButton = false

    override var titleRes = CommonStrings.settings_working_time

    override val preferenceXmlRes = R.xml.vector_settings_working_time

    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.vector_room_settings, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.findItem(R.id.roomSettingsSaveAction).isVisible = showSaveButton
    }

    @Suppress("DEPRECATION")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.roomSettingsSaveAction) {
            saveSettings()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun bindPref() {
        workingTimePreferenceSwitch?.apply {
            isIconSpaceReserved = false
            setOnPreferenceChangeListener { _, newValue ->
                viewLifecycleOwner.lifecycleScope.launch {
                    kotlin.runCatching {
                        session.workingTimeService().setWorkingTimeEnabled(newValue as Boolean)
                    }.onFailure { error ->
                        error.localizedMessage?.let { activity?.toast(it) }
                    }
                }
                true
            }
        }

        workingTimePreference?.hourFormatter = if (DateFormat.is24HourFormat(context)) {
            DateTimeFormatter.ofPattern("HH:mm", localeProvider.current())
        } else {
            DateTimeFormatter.ofPattern("h:mm a", localeProvider.current())
        }

        workingTimePreference?.setOnPreferenceChangeListener { _, _ ->
            showSaveButton = true
            activity?.invalidateOptionsMenu()
            true
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        workingTimePreferenceSwitch?.isChecked = session.workingTimeService().isWorkingTimeEnabled()
        workingTimePreference?.weekDayConfiguration = session.workingTimeService().getWorkingTimeConfiguration().toMutableMap()
        session.accountDataService().getLiveUserAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES)
                .asFlow()
                .unwrap()
                .onEach {
                    workingTimePreferenceSwitch?.isChecked = session.workingTimeService().isWorkingTimeEnabled()
                    workingTimePreference?.weekDayConfiguration = session.workingTimeService().getWorkingTimeConfiguration().toMutableMap()
                }
                .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun saveSettings() {
        viewLifecycleOwner.lifecycleScope.launch {
            workingTimePreference?.apply {
                try {
                    session.workingTimeService().setWorkingTimeConfiguration(weekDayConfiguration)
                    showSaveButton = false
                    activity?.invalidateOptionsMenu()
                    activity?.toast(CommonStrings.working_time_configuration_saved)
                } catch (error : Throwable) {
                    error.localizedMessage?.let { activity?.toast(it) }
                }
            }
        }
    }
}
