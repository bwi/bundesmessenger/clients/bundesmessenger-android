/*
 * Copyright 2019 New Vector Ltd
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.features.settings

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import de.bwi.messenger.configuration.AppConfiguration
import de.bwi.messenger.features.maintenance.DowntimeMessageFormatter
import de.bwi.messenger.features.maintenance.MaintenanceInfoView
import im.vector.app.core.preference.VectorPreference
import im.vector.app.core.resources.BuildMeta
import im.vector.app.features.analytics.plan.MobileScreen
import im.vector.app.features.themes.ThemeUtils
import kotlinx.coroutines.launch
import javax.inject.Inject
import im.vector.lib.strings.CommonStrings
import im.vector.app.R

@AndroidEntryPoint
class VectorSettingsRootFragment :
        VectorSettingsBaseFragment() {

    @Inject lateinit var appConfiguration: AppConfiguration
    @Inject lateinit var vectorPreferences: VectorPreferences
    @Inject lateinit var buildMeta: BuildMeta

    override var titleRes: Int = CommonStrings.title_activity_settings
    override val preferenceXmlRes = im.vector.app.R.xml.vector_settings_root

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        analyticsScreenName = MobileScreen.ScreenName.Settings
    }

    private val mLabsPreference by lazy {
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_LABS_PREFERENCE_KEY)
    }

    private val maintenanceDowntimeMaintenanceView by lazy {
        findPreference<MaintenanceInfoView>("SETTINGS_MAINTENANCE_DOWNTIME_MESSAGE_KEY")
    }

    private val maintenanceDowntimeInfoView by lazy {
        findPreference<MaintenanceInfoView>("SETTINGS_MAINTENANCE_INFO_MESSAGE_KEY")
    }

    private val maintenanceUpdateView by lazy {
        findPreference<MaintenanceInfoView>("SETTINGS_MAINTENANCE_UPDATE_MESSAGE_KEY")
    }

    private val mAdvancedSettingsPreference by lazy {
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_ADVANCED_SETTINGS_PREFERENCE_KEY)
    }

    override fun bindPref() {
        tintIcons()

        lifecycleScope.launch {
            val errorColor = ThemeUtils.getColor(requireContext(), com.google.android.material.R.attr.colorError)
            val warningColor = ContextCompat.getColor(requireContext(), R.color.warning_color)

            maintenanceDowntimeMaintenanceView?.isVisible = session.maintenanceService().getCurrentDowntimeInfo()?.isMaintenanceType() == true
            maintenanceDowntimeInfoView?.isVisible = session.maintenanceService().getCurrentDowntimeInfo()?.description?.isNotEmpty() == true ||
                    session.maintenanceService().getCurrentDowntimeInfo()?.isInfoMessageType() == true
            session.maintenanceService().getCurrentDowntimeInfo().let {
                val backgroundColor = if (it?.isActive() == true && it.blocking) errorColor else warningColor
                val textColor = if(it?.isActive() == true && it.blocking) ContextCompat.getColor(requireContext(), android.R.color.white) else ContextCompat.getColor(requireContext(), android.R.color.black)
                maintenanceDowntimeMaintenanceView?.configure(DowntimeMessageFormatter.getFormattedDowntimeMessage(requireContext(), it), backgroundColor, textColor,
                        session.maintenanceService().getCurrentDowntimeInfo()?.blocking == true && session.maintenanceService().shouldBlockOnBlockingDowntime()) {
                    session.maintenanceService().setBlockOnBlockingDowntime(false)
                }
                maintenanceDowntimeInfoView?.configure(it?.description ?: getString(CommonStrings.general_maintenance_message), backgroundColor, textColor,
                        session.maintenanceService().getCurrentDowntimeInfo()?.blocking == true &&
                                session.maintenanceService().shouldBlockOnBlockingDowntime() &&
                                session.maintenanceService().getCurrentDowntimeInfo()?.isInfoMessageType() == true) {
                    session.maintenanceService().setBlockOnBlockingDowntime(false)
                }
            }

            val shouldUpdate = session.maintenanceService().getVersionsInfo()?.shouldUpdate(buildMeta.versionName) == true
            maintenanceUpdateView?.isVisible = shouldUpdate
            val textColor = ContextCompat.getColor(requireContext(), android.R.color.black)
            maintenanceUpdateView?.configure(getString(CommonStrings.maintenance_should_update), warningColor, textColor)
        }

        mAdvancedSettingsPreference?.isVisible = appConfiguration.showAdvancedSettings
    }

    override fun onResume() {
        super.onResume()
        mLabsPreference?.isVisible = vectorPreferences.isShowLabSettingsEnabled()
    }

    private fun tintIcons() {
        for (i in 0 until preferenceScreen.preferenceCount) {
            (preferenceScreen.getPreference(i) as? VectorPreference)?.let { it.tintIcon = true }
        }
    }
}
