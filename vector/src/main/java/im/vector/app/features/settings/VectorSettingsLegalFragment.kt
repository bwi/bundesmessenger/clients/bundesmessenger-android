/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.features.settings

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.preference.Preference
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import dagger.hilt.android.AndroidEntryPoint
import de.bwi.messenger.configuration.AppConfiguration
import de.bwi.messenger.features.location_sharing.BwiWellknownService
import im.vector.app.R
import im.vector.app.core.di.ActiveSessionHolder
import im.vector.app.core.preference.VectorPreference
import im.vector.app.core.pushers.FcmHelper
import im.vector.app.core.utils.FirstThrottler
import im.vector.app.core.utils.displayInWebView
import im.vector.app.core.utils.openUrlInChromeCustomTab
import im.vector.app.custom.CustomConfiguration
import im.vector.app.features.analytics.plan.MobileScreen
import im.vector.lib.strings.CommonStrings
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

@AndroidEntryPoint
class VectorSettingsLegalFragment :
        VectorSettingsBaseFragment() {

    @Inject lateinit var fcmHelper: FcmHelper
    @Inject lateinit var appConfiguration: AppConfiguration
    @Inject lateinit var bwiWellknownService: BwiWellknownService
    @Inject lateinit var activeSessionHolder: ActiveSessionHolder

    override var titleRes = CommonStrings.preference_root_legals
    override val preferenceXmlRes = R.xml.bwi_settings_legal

    private val firstThrottler = FirstThrottler(1000)

    private val privacyPolicyPref by lazy {
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_PRIVACY_POLICY_PREFERENCE_KEY)!!
    }

    private val imprintPref by lazy {
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_IMPRINT_PREFERENCE_KEY)!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        analyticsScreenName = MobileScreen.ScreenName.SettingsLegals
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            val elementWellknown = bwiWellknownService.getElementWellknown(session.sessionParams.homeServerUrl)
            val dataPrivacyUrl = elementWellknown.bwiWellKnownConfig?.dataPrivacyUrl
            val imprintUrl = elementWellknown.bwiWellKnownConfig?.imprintUrl

            privacyPolicyPref.isVisible = dataPrivacyUrl != null
            if(dataPrivacyUrl != null) {
                privacyPolicyPref.onPreferenceClickListener = Preference.OnPreferenceClickListener {
                    openUrl(dataPrivacyUrl)
                    false
                }
            }

            imprintPref.isVisible = imprintUrl != null
            if(imprintUrl != null) {
                imprintPref.onPreferenceClickListener = Preference.OnPreferenceClickListener {
                    openUrl(imprintUrl)
                    false
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        hideLoadingView()
    }

    override fun bindPref() {
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_COPYRIGHT_PREFERENCE_KEY)!!
                .onPreferenceClickListener = Preference.OnPreferenceClickListener {
            openUrl(VectorSettingsUrls.COPYRIGHT)
            false
        }

        // third party notice
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_THIRD_PARTY_NOTICES_PREFERENCE_KEY)!!
                .onPreferenceClickListener = Preference.OnPreferenceClickListener {
            if (firstThrottler.canHandle() is FirstThrottler.CanHandlerResult.Yes) {
                openUrl(VectorSettingsUrls.THIRD_PARTY_LICENSES)
            }
            false
        }

        // other third party notice
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_OTHER_THIRD_PARTY_NOTICES_PREFERENCE_KEY)?.let {
            if (fcmHelper.isFirebaseAvailable()) {
                it.onPreferenceClickListener = Preference.OnPreferenceClickListener {
                    context?.startActivity(Intent(context, OssLicensesMenuActivity::class.java))
                    false
                }
            } else {
                it.isVisible = false
            }
        }
    }

    private fun openUrl(url: String) {
        if (url.startsWith("file://")) {
            activity?.displayInWebView(url, CustomConfiguration.limitWebviewAccessToHost)
        } else {
            openUrlInChromeCustomTab(requireContext(), null, url)
        }
    }
}
