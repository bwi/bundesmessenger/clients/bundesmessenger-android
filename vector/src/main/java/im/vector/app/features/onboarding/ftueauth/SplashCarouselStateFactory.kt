/*
 * Copyright (c) 2022 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.features.onboarding.ftueauth

import android.content.Context
import androidx.annotation.AttrRes
import androidx.annotation.DrawableRes
import im.vector.app.R
import im.vector.app.core.resources.LocaleProvider
import im.vector.app.core.resources.StringProvider
import im.vector.app.core.resources.isEnglishSpeaking
import im.vector.app.core.utils.colorTerminatingFullStop
import im.vector.app.features.themes.ThemeProvider
import im.vector.app.features.themes.ThemeUtils
import im.vector.lib.core.utils.epoxy.charsequence.EpoxyCharSequence
import im.vector.lib.core.utils.epoxy.charsequence.toEpoxyCharSequence
import im.vector.lib.strings.CommonStrings
import javax.inject.Inject

class SplashCarouselStateFactory @Inject constructor(
        private val context: Context,
        private val stringProvider: StringProvider,
        private val localeProvider: LocaleProvider,
        private val themeProvider: ThemeProvider,
) {

    fun create(): SplashCarouselState {
        // BWI specific
        val lightTheme = themeProvider.isLightTheme()
        fun hero(@DrawableRes lightDrawable: Int, @DrawableRes darkDrawable: Int) = if (lightTheme) lightDrawable else darkDrawable
        return SplashCarouselState(
                listOf(
                        SplashCarouselState.Item(
                                CommonStrings.welcome_title0.colorTerminatingFullStop(com.google.android.material.R.attr.colorAccent),
                                CommonStrings.welcome_body0,
                                hero(R.drawable.ic_welcome0_light, R.drawable.ic_welcome0_dark),
                                im.vector.lib.ui.styles.R.drawable.bwi_welcome_background,
                        ),
                        SplashCarouselState.Item(
                                CommonStrings.welcome_title1.colorTerminatingFullStop(com.google.android.material.R.attr.colorAccent),
                                CommonStrings.welcome_body1,
                                hero(R.drawable.ic_welcome1_light, R.drawable.ic_welcome1_dark),
                                im.vector.lib.ui.styles.R.drawable.bwi_welcome_background,
                        ),
                        SplashCarouselState.Item(
                                CommonStrings.welcome_title2.colorTerminatingFullStop(com.google.android.material.R.attr.colorAccent),
                                CommonStrings.welcome_body2,
                                hero(R.drawable.ic_welcome2_light, R.drawable.ic_welcome2_dark),
                                im.vector.lib.ui.styles.R.drawable.bwi_welcome_background,
                        ),
                        SplashCarouselState.Item(
                                CommonStrings.welcome_title3.colorTerminatingFullStop(com.google.android.material.R.attr.colorAccent),
                                CommonStrings.welcome_body3,
                                hero(R.drawable.ic_welcome3_light, R.drawable.ic_welcome3_dark),
                                im.vector.lib.ui.styles.R.drawable.bwi_welcome_background,
                        ),
                        // BWI specific: 5th page
                        SplashCarouselState.Item(
                                CommonStrings.welcome_title4.colorTerminatingFullStop(com.google.android.material.R.attr.colorAccent),
                                CommonStrings.welcome_body4,
                                hero(R.drawable.ic_welcome4_light, R.drawable.ic_welcome4_dark),
                                im.vector.lib.ui.styles.R.drawable.bwi_welcome_background,
                        ),
                )
        )
    }

    private fun collaborationTitle(): Int {
        return when {
            localeProvider.isEnglishSpeaking() -> CommonStrings.cut_the_slack_from_teams
            else -> CommonStrings.ftue_auth_carousel_workplace_title
        }
    }

    private fun Int.colorTerminatingFullStop(@AttrRes color: Int): EpoxyCharSequence {
        return stringProvider.getString(this)
                .colorTerminatingFullStop(ThemeUtils.getColor(context, color))
                .toEpoxyCharSequence()
    }
}
