/*
 * Copyright (c) 2021 New Vector Ltd
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.features.analytics.impl

import im.vector.app.features.analytics.VectorAnalytics
import im.vector.app.features.analytics.itf.VectorAnalyticsEvent
import im.vector.app.features.analytics.itf.VectorAnalyticsScreen
import im.vector.app.features.analytics.plan.UserProperties
import im.vector.app.features.analytics.plan.SuperProperties
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject
import javax.inject.Singleton

// BWI dummy vector analytics implementation
@Singleton
class DummyVectorAnalytics @Inject constructor() : VectorAnalytics {
    override fun getUserConsent(): Flow<Boolean> {
        return flowOf(false)
    }

    override suspend fun setUserConsent(userConsent: Boolean) {
    }

    override fun didAskUserConsent(): Flow<Boolean> {
        return flowOf(true)
    }

    override suspend fun setDidAskUserConsent() {
    }

    override fun getAnalyticsId(): Flow<String> {
        return flowOf("")
    }

    override suspend fun setAnalyticsId(analyticsId: String) {
    }

    override suspend fun onSignOut() {
    }

    override fun init() {
    }

    override fun capture(event: VectorAnalyticsEvent) {
    }

    override fun screen(screen: VectorAnalyticsScreen) {
    }

    override fun updateUserProperties(userProperties: UserProperties) {
    }

    override fun updateSuperProperties(updatedProperties: SuperProperties) {
    }

    override fun trackError(throwable: Throwable) {
    }
}
