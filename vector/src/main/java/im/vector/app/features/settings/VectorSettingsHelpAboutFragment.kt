/*
 * Copyright 2019 New Vector Ltd
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.features.settings

import android.os.Bundle
import android.widget.Toast
import androidx.preference.Preference
import dagger.hilt.android.AndroidEntryPoint
import de.bwi.messenger.configuration.AppConfiguration
import im.vector.app.R
import im.vector.app.core.extensions.orEmpty
import im.vector.app.core.preference.VectorPreference
import im.vector.app.core.pushers.FcmHelper
import im.vector.app.core.resources.BuildMeta
import im.vector.app.core.utils.FirstThrottler
import im.vector.app.core.utils.copyToClipboard
import im.vector.app.core.utils.displayInWebView
import im.vector.app.core.utils.openAppSettingsPage
import im.vector.app.core.utils.openUrlInChromeCustomTab
import im.vector.app.custom.CustomConfiguration
import im.vector.app.features.analytics.plan.MobileScreen
import im.vector.app.features.version.VersionProvider
import im.vector.lib.strings.CommonStrings
import org.matrix.android.sdk.api.Matrix
import javax.inject.Inject

@AndroidEntryPoint
class VectorSettingsHelpAboutFragment :
        VectorSettingsBaseFragment() {

    @Inject lateinit var versionProvider: VersionProvider
    @Inject lateinit var buildMeta: BuildMeta
    @Inject lateinit var fcmHelper: FcmHelper
    @Inject lateinit var appConfiguration: AppConfiguration
    @Inject lateinit var vectorPreferences: VectorPreferences

    var clickedForLab = 0
    val clicksToShowLab = 7

    override var titleRes = CommonStrings.preference_root_help_about
    override val preferenceXmlRes = R.xml.vector_settings_help_about

    private val firstThrottler = FirstThrottler(1000)

    private val appTermConditionsPref by lazy {
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_APP_TERM_CONDITIONS_PREFERENCE_KEY)!!
    }

    private val elementVersionPref by lazy {
        findPreference<VectorPreference>("SETTINGS_ELEMENT_VERSION_PREFERENCE_KEY")
    }

    private val supportPref by lazy {
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_SUPPORT_PREFERENCE_KEY)!!
    }

    private val releaseNotesPref by lazy {
        findPreference<VectorPreference>("RELEASE_NOTES")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        analyticsScreenName = MobileScreen.ScreenName.SettingsHelp
    }

    override fun onDestroy() {
        super.onDestroy()
        hideLoadingView()
    }

    override fun bindPref() {
        // Help
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_HELP_PREFERENCE_KEY)?.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            if (firstThrottler.canHandle() is FirstThrottler.CanHandlerResult.Yes) {
                openUrlInChromeCustomTab(requireContext(), null, VectorSettingsUrls.HELP)
            }
            false
        }

        // preference to start the App info screen, to facilitate App permissions access
        findPreference<VectorPreference>(APP_INFO_LINK_PREFERENCE_KEY)!!
                .onPreferenceClickListener = Preference.OnPreferenceClickListener {
            activity?.let { openAppSettingsPage(it) }
            true
        }

        // application version
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_VERSION_PREFERENCE_KEY)!!.let {
            it.summary = buildString {
                append(versionProvider.getVersion(longFormat = false))
                if (buildMeta.isDebug) {
                    append(" ")
                    append(buildMeta.gitBranchName)
                    append(" ")
                    append(buildMeta.gitRevision)
                }
            }

            it.setOnPreferenceClickListener { pref ->
                copyToClipboard(requireContext(), pref.summary.orEmpty())
                true
            }
        }

        // SDK version
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_SDK_VERSION_PREFERENCE_KEY)!!.let {
            it.summary = Matrix.getSdkVersion()

            it.setOnPreferenceClickListener { pref ->
                copyToClipboard(requireContext(), pref.summary.orEmpty())
                true
            }
        }

        // olm version
        findPreference<VectorPreference>(VectorPreferences.SETTINGS_CRYPTO_VERSION_PREFERENCE_KEY)!!
                .summary = Matrix.getCryptoVersion(true)

        // support
        supportPref.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            openUrl(VectorSettingsUrls.SUPPORT)
            false
        }

        // terms & conditions
        appTermConditionsPref.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            openUrl(VectorSettingsUrls.TAC)
            false
        }

        elementVersionPref?.let {
            it.summary = buildMeta.elementVersion
        }

        releaseNotesPref?.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            openUrl(VectorSettingsUrls.RELEASE_NOTES)
            false
        }

        elementVersionPref?.setOnPreferenceClickListener {
            if (!vectorPreferences.isShowLabSettingsEnabled()) {
                clickedForLab++
                if (clickedForLab >= clicksToShowLab) {
                    vectorPreferences.setShowLabSettings(true)
                    Toast.makeText(requireContext(), "Lab-Einstellungen freigeschaltet", Toast.LENGTH_LONG).show()
                }
            }
            false
        }
    }

    private fun openUrl(url: String) {
        if (url.startsWith("file://")) {
            activity?.displayInWebView(url, CustomConfiguration.limitWebviewAccessToHost)
        } else {
            openUrlInChromeCustomTab(requireContext(), null, url)
        }
    }

    override fun onResume() {
        super.onResume()

        appTermConditionsPref.isVisible = appConfiguration.showTac
        supportPref.isVisible = appConfiguration.showSupportSettings
    }

    companion object {
        private const val APP_INFO_LINK_PREFERENCE_KEY = "APP_INFO_LINK_PREFERENCE_KEY"
    }
}
