/*
 * Copyright (c) 2020 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.core.ui.views

import android.content.Context
import android.content.Intent
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

/***
 * Helper [WebViewClient] to implement opening of Phone number and Emails on the phone for further actions
 *
 * @param restrictToHost    used to restrict further linking to a specific host
 */
class AssetWebViewClient(private val context: Context, private val restrictToHost: String? = null) : WebViewClient() {

    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        val uri = request.url
        if (uri.toString().startsWith("mailto:")) {
            //Handle mail Urls
            context.startActivity(Intent(Intent.ACTION_SENDTO, uri))
        } else if (uri.toString().startsWith("tel:")) {
            //Handle telephony Urls
            context.startActivity(Intent(Intent.ACTION_DIAL, uri))
        } else {
            //only handle urls which are ok
            restrictToHost?.let {
                if (uri.toString().startsWith(it)) {
                    view.loadUrl(uri.toString())
                }
            }
        }
        return true
    }
}
