/*
 * Copyright (c) 2020 New Vector Ltd
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.core.resources

import android.content.res.Resources
import androidx.annotation.IntegerRes
import androidx.annotation.NonNull
import javax.inject.Inject

class IntegerProvider @Inject constructor(private val resources: Resources) {

    /**
     * Returns a localized int from the application's package's
     * default int table.
     *
     * @param resId Resource id for the int
     * @return The integer value associated with the resource
     */
    @NonNull
    fun getInt(@IntegerRes resId: Int): Int {
        return resources.getInteger(resId)
    }
}
