/*
 * Copyright 2019 New Vector Ltd
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.core.resources

import im.vector.app.features.settings.VectorPreferences
import org.matrix.android.sdk.api.session.Session
import javax.inject.Inject

class UserPreferencesProvider @Inject constructor(private val vectorPreferences: VectorPreferences,
                                                  private val session: Session) {

    fun shouldShowHiddenEvents(): Boolean {
        return vectorPreferences.shouldShowHiddenEvents()
    }

    fun shouldShowReadReceipts(): Boolean {
        return vectorPreferences.showReadReceipts()
    }

    fun shouldShowRedactedMessages(roomId: String? = null): Boolean {
        return if(session.personalNotesService().getPersonalNotesRoomId() != null && session.personalNotesService().getPersonalNotesRoomId() == roomId) {
            false
        } else {
            vectorPreferences.showRedactedMessages()
        }
    }

    fun shouldShowLongClickOnRoomHelp(): Boolean {
        return vectorPreferences.shouldShowLongClickOnRoomHelp()
    }

    fun neverShowLongClickOnRoomHelpAgain() {
        vectorPreferences.neverShowLongClickOnRoomHelpAgain()
    }

    fun shouldShowJoinLeaves(): Boolean {
        return vectorPreferences.showJoinLeaveMessages()
    }

    fun shouldShowAvatarDisplayNameChanges(): Boolean {
        return vectorPreferences.showAvatarDisplayNameChangeMessages()
    }

    fun areThreadMessagesEnabled(): Boolean {
        return vectorPreferences.areThreadMessagesEnabled()
    }

    fun shouldShowRoomProfileChanges() : Boolean {
        return vectorPreferences.showRoomProfileChangeMessages()
    }

    fun showLiveSenderInfo(): Boolean {
        return vectorPreferences.showLiveSenderInfo()
    }

    fun autoplayAnimatedImages(): Boolean {
        return vectorPreferences.autoplayAnimatedImages()
    }
}
