/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.core.preference

import android.app.TimePickerDialog
import android.content.Context
import android.text.format.DateFormat
import android.util.AttributeSet
import android.widget.Button
import android.widget.TextView
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import com.google.android.material.radiobutton.MaterialRadioButton
import de.bwi.messenger.features.workingtime.WorkingTime
import im.vector.app.R
import im.vector.app.core.epoxy.onClick
import im.vector.lib.strings.CommonStrings
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.TextStyle
import java.util.Locale

class WorkingTimePreference : Preference {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    var weekDayConfiguration: MutableMap<DayOfWeek, WorkingTime> = mutableMapOf()
        set(value) {
            field = value
            notifyChanged()
        }

    var hourFormatter : DateTimeFormatter? = null

    init {
        layoutResource = R.layout.preference_working_time_configuration
        isIconSpaceReserved = false
        isSelectable = false
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)

        val weekDayButton : MutableMap<DayOfWeek, MaterialRadioButton?> = mutableMapOf()
        weekDayButton[DayOfWeek.MONDAY] = holder.itemView.findViewById(R.id.working_time_preference_monday)
        weekDayButton[DayOfWeek.TUESDAY] = holder.itemView.findViewById(R.id.working_time_preference_tuesday)
        weekDayButton[DayOfWeek.WEDNESDAY] = holder.itemView.findViewById(R.id.working_time_preference_wednesday)
        weekDayButton[DayOfWeek.THURSDAY] = holder.itemView.findViewById(R.id.working_time_preference_thursday)
        weekDayButton[DayOfWeek.FRIDAY] = holder.itemView.findViewById(R.id.working_time_preference_friday)
        weekDayButton[DayOfWeek.SATURDAY] = holder.itemView.findViewById(R.id.working_time_preference_saturday)
        weekDayButton[DayOfWeek.SUNDAY] = holder.itemView.findViewById(R.id.working_time_preference_sunday)

        if(weekDayButton.values.none { it?.isChecked == true }) {
            weekDayButton[DayOfWeek.from(LocalDateTime.now())]?.isChecked = true
        }

        weekDayButton.forEach { entry->
            entry.value?.apply {
                weekDayConfiguration[entry.key]?.let { workingTime ->
                    contentDescription = entry.key.getDisplayName(TextStyle.FULL, Locale.getDefault())
                    text = entry.key.getDisplayName(TextStyle.SHORT, Locale.getDefault()).substring(0, 2)
                    isActivated = workingTime.isEnabled == true
                    if(isChecked) {
                        bindTimeControls(holder, entry.key, workingTime)
                    }
                    setOnCheckedChangeListener { _, isChecked ->
                        if(isChecked) {
                            bindTimeControls(holder, entry.key, workingTime)
                        }
                    }
                }
            }
        }
    }

    private fun bindTimeControls(holder: PreferenceViewHolder?, dayOfWeek: DayOfWeek, workingTime: WorkingTime) {
        holder?.itemView?.findViewById<TextView>(R.id.working_time_preference_from_time)?.apply {
            if(workingTime.isEnabled) {
                contentDescription = context.getString(
                        CommonStrings.working_times_from_time_content_description,
                        dayOfWeek.getDisplayName(TextStyle.FULL, Locale.getDefault()),
                        hourFormatter?.format(workingTime.fromTime)
                )
            } else {
                contentDescription = null
            }
            text = if(workingTime.isEnabled) hourFormatter?.format(workingTime.fromTime) else "--:--"
            isEnabled = workingTime.isEnabled && this@WorkingTimePreference.isEnabled
            setOnClickListener {
                val timeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                    workingTime.fromTime = LocalTime.of(hour, minute)
                    notifyChanged()
                    callChangeListener(null)
                }
                TimePickerDialog(context, timeSetListener, workingTime.fromTime.hour, workingTime.fromTime.minute, DateFormat.is24HourFormat(context)).show()
            }
        }
        holder?.itemView?.findViewById<TextView>(R.id.working_time_preference_to_time)?.apply {
            if(workingTime.isEnabled) {
                contentDescription = context.getString(
                        CommonStrings.working_times_to_time_content_description,
                        dayOfWeek.getDisplayName(TextStyle.FULL, Locale.getDefault()),
                        hourFormatter?.format(workingTime.toTime)
                )
            } else {
                contentDescription = null
            }
            text = if(workingTime.isEnabled) hourFormatter?.format(workingTime.toTime) else "--:--"
            isEnabled = workingTime.isEnabled && this@WorkingTimePreference.isEnabled
            setOnClickListener {
                val timeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                    workingTime.toTime = LocalTime.of(hour, minute)
                    notifyChanged()
                    callChangeListener(null)
                }
                TimePickerDialog(context, timeSetListener, workingTime.toTime.hour, workingTime.toTime.minute, DateFormat.is24HourFormat(context)).show()
            }
        }

        holder?.itemView?.findViewById<TextView>(R.id.working_time_preference_lower_time_label)?.isEnabled = workingTime.isEnabled && this@WorkingTimePreference.isEnabled
        holder?.itemView?.findViewById<TextView>(R.id.working_time_preference_upper_time_label)?.isEnabled = workingTime.isEnabled && this@WorkingTimePreference.isEnabled

        val weekDay = dayOfWeek.getDisplayName(TextStyle.FULL_STANDALONE, Locale.getDefault())

        holder?.itemView?.findViewById<Button>(R.id.working_time_toggle_day_button)?.apply {
            text = context.getString(if(workingTime.isEnabled) CommonStrings.working_times_deactivate_day else CommonStrings.working_times_activate_day,
                    weekDay)
            onClick {
                workingTime.isEnabled = !workingTime.isEnabled
                notifyChanged()
                callChangeListener(null)
            }
        }

        holder?.itemView?.findViewById<TextView>(R.id.working_time_preference_day_description)?.apply {
            text = if(workingTime.isEnabled) context.getString(CommonStrings.working_time_preference_day_description, weekDay) else
                context.getString(CommonStrings.working_time_preference_day_disabled_description, weekDay)
        }
    }
}
