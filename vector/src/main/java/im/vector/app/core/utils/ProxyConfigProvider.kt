/*
 * Copyright (c) 2020 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.vector.app.core.utils

import im.vector.app.BuildConfig
import im.vector.app.R
import im.vector.app.core.resources.IntegerProvider
import im.vector.app.core.resources.StringProvider
import im.vector.lib.strings.CommonStrings
import java.net.InetSocketAddress
import java.net.Proxy
import javax.inject.Inject

class ProxyConfigProvider @Inject constructor(val stringProvider: StringProvider, val integerProvider: IntegerProvider) {

    fun getProxyConfiguration(): Proxy? {
        var hostname = stringProvider.getString(im.vector.app.config.R.string.proxy_host)
        if (hostname.isEmpty()) {
            hostname = BuildConfig.HTTP_PROXY_HOST
        }

        var port = integerProvider.getInt(im.vector.app.config.R.integer.proxy_port)
        if(port < 0) {
            port = BuildConfig.HTTP_PROXY_PORT
        }

        if (hostname.isEmpty() || port == -1) {
            return null
        }

        return Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(hostname, port))
    }
}
