
Changes in BWI project 1.X.X (2022-XX-XX)
===================================================

Upstream merge ✨:
 -

Features ✨:
 -

Improvements 🙌:
 -

Bugfix 🐛:
 -

Translations 🗣:
 -

SDK API changes ⚠️:
 -

Build 🧱:
 -

Other changes:
 - 


Changes in BWI project 2.24.0 (2025-2-24)
===================================================
Features ✨:
- MESSENGER-6693 JWT Token Expiration check
- MESSENGER-6853 Change the Happy-Birthday dialog
- MESSENGER-6791 Token check for Authenticated Media

Changes in BWI project 2.23.0 (2024-10-25)
===================================================
Improvements 🙌:
- MESSENGER-6640 Update MapLibre Dependency

Changes in BWI project 2.22.0 (2024-10-02)
===================================================
Upstream merge ✨:
- EA 1.6.22 (https://github.com/element-hq/element-android/releases/tag/v1.6.22)

Improvements 🙌:
- MESSENGER-5785 Improved error message, if the last admin tries to leave a room with pending invitations
- MESSENGER-6527 Improved information text, if a room will be delete, when an admin leaves a room
- MESSENGER-6436 PFD library was replaced by a new open source library

Changes in BWI project 2.21.0 (2024-08-26)
===================================================
Upstream merge ✨:
- EA 1.6.20 (https://github.com/element-hq/element-android/releases/tag/v1.6.20)

Features ✨:
- MESSENGER-5883 JWT based homeserver url validation

Improvements 🙌:
- MESSENGER-5885 Added support for the federation release notes banner
- MESSENGER-6167 Show federation badges in invitation details screen
- MESSENGER-6172 Improved federation badges layout in user's profile screen
- MESSENGER-6352 Removed unused/not supported deeplink host names

Bugfix 🐛:
- MESSENGER-5790 Fixed user's function name refresh issue
- MESSENGER-6116 Fixed admin-role section visibility issue in user's profile screen

Changes in BWI project 2.20.0 (2024-07-02)
===================================================
Improvements 🙌:
- MESSENGER-6187 Removed Jitsy and related unused libraries and dependencies

Changes in BWI project 2.19.0 (2024-05-28)
===================================================
Features ✨:
- MESSENGER-6017 Advanced error handling for simplified login

Improvements 🙌:
- MESSENGER-5839 Remove option to change role of an admin user
- MESSENGER-6128 Remove some unused dependencies

Bugfix 🐛:
- MESSENGER-6125 New error message in context of invitations and federated rooms
- MESSENGER-6005 Mention user in new composer

Changes in BWI project 2.18.0 (2024-05-06)
===================================================
Upstream merge ✨:
- EA 1.6.14 (https://github.com/element-hq/element-android/releases/tag/v1.6.14)

Features ✨:
- MESSENGER-4804 Test for battery optimizations in push notification troubleshooting option

Bugfix 🐛:
- MESSENGER-5931 Fixed refreshing issues with federation badges

Changes in BWI project 2.17.0 (2024-04-08)
===================================================
Features ✨:
- MESSENGER-5659 Federation introduction
- MESSENGER-5612 Federation banner
- MESSENGER-5891 Activate Federation via Well-Known
- MESSENGER-5144 Simplified Login
- MESSENGER-5792 Representation of media files that are not available

Bugfix 🐛:
- MESSENGER-5574 Wrong wording WHS-Reset
- MESSENGER-5842 Wrong wording room member list



Changes in BWI project 2.16.0 (2024-03-08)
===================================================
Upstream merge ✨:
- EA 1.6.12 (https://github.com/element-hq/element-android/releases/tag/v1.6.12)

Features ✨:
- MESSENGER-5705 Federation announcement

Improvements 🙌:
- MESSENGER-5460 Improved wording of the federation state events in the timeline
- MESSENGER-5577 Improved success feedback for the federation setting change
- MESSENGER-5611 Added federation related hint to the user's profile picture
- MESSENGER-5765 Removed "remind later" button

Bugfix 🐛:
- MESSENGER-5579 Fixed wrong colors in the users search screen
- MESSENGER-5582 Fixed wrong colors in the location share screen
- MESSENGER-5618 Fixed empty favourites tab
- MESSENGER-5712 Added missing federation pills in the room settings sub-screens


Changes in BWI project 2.15.0 (2024-02-13)
===================================================

Features ✨:
- MESSENGER-5227 Show federation in timeline header
- MESSENGER-5230 UI for the decision, if a room should be federated
- MESSENGER-5313 Mark federated user in status events
- MESSENGER-5382 Federated users could not be admins
- MESSENGER-5384 Check alias for existing rooms
- MESSENGER-5387 Federated users could only be invited, after the admins has decide to federate the room
- MESSENGER-5566 Only admins can change the federation state of a room

Improvements 🙌:
- MESSENGER-5607 Update library dependencies
- MESSENGER-5606 Remove unused LaTex library
- MESSENGER-5263 Min SDK level is 29

Bugfix 🐛:
- MESSENGER-5616 Fixed Huawei App Gallery promon shield config
- MESSENGER-5583 Wrong color of 'invite user' button in dark mode
- MESSENGER-5584 Notes room is not shown after passphrase reset
- MESSENGER-5640 Solve problems with wrong focus of UI elements


Changes in BWI project 2.14.1 (2024-02-01)
===================================================
Bugfix 🐛:
- MESSENGER-5686 Solve problems with new composer


Changes in BWI project 2.14.0 (2024-01-15)
===================================================
Upstream merge ✨:
- EA 1.6.10 (https://github.com/element-hq/element-android/releases/tag/v1.6.10)

Features ✨:
- MESSENGER-4818 Added MDM configuration parameter for skipping the PIN lock screen
- MESSENGER-4818 New MDM-flag for disabling PIN on login
- MESSENGER-4844 Show federation badge in room profile
- MESSENGER-4845 Show federation badges in room member list
- MESSENGER-5206 Show federation badges on user search
- MESSENGER-5211 Show federation badges on federated rooms
- MESSENGER-5289 Show federation badge in user's profile
- MESSENGER-5302 Show federation badges on user invite
- MESSENGER-5411 Added federation setting switch to the room profile

Improvements 🙌:
- MESSENGER-5370 Improved maintenance handling
- MESSENGER-5235 Hide shield icons on messages
- MESSENGER-5285 Improved error messages on login
- MESSENGER-5357 Moved WYSIWYG composer switch to settings
- MESSENGER-5507 Activate "show current user avatar" setting by default
- MESSENGER-5198 Changed wording for login state progress
- MESSENGER-5199 Disabled location share intent to external applications
- MESSENGER-5244 Copy permalinks to the clipboard

Bugfix 🐛:
- MESSENGER-5369 Fixed "infected" icon tinting


Changes in BWI project 2.12.0 (2023-11-28)
===================================================
Upstream merge ✨:
- EA 1.6.6 (https://github.com/vector-im/element-android/releases/tag/v1.6.6)

Features ✨:
- MESSENGER-4818 Added MDM configuration parameter for skipping the PIN lock screen

Improvements 🙌:
- MESSENGER-5034 Changed OIDC login mask
- MESSENGER-5239 Disabled file sharing and file download due to security considerations
- MESSENGER-5245 Disabled "quote" message action due to inconsistence with deleted and edited messages

Bugfix 🐛:
- MESSENGER-5291 Fixed decryption error messages in the timeline


Changes in BWI project 2.11.0 (2023-10-25)
===================================================
Upstream merge ✨:
- EA 1.6.5 (https://github.com/vector-im/element-android/releases/tag/v1.6.5)

Features ✨:
- MESSENGER-4908 Forwarding voice messages

Bugfix 🐛:
- MESSENGER-5094 Fixed error message on voice message recording


Changes in BWI project 2.10.0 (2023-09-29)
===================================================
Upstream merge ✨:
- EA 1.6.5 (https://github.com/vector-im/element-android/releases/tag/v1.6.5)

Improvements 🙌:
- MESSENGER-4729 Toggle personal notes room
- MESSENGER-4801 Show personal notes room in favorites
- MESSENGER-4906 Forwarding locations
- MESSENGER-4909 Forwarding txt files
- MESSENGER-4945 Removed beta features
- MESSENGER-5070 ISO-8601 time format for maintenance messages
- MESSENGER-5183 Key-backup import

Bugfix 🐛:
- MESSENGER-5121 Fixed "send button" for videos
- MESSENGER-5185 Fixed refresh issues with personal notes room

Changes in BWI project 2.9.0 (2023-09-04)
===================================================
Upstream merge ✨:
- EA 1.6.5 (https://github.com/vector-im/element-android/releases/tag/v1.6.5)

Improvements 🙌:
- MESSENGER-4866 New colors
- MESSENGER-4900 Huddle permissions for new rooms

Bugfix 🐛:
- MESSENGER-4723 People search: User appears in wrong section
- MESSENGER-4856 Wrong UI- with permalink to left room


Changes in BWI project 2.8.0 (2023-08-1)
===================================================
Upstream merge ✨:
- EA 1.6.3 (https://github.com/vector-im/element-android/releases/tag/v1.6.3)

Improvements 🙌:
- MESSENGER-4487 Show poll participants in the poll history
- MESSENGER-4797 Changed date format for the poll participants view
- MESSENGER-4904 Element call related changes
- MESSENGER-4919 Set the new device messenger as default for beta users
- MESSENGER-4958 Activate Voice Broadcast for beta users

Bugfix 🐛:
- MESSENGER-4799 Show poll participants only after voting


Changes in BWI project 2.7.0 (2023-07-03)
===================================================
Features ✨:
- MESSENGER-4154 Show downtime information with custom messages
- MESSENGER-4485 Show poll participatns PART 1
- MESSENGER-4486 Show poll participants PART 2
- MESSENGER-4684 Configurable imprint link
- MESSENGER-4752 Configurable data privacy link
- MESSENGER-4773 Accessibility statement

Improvements 🙌:
- MESSENGER-4302 Made home screen tabs scrollable
- MESSENGER-4508 Accessibility improvements
- MESSENGER-4562 Accessibility improvements
- MESSENGER-4500 Accessibility improvements
  MESSENGER-4639 Accessibility improvementsf
- MESSENGER-4659 Accessibility improvements
- MESSENGER-4745 Removed black theme
- MESSENGER-4771 Removed not used badge in room list

Bugfix 🐛:
- MESSENGER-4768 Show error message if home server is not reachable
- MESSENGER-4786, MESSENGER-4792, MESSENGER-4793 Fixed some crashes and ANRs
- MESSENGER-4931 Hide data privacy link and imprint link in legals screen, if not configured


Changes in BWI project 2.6.0 (2023-05-08)
===================================================
Upstream merge ✨:
- EA 1.5.30 (https://github.com/vector-im/element-android/releases/tag/v1.5.30)

Features ✨:
- MESSENGER-4517 open room links from QR code 

Improvements 🙌:
- MESSENGER-4267 added content description for form elements
- MESSENGER-4269 added content description for form elements
- MESSENGER-4447 BWI specific device manager adjustments
- MESSENGER-4455 changed Matomo privacy policy link
- MESSENGER-4501 content description for form elements
- MESSENGER-4503 content description for profile pictures
- MESSENGER-4550 activated location sharing

Bugfix 🐛:
- MESSENGER-4181 fixed wording in reset password dialog
- MESSENGER-4431 fixed video compression


Changes in BWI project 2.5.0 (2023-04-20)
===================================================
Upstream merge ✨:
- EA 1.5.28 (https://github.com/vector-im/element-android/releases/tag/v1.5.28)

Features ✨:
- MESSENGER-4202  show my QR code
- MESSENGER-4092  scan permalinks as QR codes

Improvements 🙌:
- MESSENGER-4270  content description for form elements

Other changes:
- MESSENGER-4159  changed login server icon

Changes in BWI project 2.4.0 (2023-03-13)
===================================================
Upstream merge ✨:
- EA 1.5.22 (https://github.com/vector-im/element-android/releases/tag/v1.5.22)

Features ✨:
- MESSENGER-837 Video capturing
- MESSENGER-4266 Show current date in the timeline on scrolling

Improvements 🙌:
- MESSENGER-4412 PIN code hashing


Changes in BWI project 2.3.1 (2023-02-22)
===================================================
Improvements 🙌:
- MESSENGER-4225 Refactored the .well-known requests to use a default value if failing


Changes in BWI project 2.3.0 (2023-02-09)
===================================================
Upstream merge ✨:
- EA 1.5.18 (https://github.com/vector-im/element-android/releases/tag/v1.5.18)

Improvements 🙌:
- MESSENGER-3167 Developer setting for setting a permalink hosts (for testing)
- MESSENGER-3926 Finished work for the new layout and activated it as default
- MESSENGER-4002 Prevent permalink creation for private rooms and DMs

Build 🧱:
- MESSENGER-4035 Fixed sonarqube job

Bugfix 🐛:
- MESSENGER-4037 Fixed crash on root detection
- MESSENGER-4105 Fixed feature flag for incoming sharing intents


Changes in BWI project 2.2.0 (2023-01-18)
===================================================
Upstream merge ✨:
- EA 1.5.14 (https://github.com/vector-im/element-android/releases/tag/v1.5.14)

Features ✨:
- MESSENGER-3947 Test notification time settings in notification troubleshooting screen

Improvements 🙌:
- MESSENGER-3934 New menu for TOC and privacy topics
- MESSENGER-3960 Switched from "clean cache" to "do initial sync"
- MESSENGER-4002 Share locations without GPS signal
- MESSENGER-4003 Use configured map server for showing shared locations in the timeline
- MESSENGER-3568 Activate permalinks

Bugfix 🐛:
- MESSENGER-3924 Fixed logout taking too long


Changes in BWI project 2.1.0 (2022-12-14)
===================================================
Upstream merge ✨:
- EA 1.5.7 (https://github.com/vector-im/element-android/releases/tag/v1.5.7)

Features ✨:
- MESSENGER-3781 Welcome experience (onboarding slider)
- MESSENGER-3617 Fetch and display the privacy link on login screen
- 
Improvements 🙌:
- MESSENGER-3956 New app logo
- MESSENGER-3628 Content Scanner url derived from the Home Server url
- MESSENGER-3667 Error dialog on non allowed server selection

Bugfix 🐛:
- MESSENGER-3787 Fixed working times screen layout
- MESSENGER-3773 Fixed crash after logout
- MESSENGER-3768 Fixed error message on password change


Changes in BWI project 2.0.0 (2022-11-21)
===================================================
Upstream merge ✨:
- EA 1.5.4 (https://github.com/vector-im/element-android/releases/tag/v1.5.4)

Features ✨:
- MESSENGER-3764 QR-Code Scan for server urls

Improvements 🙌:
- MESSENGER-3619 Splash screen animation
- MESSENGER-3623 Changes support and help texts
- MESSENGER-3655 Changed login flow (force server selection first)
- MESSENGER-3665 Changed logo
- MESSENGER-3786 Enable push troubleshooting dialog
- MESSENGER-3856 Apply BWI features to the new layout


Changes in BWI project 1.26.0 (2022-10-23)
===================================================
Upstream merge ✨:
- EA 1.5.0 (https://github.com/vector-im/element-android/releases/tag/v1.5.0)

Improvements 🙌:
- MESSENGER-2952 Changed the copyright url
- MESSENGER-2576 Renamed closed polls
- MESSENGER-3686 Adjusted the bottom navigation colors according to BITV 2.0


Changes in BWI project 1.25.0 (2022-09-23)
===================================================
Upstream merge ✨:
- EA 1.4.34 (https://github.com/vector-im/element-android/releases/tag/v1.4.34)

#Features ✨:
 - MESSENGER-3547 Added a link to the feature table

Improvements 🙌:
- MESSENGER-3607 Added new Matomo integration with user's content saved in the account data 
- MESSENGER-2505 Analytics for slow "send text message" events 
- MESSENGER-3467 Added a "close" button to the top overlay popup


Changes in BWI project 1.24.0 (2022-08-29)
===================================================
Upstream merge ✨:
- EA 1.4.32 (https://github.com/vector-im/element-android/releases/tag/v1.4.32)

Features ✨:
- MESSENGER-3228 Enable host name validation on login with matrix-id

Improvements 🙌:
- MESSENGER-2620 Hide notifications on certain event types
- MESSENGER-3422 Renamed "standard" role to "memeber" 


Changes in BWI project 1.23.0 (2022-08-09)
===================================================
Upstream merge ✨:
- EA 1.4.30 (https://github.com/vector-im/element-android/releases/tag/v1.4.30)

Improvements 🙌:
- MESSENGER-3308Android Changed avatar for notes room

Bugfix 🐛:
- MESSENGER-3499 Content scanner implementation for audio files


Changes in BWI project 1.22.1 (2022-07-19)
===================================================
Upstream merge ✨:
- EA 1.4.19 (https://github.com/vector-im/element-android/releases/tag/v1.4.19)

Features ✨:
- MESSENGER-3119 Location sharing with custom map server
- MESSENGER-3164 MDM permalink prefix configuration

Improvements 🙌:
- MESSENGER-328 Automatic alias generation for rooms

Bugfix 🐛:
- MESSENGER-3299 Prevent empty function names

Other changes:
- MESSENGER-3386 Activated cross-signing for Beta users


Changes in BWI project 1.21.0 (2022-06-02)
===================================================

Upstream merge ✨:
 - EA 1.4.14 (https://github.com/vector-im/element-android/releases/tag/v1.4.14)
 
Features ✨:
 - MESSENGER-1969 Extended notification times settings
 - MESSENGER-2933 Save notification times configuration in user's account data
 - MESSENGER-3158 "Standard"-setting for per room notifications

Improvements 🙌:
- MESSENGER-3220 Speach bubbles setting is on 
- MESSENGER-3138 "Change password" Dialog refactoring
- MESSENGER-3117 Open password protected PDFs
- 
