/*
 * Copyright 2020 The Matrix.org Foundation C.I.C.
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.matrix.android.sdk.internal.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import de.bwi.bwmessenger.features.customheader.CustomHeaderInterceptor
import de.bwi.bwmessenger.features.customheader.CustomHeaderService
import de.bwi.messenger.features.maintenance.api.BlockingMaintenanceInterceptor
import okhttp3.ConnectionSpec
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import org.matrix.android.sdk.BuildConfig
import org.matrix.android.sdk.api.MatrixConfiguration
import org.matrix.android.sdk.internal.network.ApiInterceptor
import org.matrix.android.sdk.internal.network.TimeOutInterceptor
import org.matrix.android.sdk.internal.network.UserAgentInterceptor
import org.matrix.android.sdk.internal.network.httpclient.applyMatrixConfiguration
import org.matrix.android.sdk.internal.network.interceptors.CurlLoggingInterceptor
import org.matrix.android.sdk.internal.network.interceptors.FormattedJsonHttpLogger
import java.util.Collections
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
internal object NetworkModule {

    @Provides
    @JvmStatic
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logger = FormattedJsonHttpLogger(BuildConfig.OKHTTP_LOGGING_LEVEL)
        val interceptor = HttpLoggingInterceptor(logger)
        interceptor.level = BuildConfig.OKHTTP_LOGGING_LEVEL
        return interceptor
    }

    @Provides
    @JvmStatic
    @Named("DebugInterceptor")
    fun providesDebugInterceptor(): Interceptor? {
        return null
    }

    @Provides
    @JvmStatic
    fun providesCurlLoggingInterceptor(): CurlLoggingInterceptor {
        return CurlLoggingInterceptor()
    }

    @Provides
    @JvmStatic
    fun provideCustomHeaderInterceptor(customHeaderProvder: CustomHeaderService): CustomHeaderInterceptor {
        return CustomHeaderInterceptor(customHeaderProvder)
    }

    @MatrixScope
    @Provides
    @JvmStatic
    @Unauthenticated
    fun providesOkHttpClient(
            matrixConfiguration: MatrixConfiguration,
            @Named("DebugInterceptor") stethoInterceptor: Interceptor?,
            timeoutInterceptor: TimeOutInterceptor,
            userAgentInterceptor: UserAgentInterceptor,
            curlLoggingInterceptor: CurlLoggingInterceptor,
            apiInterceptor: ApiInterceptor,
            customHeaderInterceptor: CustomHeaderInterceptor,
            blockingMaintenanceInterceptor: BlockingMaintenanceInterceptor,
    ): OkHttpClient {
        val spec = ConnectionSpec.Builder(matrixConfiguration.connectionSpec).build()
        val dispatcher = Dispatcher().apply {
            maxRequestsPerHost = 20
        }
        return OkHttpClient.Builder()
                // workaround for #4669
                .protocols(listOf(Protocol.HTTP_1_1))
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .apply {
                    if (BuildConfig.DEBUG) {
                        stethoInterceptor?.let {
                            addNetworkInterceptor(it)
                        }
                    }
                }
                .addInterceptor(blockingMaintenanceInterceptor)
                .addInterceptor(timeoutInterceptor)
                .addInterceptor(customHeaderInterceptor)
                .addInterceptor(userAgentInterceptor)
                .addInterceptor(apiInterceptor)
                .apply {
                    if (BuildConfig.LOG_PRIVATE_DATA) {
                        addInterceptor(curlLoggingInterceptor)
                    }
                    matrixConfiguration.proxy?.let {
                        proxy(it)
                    }
                }
                .dispatcher(dispatcher)
                .connectionSpecs(Collections.singletonList(spec))
                .applyMatrixConfiguration(matrixConfiguration)
                .build()
    }

    @Provides
    @JvmStatic
    fun providesMoshi(): Moshi {
        return MoshiProvider.providesMoshi()
    }
}
