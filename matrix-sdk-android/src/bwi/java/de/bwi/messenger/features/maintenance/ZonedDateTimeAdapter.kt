/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter

class ZonedDateTimeAdapter {
    @ToJson
    fun toJson(value: ZonedDateTime): String {
        return FORMATTER.format(value)
    }

    @FromJson
    fun fromJson(value: String): ZonedDateTime {
        return FORMATTER.parse(value, ZonedDateTime::from)
    }

    companion object {
        private val FORMATTER = DateTimeFormatter.ISO_ZONED_DATE_TIME
    }
}
