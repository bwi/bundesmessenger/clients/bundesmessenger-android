/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.workingtime

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.internal.di.UserId
import org.matrix.android.sdk.internal.session.user.accountdata.AccountDataAPI
import org.matrix.android.sdk.internal.session.user.accountdata.UserAccountDataDataSource
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDateTime
import javax.inject.Inject

internal class DefaultWorkingTimesService @Inject constructor(
        private val userAccountDataDataSource: UserAccountDataDataSource,
        private val userAccountApi: AccountDataAPI,
        @UserId private val userId: String,
) : WorkingTimesService {

    override suspend fun setWorkingTimeConfiguration(configuration: Map<DayOfWeek, WorkingTime>) {
        currentWorkingTimeContent().let { content ->
            val newEntries = DayOfWeek.values().map { configuration[it]!!.fromDomain() }
            val newData = content.copy(entries = newEntries)
            withContext(Dispatchers.IO) {
                userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_NOTIFICATION_TIMES, newData)
            }
        }
    }

    override suspend fun setWorkingTimeEnabled(isEnabled: Boolean) {
        val newData = currentWorkingTimeContent().copy(isEnabled = isEnabled)
        withContext(Dispatchers.IO) {
            userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_NOTIFICATION_TIMES, newData)
        }
    }

    override fun isWorkingTimeEnabled(): Boolean {
        return currentWorkingTimeContent().isEnabled
    }

    override fun isWorkingTimeEnabledForRoom(roomId: String, isDirect: Boolean): Boolean {
        val roomEntries = currentWorkingTimeContent().rooms
        return if (isDirect) {
            roomEntries.any { it.roomId == roomId && it.isEnabled }
        } else {
            roomEntries.none { it.roomId == roomId } || roomEntries.any { it.roomId == roomId && it.isEnabled }
        }
    }

    override suspend fun setWorkingTimeEnabledForRoom(roomId: String, isEnabled: Boolean, isDirect: Boolean) {
        currentWorkingTimeContent().let { content ->
            val newEntries = content.rooms.filter { it.roomId != roomId }.toMutableList()
            if (isDirect && isEnabled) {
                newEntries.add(WorkingTimeContentRoom(roomId, true))
            } else if (!isDirect && !isEnabled) {
                newEntries.add(WorkingTimeContentRoom(roomId, false))
            }
            val newData = content.copy(rooms = newEntries)

            withContext(Dispatchers.IO) {
                userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_NOTIFICATION_TIMES, newData)
            }
        }
    }

    override fun shouldWorkingTimeBlockNotification(roomId: String?, isDirect: Boolean): Boolean {
        return if (isWorkingTimeEnabled() && roomId != null) {
            isWorkingTimeEnabledForRoom(roomId, isDirect) && !isWorkingTimeNow()
        } else if (isWorkingTimeEnabled()) {
            !isWorkingTimeNow()
        } else {
            false
        }
    }

    override fun getWorkingTimeConfiguration(): Map<DayOfWeek, WorkingTime> {
        val content = currentWorkingTimeContent()
        return DayOfWeek.values().associateWith { content.entries[it.ordinal].asDomain() }
    }

    private fun isWorkingTimeNow(): Boolean {
        val now = LocalDateTime.now()
        val result = getWorkingTimeConfiguration()[now.dayOfWeek]
        return result?.isEnabled == true && result.contains(now.toLocalTime())
    }

    private fun currentWorkingTimeContent(): WorkingTimeContent {
        val configuration = userAccountDataDataSource.getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES)?.content
                .toModel<WorkingTimeContent>(catchError = true) ?: createDefaultConfiguration()
        return if (configuration.entries.isNotEmpty() && configuration.entries.size == DayOfWeek.values().size) configuration else createDefaultConfiguration()
    }

    private fun createDefaultConfiguration(): WorkingTimeContent {
        val entries = DayOfWeek.values().map { WorkingTimeContentEntry(it != DayOfWeek.SATURDAY && it != DayOfWeek.SUNDAY, 8, 0, 17, 0) }
        return WorkingTimeContent(entries = entries)
    }
}
