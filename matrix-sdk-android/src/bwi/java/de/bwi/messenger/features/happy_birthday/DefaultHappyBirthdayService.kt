/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.happy_birthday

import de.bwi.messenger.features.notifications.NotificationsService
import javax.inject.Inject

internal class DefaultHappyBirthdayService @Inject constructor(
        private val notificationsService: NotificationsService,
) : HappyBirthdayService {

    override suspend fun shouldShowHappyBirthday(year: Int): Boolean =
            notificationsService.getNotificationsContent().shouldShowHappyBirthday.let {
                when (year) {
                    2025 -> it.year2025
                    else -> false
                }
            }

    override suspend fun setShouldShowHappyBirthday(value: Boolean, year: Int) {
        notificationsService.apply {
            setNotificationsContent(
                    getNotificationsContent().apply {
                        shouldShowHappyBirthday.apply {
                            when (year) {
                                2025 -> year2025 = value
                            }
                        }
                    }
            )
        }
    }
}
