/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.bwmessenger.features.customheader

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class CustomHeaderInterceptor(private val customHeaderService: CustomHeaderService) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        request = addCustomHeadersToRequest(request)
        return chain.proceed(request)
    }

    private fun addCustomHeadersToRequest(request: Request): Request {
        var newRequest = request
        CustomHeader.values().forEach { customHeader ->
            customHeaderService.getCustomHeaderValue(customHeader)?.let { value ->
                val newRequestBuilder = newRequest.newBuilder()
                newRequestBuilder.header(customHeader.headerName, value)
                newRequest = newRequestBuilder.build()
            }
        }
        return newRequest
    }
}
