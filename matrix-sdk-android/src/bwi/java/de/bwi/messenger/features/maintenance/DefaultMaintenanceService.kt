/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance

import de.bwi.messenger.features.maintenance.api.DowntimeInfo
import de.bwi.messenger.features.maintenance.api.MaintenanceApiFactory
import de.bwi.messenger.features.maintenance.api.MaintenanceResponse
import de.bwi.messenger.features.maintenance.api.VersionsInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.matrix.android.sdk.api.failure.Failure
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import javax.inject.Inject

internal class DefaultMaintenanceService @Inject constructor(
        private val maintenanceApiFactory: MaintenanceApiFactory,
) : MaintenanceService {
    override suspend fun fetchMaintenanceData(homeServerUrl: String) {
        val maintenanceApi = maintenanceApiFactory.createMaintenanceApi(homeServerUrl)
        runCatching<MaintenanceResponse?> {
            withContext(Dispatchers.IO) {
                maintenanceApi.getMaintenanceData()
            }
        }.onFailure {
            if (it is Failure.ServerError && it.httpCode == 444) {
                // this is a special case where the server is not reachable and we receive the DMZ error code
                cachedDowntimeInfo = DowntimeInfo(
                        ZonedDateTime.of(LocalDateTime.MIN, ZoneId.systemDefault()),
                        ZonedDateTime.of(LocalDateTime.MAX, ZoneId.systemDefault()),
                        ZonedDateTime.of(LocalDateTime.MIN, ZoneId.systemDefault()),
                        DowntimeInfo.TYPE_INFO_MESSAGE,
                        null,
                        true
                )
            }
        }.getOrNull()?.let { response ->
            val now = ZonedDateTime.now(ZoneId.systemDefault())
            val sortedDowntimes = response.downtime
                    .filter {
                        when {
                            // check type
                            it.isMaintenanceType().not() && it.isInfoMessageType().not() -> false

                            // warning start time must not be greater than start time
                            it.warningStartTime > it.startTime -> false

                            // check description for adhoc message
                            it.isInfoMessageType() && it.description.isNullOrBlank() -> false

                            it.startTime > it.endTime -> false

                            else -> true
                        }
                    }
                    .sortedBy { it.startTime }
            cachedDowntimeInfo = sortedDowntimes.firstOrNull { it.isActive() }
                    ?: sortedDowntimes.firstOrNull { now in it.localizedWarningStartTime()..it.localizedEndTime() }
            cachedVersionsInfo = response.versionsInfo?.versions
        }
    }

    override fun getCurrentDowntimeInfo() = cachedDowntimeInfo
    override fun getVersionsInfo(): VersionsInfo? = cachedVersionsInfo
    override fun setBlockOnBlockingDowntime(block: Boolean) {
        shouldBlockOnBlockingDowntime = block
    }

    override fun shouldBlockOnBlockingDowntime(): Boolean {
        return shouldBlockOnBlockingDowntime
    }

    companion object {

        internal fun invalidateCache() {
            cachedDowntimeInfo = null
            cachedVersionsInfo = null
        }

        private var cachedDowntimeInfo: DowntimeInfo? = null
        private var cachedVersionsInfo: VersionsInfo? = null
        private var shouldBlockOnBlockingDowntime = true
    }
}
