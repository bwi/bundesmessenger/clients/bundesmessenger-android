/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.release_notes

import de.bwi.messenger.features.notifications.NotificationsService
import de.bwi.messenger.features.notifications.ReleaseNotesHintEntry
import javax.inject.Inject

internal class DefaultReleaseNotesService @Inject constructor(
        private val notificationsService: NotificationsService,
) : ReleaseNotesService {

    override suspend fun shouldShowReleaseNotesHint(version: String): Boolean {
        val content = notificationsService.getNotificationsContent()
        return content.releaseNotesEntries.none { it.version == trimmedVersion(version) } ||
                content.releaseNotesEntries.any { it.version == trimmedVersion(version) && it.showHint }
    }

    override suspend fun setShouldShowReleaseNotesHint(version: String, shouldShow: Boolean) {
        val content = notificationsService.getNotificationsContent()
        val entries = content.releaseNotesEntries
        val existingEntryIndex = entries.indexOfFirst { entry -> entry.version == trimmedVersion(version) }
        if (existingEntryIndex < 0) {
            entries.add(ReleaseNotesHintEntry(trimmedVersion(version), shouldShow))
        } else {
            entries[existingEntryIndex].showHint = shouldShow
        }
        content.releaseNotesEntries = entries
        notificationsService.setNotificationsContent(content)
    }

    override suspend fun shouldShowFederationAnnouncement(): Boolean {
        return notificationsService.getNotificationsContent().shouldShowFederationAnnouncementDialog
    }

    override suspend fun setShouldShowFederationAnnouncement(shouldShow: Boolean) {
        val content = notificationsService.getNotificationsContent()
        content.shouldShowFederationAnnouncementDialog = shouldShow
        notificationsService.setNotificationsContent(content)
    }

    override suspend fun shouldShowFederationIntroduction(): Boolean {
        return notificationsService.getNotificationsContent().shouldShowFederationIntroductionDialog
    }

    override suspend fun setShouldShowFederationIntroduction(shouldShow: Boolean) {
        val content = notificationsService.getNotificationsContent()
        content.shouldShowFederationIntroductionDialog = shouldShow
        notificationsService.setNotificationsContent(content)
    }

    private fun trimmedVersion(version: String): String {
        return version.split(".").take(2).joinToString(".")
    }
}
