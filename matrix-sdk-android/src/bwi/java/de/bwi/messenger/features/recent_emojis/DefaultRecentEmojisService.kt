/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.recent_emojis

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import kotlinx.coroutines.withContext
import org.matrix.android.sdk.api.MatrixCoroutineDispatchers
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.util.Optional
import org.matrix.android.sdk.internal.di.UserId
import org.matrix.android.sdk.internal.session.user.accountdata.AccountDataAPI
import org.matrix.android.sdk.internal.session.user.accountdata.UserAccountDataDataSource
import javax.inject.Inject

internal class DefaultRecentEmojisService @Inject constructor(
        @UserId private val userId: String,
        private val userAccountApi: AccountDataAPI,
        private val dispatchers: MatrixCoroutineDispatchers,
        private val userAccountDataDataSource: UserAccountDataDataSource
) : RecentEmojisService {

     override fun getRecentEmojis(): LiveData<List<String>> {
         return userAccountDataDataSource.getLiveAccountDataEvent(UserAccountDataTypes.TYPE_RECENT_EMOJI).map {
             if (it.getOrNull()?.content?.isNotEmpty() == true) {
                 val data = Optional.from(it.get().content.toModel<RecentEmojisContent>()).get()
                 data.list.filter { it.size == 2 && it[0] is String && it[1] is Double }.sortedByDescending { it[1] as Double }.map { it[0] as String }.take(8).toList()
             } else {
                 emptyList()
             }
         }
     }

    override suspend fun saveRecentUsedEmoji(emoji: String) {
        val currentRecentEmojisEvent = userAccountDataDataSource.getAccountDataEvent(UserAccountDataTypes.TYPE_RECENT_EMOJI)
        val currentRecentEmojisContent = currentRecentEmojisEvent?.content.toModel<RecentEmojisContent>() ?: RecentEmojisContent()
        val existingIndex = currentRecentEmojisContent.list.indexOfFirst { it.size == 2 && it[0] is String && it[0] == emoji }
        if (existingIndex >= 0) {
            val entry = currentRecentEmojisContent.list[existingIndex]
            if (entry.size == 2 && entry[1] is Double) {
                var previousCount = entry[1] as Double
                currentRecentEmojisContent.list[existingIndex] = listOf(emoji, ++previousCount)
            }
        } else {
            val newIndex = currentRecentEmojisContent.list.indexOfFirst { (it[1] as Double) == 1.0 }
            if (newIndex >= 0) {
                currentRecentEmojisContent.list.add(newIndex, listOf(emoji, 1.0))
            } else {
                currentRecentEmojisContent.list.add(listOf(emoji, 1.0))
            }
        }
        withContext(dispatchers.io) {
            kotlin.runCatching {
                userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_RECENT_EMOJI, currentRecentEmojisContent)
            }
        }
    }
}
