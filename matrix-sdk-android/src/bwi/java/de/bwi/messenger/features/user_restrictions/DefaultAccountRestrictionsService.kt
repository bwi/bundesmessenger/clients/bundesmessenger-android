/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.user_restrictions

import android.content.res.Resources
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.internal.di.UserId
import org.matrix.android.sdk.internal.session.user.accountdata.AccountDataAPI
import org.matrix.android.sdk.internal.session.user.accountdata.UserAccountDataDataSource
import javax.inject.Inject

internal class DefaultAccountRestrictionsService @Inject constructor(
        private val userAccountDataDataSource: UserAccountDataDataSource,
        private val userAccountApi: AccountDataAPI,
        @UserId private val userId: String,
        private val resources: Resources
) : AccountRestrictionsService {

    override fun getAccountRestrictions(): AccountRestrictionsContent {
        return userAccountDataDataSource.getAccountDataEvent(UserAccountDataTypes.TYPE_ACCOUNT_RESTRICTIONS)?.content.toModel<AccountRestrictionsContent>() ?: AccountRestrictionsContent()
    }

    override suspend fun setAccountRestrictions(restrictions: AccountRestrictionsContent) {
        userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_ACCOUNT_RESTRICTIONS, restrictions)
    }
}
