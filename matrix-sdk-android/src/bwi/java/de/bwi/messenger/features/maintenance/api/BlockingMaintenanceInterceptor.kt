/*
 * Copyright 2020 The Matrix.org Foundation C.I.C.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance.api

import de.bwi.messenger.features.maintenance.MaintenanceService
import okhttp3.Interceptor
import okhttp3.Response
import okio.IOException
import org.matrix.android.sdk.internal.network.NetworkConstants
import javax.inject.Inject

internal class BlockingMaintenanceInterceptor @Inject constructor(
        private val maintenanceService: MaintenanceService
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        if (maintenanceService.shouldBlockOnBlockingDowntime() &&
                maintenanceService.getCurrentDowntimeInfo()?.isActive() == true &&
                maintenanceService.getCurrentDowntimeInfo()?.blocking == true &&
                !request.url.encodedPath.contains(
                        NetworkConstants.URI_API_MAINTENANCE
                )
        ) {
            throw IOException("Maintenance is blocking")
        } else {
            return chain.proceed(request)
        }
    }
}
