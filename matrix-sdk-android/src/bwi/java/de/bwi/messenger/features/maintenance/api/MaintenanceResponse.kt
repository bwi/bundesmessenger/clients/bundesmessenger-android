/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

@JsonClass(generateAdapter = true)
data class DowntimeInfo(
        @Json(name = "start_time")
        val startTime: ZonedDateTime,
        @Json(name = "end_time")
        val endTime: ZonedDateTime,
        @Json(name = "warning_start_time")
        val warningStartTime: ZonedDateTime,
        @Json(name = "type")
        val type: String,
        @Json(name = "description")
        val description: String?,
        @Json(name = "blocking")
        val blocking: Boolean = false
) {

    fun localizedStartTime(): ZonedDateTime {
        return startTime.withZoneSameInstant(ZoneId.systemDefault())
    }

    fun localizedEndTime(): ZonedDateTime {
        return endTime.withZoneSameInstant(ZoneId.systemDefault())
    }

    fun localizedWarningStartTime(): ZonedDateTime {
        return warningStartTime.withZoneSameInstant(ZoneId.systemDefault())
    }

    fun isActive(): Boolean {
        return ZonedDateTime.now(ZoneId.systemDefault()) in localizedStartTime()..localizedEndTime()
    }

    fun isStartAndEndOnSameDay(): Boolean {
        return localizedStartTime().year == localizedEndTime().year &&
                localizedStartTime().dayOfYear == localizedEndTime().dayOfYear
    }

    fun isMaintenanceType(): Boolean {
        return type == TYPE_MAINTENANCE
    }

    fun isInfoMessageType(): Boolean {
        return type == TYPE_INFO_MESSAGE
    }

    companion object {
        val TYPE_MAINTENANCE = "MAINTENANCE"
        val TYPE_INFO_MESSAGE = "ADHOC_MESSAGE"
    }
}

@JsonClass(generateAdapter = true)
data class BwiVersionsInfo(
        @Json(name = "android")
        val versions: VersionsInfo?
)

@JsonClass(generateAdapter = true)
data class VersionsInfo(
        @Json(name = "update_before")
        val updateBeforeVersion: String,
        @Json(name = "warn_before")
        val warnBeforeVersion: String
) {
    fun isOutdated(version: String): Boolean {
        return isV1OlderThanV2(v1 = version, v2 = updateBeforeVersion)
    }

    fun shouldUpdate(version: String): Boolean {
        return isV1OlderThanV2(v1 = version, v2 = warnBeforeVersion)
    }

    private fun isV1OlderThanV2(v1: String, v2: String): Boolean {
        return kotlin.runCatching {
            versionParts(v1)
                .zip(versionParts(v2))
                .first { it.first != it.second }
                .let { it.first < it.second }
        }
                .getOrDefault(false)
    }

    private fun versionParts(version: String): List<Int> {
        return version.split(".").map { it.toIntOrNull() ?: 0 }.toList()
    }
}

@JsonClass(generateAdapter = true)
data class MaintenanceResponse(
        @Json(name = "downtime") val downtime: List<DowntimeInfo> = emptyList(),
        @Json(name = "versions") val versionsInfo: BwiVersionsInfo?
)
