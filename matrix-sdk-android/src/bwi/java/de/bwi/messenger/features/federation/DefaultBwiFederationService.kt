/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import de.bwi.messenger.features.room_alias.BwiRoomAliasService
import org.matrix.android.sdk.api.MatrixPatterns.getServerName
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toContent
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.RoomService
import org.matrix.android.sdk.api.session.room.getStateEvent
import org.matrix.android.sdk.api.session.room.model.RoomServerAclContent
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.internal.di.UserId
import javax.inject.Inject

internal class DefaultBwiFederationService @Inject constructor(
        private val roomService: RoomService,
        private val bwiRoomAliasService: BwiRoomAliasService,
        @UserId private val userId: String,
) : BwiFederationService {

//    uncomment to use in room's profile view for live updates
//    override fun isRoomFederatedLive(roomId: String): LiveData<Boolean> {
//        return Transformations.map(roomService.getRoom(roomId)?.stateService()?.getStateEventLive(EventType.STATE_ROOM_SERVER_ACL, QueryStringValue.IsNotNull) ?: liveData { Optional(false) }) { optional ->
//            val eventContent = optional.getOrNull()?.content.toModel<RoomServerAclContent>()
//            eventContent?.allowList?.contains("*") == true
//        }
//    }

    override fun isRoomFederated(roomSummary: RoomSummary): Boolean {
        return if (roomSummary.isDirect) {
            userId.getServerName() != roomSummary.directUserId?.getServerName() && roomSummary.directUserId != null
        } else {
            val aclEvent = roomService.getRoom(roomSummary.roomId)?.getStateEvent(EventType.STATE_ROOM_SERVER_ACL, QueryStringValue.IsNotNull)
            val eventContent = aclEvent?.content.toModel<RoomServerAclContent>()
            aclEvent == null || eventContent?.allowList?.contains(RoomServerAclContent.ALL) == true
        }
    }

    override fun isFederationSetForRoom(roomId: String): Boolean {
        val aclEvent = roomService.getRoom(roomId)?.getStateEvent(EventType.STATE_ROOM_SERVER_ACL, QueryStringValue.IsNotNull)
        return aclEvent != null
    }

    override suspend fun setFederationForRoom(roomId: String, isFederated: Boolean) {
        val listOfAllowedServer = if (isFederated) listOf(RoomServerAclContent.ALL) else listOf(userId.getServerName())
        roomService.getRoom(roomId)?.let { room ->
            if (isFederated && room.aliasService().getRoomAliases().isEmpty()) {
                room.aliasService().addAlias(bwiRoomAliasService.getAliasForRoomName(room.roomSummary()?.name))
            }

            room.stateService().sendStateEvent(
                    eventType = EventType.STATE_ROOM_SERVER_ACL,
                    stateKey = "",
                    body = RoomServerAclContent(
                            allowIpLiterals = false,
                            allowList = listOfAllowedServer,
                            denyList = null
                    ).toContent()
            )
        }
    }
}
