/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.tracking

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.events.model.toMapModel
import org.matrix.android.sdk.internal.di.DeviceId
import org.matrix.android.sdk.internal.di.UserId
import org.matrix.android.sdk.internal.session.user.accountdata.AccountDataAPI
import org.matrix.android.sdk.internal.session.user.accountdata.UserAccountDataDataSource
import javax.inject.Inject

internal class DefaultAnalyticsConsentService @Inject constructor(
        private val userAccountDataDataSource: UserAccountDataDataSource,
        private val userAccountApi: AccountDataAPI,
        @UserId private val userId: String,
        @DeviceId private val deviceId: String?
) : AnalyticsConsentService {

    override suspend fun shouldAskForAnalyticsConsent(): Boolean {
        return withContext(Dispatchers.IO) {
            getCurrentContentDataFromServer().containsKey(deviceId).not()
        }
    }

    override fun isTrackingEnabledLive(): LiveData<Boolean> {
        return userAccountDataDataSource.getLiveAccountDataEvent(UserAccountDataTypes.TYPE_BWI_TRACKING).map {
            it.getOrNull()?.content.toMapModel<ConsentData>()?.get(deviceId)?.consent ?: false
        }
    }

    override fun isTrackingEnabled(): Boolean {
        return userAccountDataDataSource.getAccountDataEvent(UserAccountDataTypes.TYPE_BWI_TRACKING)
                ?.content.toMapModel<ConsentData>()
                ?.get(deviceId)?.consent ?: false
    }

    override suspend fun setTrackingEnabled(appVersion: String, value: Boolean) {
        deviceId?.let {
            withContext(Dispatchers.IO) {
                val content = getCurrentContentDataFromServer()
                content[it] = ConsentData(time = System.currentTimeMillis(), appVersion = appVersion, consent = value)
                userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_BWI_TRACKING, content)
            }
        }
    }

    private suspend fun getCurrentContentDataFromServer(): MutableMap<String, ConsentData> {
        return kotlin.runCatching { userAccountApi.getAccountData(userId, UserAccountDataTypes.TYPE_BWI_TRACKING)?.toMapModel<ConsentData>()?.toMutableMap() ?: mutableMapOf() }.getOrDefault(mutableMapOf())
    }
}
