/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance.api

import dagger.Lazy
import okhttp3.OkHttpClient
import org.matrix.android.sdk.internal.di.Unauthenticated
import org.matrix.android.sdk.internal.network.RetrofitFactory
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import javax.inject.Inject

internal interface MaintenanceApiFactory {
    fun createMaintenanceApi(homeServerUrl: String): MaintenanceApi
}

internal class DefaultMaintenanceApiFactory @Inject constructor(
        val retrofitFactory: RetrofitFactory,
        @Unauthenticated
        val okHttpClient: Lazy<OkHttpClient>
) : MaintenanceApiFactory {

    override fun createMaintenanceApi(homeServerUrl: String): MaintenanceApi {
        return retrofitFactory.create(okHttpClient, homeServerUrl)
                .create(MaintenanceApi::class.java)
    }
}

internal class TestingMaintenanceApiFactory @Inject constructor() : MaintenanceApiFactory {
    override fun createMaintenanceApi(homeServerUrl: String): MaintenanceApi {
        return object : MaintenanceApi {
            override suspend fun getMaintenanceData(): MaintenanceResponse {
                val downtimes = listOf(
                    DowntimeInfo(
                        ZonedDateTime.of(LocalDateTime.MIN, ZoneId.systemDefault()),
                        ZonedDateTime.of(LocalDateTime.MAX, ZoneId.systemDefault()),
                        ZonedDateTime.of(LocalDateTime.MIN, ZoneId.systemDefault()), "", ""
                    )
                )
                return MaintenanceResponse(downtimes, null)
            }
        }
    }
}
