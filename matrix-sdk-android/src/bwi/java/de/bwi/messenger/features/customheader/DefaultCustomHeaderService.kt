/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.bwmessenger.features.customheader

import com.zhuinden.monarchy.Monarchy
import de.bwi.messenger.database.BwiDatabase
import io.realm.kotlin.where
import org.matrix.android.sdk.internal.util.fetchCopied
import javax.inject.Inject

class DefaultCustomHeaderService @Inject constructor(@BwiDatabase val database: Monarchy) : CustomHeaderService {

    init {
        setCustomHeaderValue(CustomHeader.XVersionId, "1.24.03")
    }

    override fun setCustomHeaderValue(header: CustomHeader, value: String) {
        database.runTransactionSync { realm ->
            val entity = CustomHeaderEntity(header.headerName, value)
            realm.insertOrUpdate(entity)
        }
    }

    override fun getCustomHeaderValue(header: CustomHeader): String? {
        return kotlin.runCatching {
            database.fetchCopied { realm ->
                realm.where<CustomHeaderEntity>()
                        .equalTo(CustomHeaderEntityFields.HEADER_NAME, header.headerName)
                        .findFirst()
            }?.headerValue
        }.getOrNull()
    }

    override fun clearAllCustomHeaderValues() {
        database.runTransactionSync { realm ->
            realm.where<CustomHeaderEntity>()
                    .findAll()
                    .deleteAllFromRealm()
        }
    }
}
