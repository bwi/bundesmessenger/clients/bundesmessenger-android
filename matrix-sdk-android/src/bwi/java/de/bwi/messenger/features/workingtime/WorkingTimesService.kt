/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.workingtime

import org.threeten.bp.DayOfWeek

interface WorkingTimesService {
    suspend fun setWorkingTimeConfiguration(configuration: Map<DayOfWeek, WorkingTime>)
    fun getWorkingTimeConfiguration(): Map<DayOfWeek, WorkingTime>

    suspend fun setWorkingTimeEnabled(isEnabled: Boolean)
    fun isWorkingTimeEnabled(): Boolean

    fun isWorkingTimeEnabledForRoom(roomId: String, isDirect: Boolean): Boolean
    suspend fun setWorkingTimeEnabledForRoom(roomId: String, isEnabled: Boolean, isDirect: Boolean)

    // Notification should not be displayed off-time. For room related notifications, the room id
    // has to be used for checking if the working time rule was disabled by the user for this room
    fun shouldWorkingTimeBlockNotification(roomId: String? = null, isDirect: Boolean = false): Boolean
}
