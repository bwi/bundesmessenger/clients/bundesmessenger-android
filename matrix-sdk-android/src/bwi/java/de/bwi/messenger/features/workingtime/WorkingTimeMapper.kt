/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.workingtime

import org.threeten.bp.LocalTime

internal object WorkingTimeMapper {
    fun map(entry: WorkingTimeContentEntry): WorkingTime {
        return WorkingTime(entry.isEnabled, LocalTime.of(entry.fromHour, entry.fromMinute), LocalTime.of(entry.toHour, entry.toMinute))
    }

    fun map(time: WorkingTime): WorkingTimeContentEntry {
        return WorkingTimeContentEntry(time.isEnabled, time.fromTime.hour, time.fromTime.minute, time.toTime.hour, time.toTime.minute)
    }
}

internal fun WorkingTimeContentEntry.asDomain(): WorkingTime {
    return WorkingTimeMapper.map(this)
}

internal fun WorkingTime.fromDomain(): WorkingTimeContentEntry {
    return WorkingTimeMapper.map(this)
}
