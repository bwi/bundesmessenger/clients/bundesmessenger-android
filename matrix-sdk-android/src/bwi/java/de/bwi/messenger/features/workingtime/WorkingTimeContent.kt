/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.workingtime

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class WorkingTimeContentEntry(
        @Json(name = "is_enabled")
        val isEnabled: Boolean = false,
        @Json(name = "from_hour")
        val fromHour: Int,
        @Json(name = "from_minute")
        val fromMinute: Int,
        @Json(name = "to_hour")
        val toHour: Int,
        @Json(name = "to_minute")
        val toMinute: Int
)

@JsonClass(generateAdapter = true)
internal data class WorkingTimeContentRoom(
        @Json(name = "room_id")
        val roomId: String,
        @Json(name = "is_enabled")
        val isEnabled: Boolean = false
)

@JsonClass(generateAdapter = true)
internal data class WorkingTimeContent(
        @Json(name = "is_enabled")
        val isEnabled: Boolean = false,
        val entries: List<WorkingTimeContentEntry> = emptyList(),
        val rooms: List<WorkingTimeContentRoom> = emptyList()
)
