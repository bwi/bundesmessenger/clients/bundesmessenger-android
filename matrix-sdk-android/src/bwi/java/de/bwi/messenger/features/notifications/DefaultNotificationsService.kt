/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.notifications

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.internal.di.UserId
import org.matrix.android.sdk.internal.session.user.accountdata.AccountDataAPI
import org.matrix.android.sdk.internal.session.user.accountdata.UserAccountDataDataSource
import timber.log.Timber
import javax.inject.Inject

internal class DefaultNotificationsService @Inject constructor(
        private val userAccountApi: AccountDataAPI,
        private val userAccountDataDataSource: UserAccountDataDataSource,
        @UserId private val userId: String,
) : NotificationsService {
    override suspend fun getNotificationsContent(): NotificationsContent {
        return withContext(Dispatchers.IO) {
            kotlin.runCatching {
                userAccountApi.getAccountData(userId, UserAccountDataTypes.TYPE_NOTIFICATIONS).toModel<NotificationsContent>()
            }.onFailure {
                Timber.e(it)
            }.getOrNull()
                    ?: runCatching { userAccountDataDataSource.getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATIONS)?.content.toModel<NotificationsContent>() }.getOrNull()
                    ?: NotificationsContent()
        }
    }

    override suspend fun setNotificationsContent(content: NotificationsContent) {
        withContext(Dispatchers.IO) {
            kotlin.runCatching {
                userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_NOTIFICATIONS, content)
            }
        }
    }
}
