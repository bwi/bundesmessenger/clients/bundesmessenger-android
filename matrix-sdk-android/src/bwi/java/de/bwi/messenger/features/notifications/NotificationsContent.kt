/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.notifications

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class ReleaseNotesHintEntry(
        val version: String,
        @Json(name = "show_hint")
        var showHint: Boolean
)

@JsonClass(generateAdapter = true)
internal data class NotificationsContent(
        @Json(name = "should_show_feature_hint_notes_room")
        var shouldShowNotesRoomNotification: Boolean = false,

        @Json(name = "should_show_android_birthday_campaign")
        var shouldShowHappyBirthday: BirthdayCampaign = BirthdayCampaign(),

        @Json(name = "should_show_android_release_notes")
        var releaseNotesEntries: MutableList<ReleaseNotesHintEntry> = mutableListOf(),

        @Json(name = "should_show_analytics_consent_dialog")
        var shouldShowAnalyticsDialog: Boolean = true,

        @Json(name = "should_show_federation_announcement")
        var shouldShowFederationAnnouncementDialog: Boolean = true,

        @Json(name = "should_show_federation_introduction")
        var shouldShowFederationIntroductionDialog: Boolean = true
)

@JsonClass(generateAdapter = true)
internal data class BirthdayCampaign(
        @Json(name = "2022")
        var year2022: Boolean = false,
        @Json(name = "2024")
        var year2024: Boolean = false,
        @Json(name = "2025")
        var year2025: Boolean = true
)
