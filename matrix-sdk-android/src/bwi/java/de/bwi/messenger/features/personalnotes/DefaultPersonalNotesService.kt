/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.personalnotes

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import de.bwi.messenger.features.notifications.NotificationsService
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.model.GuestAccess
import org.matrix.android.sdk.api.session.room.model.RoomDirectoryVisibility
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomParams
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomPreset
import org.matrix.android.sdk.api.session.room.model.tag.RoomTag
import org.matrix.android.sdk.api.util.Optional
import org.matrix.android.sdk.api.util.emptyJsonDict
import org.matrix.android.sdk.internal.di.UserId
import org.matrix.android.sdk.internal.session.room.create.CreateRoomTask
import org.matrix.android.sdk.internal.session.room.tags.AddTagToRoomTask
import org.matrix.android.sdk.internal.session.user.accountdata.AccountDataAPI
import org.matrix.android.sdk.internal.session.user.accountdata.UserAccountDataDataSource
import retrofit2.HttpException
import javax.inject.Inject

internal class DefaultPersonalNotesService @Inject constructor(
        private val userAccountDataDataSource: UserAccountDataDataSource,
        private val userAccountApi: AccountDataAPI,
        private val createRoomTask: CreateRoomTask,
        @UserId private val userId: String,
        private val tagToRoomTask: AddTagToRoomTask,
        private val resources: Resources,
        private val notificationsService: NotificationsService,
) : PersonalNotesService {

    var notesRoomId: String? = null

    override fun getPersonalNotesRoomId(): String? {
        if (notesRoomId == null) {
            notesRoomId = userAccountDataDataSource.getAccountDataEvent(UserAccountDataTypes.TYPE_PERSONAL_NOTES_ROOM)
                    ?.content.toModel<PersonalNotesRoomContent>()?.roomId
        }
        return notesRoomId
    }

    override fun getPersonalNotesRoomIdLive(): LiveData<Optional<String>> {
        return userAccountDataDataSource.getLiveAccountDataEvent(UserAccountDataTypes.TYPE_PERSONAL_NOTES_ROOM).map {
            if (it.getOrNull()?.content?.isNotEmpty() == true) {
                notesRoomId = it.get().content.toModel<PersonalNotesRoomContent>()?.roomId
                Optional.from(notesRoomId)
            } else {
                Optional.empty()
            }
        }
    }

    override suspend fun shouldCreatePersonalNotesRoom(): Boolean {
        return try {
            val data = userAccountApi.getAccountData(userId, UserAccountDataTypes.TYPE_PERSONAL_NOTES_ROOM)
            kotlin.runCatching { data.toModel<PersonalNotesRoomContent>(false) }.isFailure
        } catch (e: Throwable) {
            return e is HttpException && e.code() == 404
        }
    }

    override suspend fun createPersonalNotesRoom(@StringRes titleRes: Int) {
        val createRoomParams = CreateRoomParams()
                .apply {
                    name = resources.getString(titleRes)
                    visibility = RoomDirectoryVisibility.PRIVATE
                    preset = CreateRoomPreset.PRESET_TRUSTED_PRIVATE_CHAT
                    guestAccess = GuestAccess.Forbidden
                    disableFederation = true
                    enableEncryption()
                }
        kotlin.runCatching { createRoomTask.execute(createRoomParams) }.getOrNull()?.let { roomId ->
            userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_PERSONAL_NOTES_ROOM, PersonalNotesRoomContent(roomId))
            tagToRoomTask.execute(AddTagToRoomTask.Params(roomId, RoomTag.ROOM_TAG_PERSONAL_NOTES, 0.0))
            tagToRoomTask.execute(AddTagToRoomTask.Params(roomId, RoomTag.ROOM_TAG_FAVOURITE, 0.0))
        }
    }

    override suspend fun resetPersonalNotesRoom() {
        userAccountApi.setAccountData(userId, UserAccountDataTypes.TYPE_PERSONAL_NOTES_ROOM, emptyJsonDict)
        val content = notificationsService.getNotificationsContent()
        notificationsService.setNotificationsContent(content.apply { shouldShowNotesRoomNotification = true })
    }
}
