/*
 * Copyright 2020 The Matrix.org Foundation C.I.C.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.database

import de.bwi.bwmessenger.features.customheader.CustomHeaderEntityFields
import io.realm.DynamicRealm
import io.realm.RealmMigration

internal object BwiRealmMigration : RealmMigration {

    const val SCHEMA_VERSION = 2L

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        if (oldVersion == 1L) {
            realm.schema.create("CustomHeaderEntity")
                    .addField(CustomHeaderEntityFields.HEADER_NAME, String::class.java)
                    .addPrimaryKey(CustomHeaderEntityFields.HEADER_NAME)
                    .setRequired(CustomHeaderEntityFields.HEADER_NAME, true)
                    .addField(CustomHeaderEntityFields.HEADER_VALUE, String::class.java)
                    .setRequired(CustomHeaderEntityFields.HEADER_VALUE, true)
        }
    }
}
