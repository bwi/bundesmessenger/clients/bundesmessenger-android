/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.database

import com.zhuinden.monarchy.Monarchy
import dagger.Binds
import dagger.Module
import dagger.Provides
import de.bwi.bwmessenger.features.customheader.CustomHeaderService
import de.bwi.bwmessenger.features.customheader.DefaultCustomHeaderService
import de.bwi.messenger.configuration.BwiSDKConfiguration
import de.bwi.messenger.configuration.SDKConfiguration
import de.bwi.messenger.features.federation.BwiFederationService
import de.bwi.messenger.features.federation.DefaultBwiFederationService
import de.bwi.messenger.features.happy_birthday.DefaultHappyBirthdayService
import de.bwi.messenger.features.happy_birthday.HappyBirthdayService
import de.bwi.messenger.features.maintenance.DefaultMaintenanceService
import de.bwi.messenger.features.maintenance.MaintenanceService
import de.bwi.messenger.features.maintenance.api.DefaultMaintenanceApiFactory
import de.bwi.messenger.features.maintenance.api.MaintenanceApiFactory
import de.bwi.messenger.features.notifications.DefaultNotificationsService
import de.bwi.messenger.features.notifications.NotificationsService
import de.bwi.messenger.features.personalnotes.DefaultPersonalNotesService
import de.bwi.messenger.features.personalnotes.PersonalNotesService
import de.bwi.messenger.features.recent_emojis.DefaultRecentEmojisService
import de.bwi.messenger.features.recent_emojis.RecentEmojisService
import de.bwi.messenger.features.release_notes.DefaultReleaseNotesService
import de.bwi.messenger.features.release_notes.ReleaseNotesService
import de.bwi.messenger.features.room_alias.BwiRoomAliasService
import de.bwi.messenger.features.room_alias.DefaultBwiRoomAliasService
import de.bwi.messenger.features.tracking.AnalyticsConsentService
import de.bwi.messenger.features.tracking.DefaultAnalyticsConsentService
import de.bwi.messenger.features.user_restrictions.AccountRestrictionsService
import de.bwi.messenger.features.user_restrictions.DefaultAccountRestrictionsService
import de.bwi.messenger.features.workingtime.DefaultWorkingTimesService
import de.bwi.messenger.features.workingtime.WorkingTimesService
import io.realm.RealmConfiguration
import org.matrix.android.sdk.internal.database.RealmKeysUtils

@Module
internal abstract class BwiModule {

    @Module
    companion object {
        private const val DB_ALIAS = "matrix-sdk-bwi"

        @JvmStatic
        @Provides
        @BwiDatabase
        fun providesMonarchy(@BwiDatabase realmConfiguration: RealmConfiguration): Monarchy {
            return Monarchy.Builder()
                    .setRealmConfiguration(realmConfiguration)
                    .build()
        }

        @JvmStatic
        @Provides
        @BwiDatabase
        internal fun providesRealmConfiguration(realmKeysUtils: RealmKeysUtils): RealmConfiguration {
            // TODO this database should be stored in the user related directory
            return RealmConfiguration.Builder()
                    .apply {
                        realmKeysUtils.configureEncryption(this, DB_ALIAS)
                    }
                    .name("matrix-sdk-bwi.realm")
                    .schemaVersion(BwiRealmMigration.SCHEMA_VERSION)
                    .migration(BwiRealmMigration)
                    .allowWritesOnUiThread(true)
                    .modules(BwiRealmModule())
                    .build() // NOSONAR encryption if configured in realmKeysUtils.configureEncryption
        }

        @JvmStatic
        @Provides
        internal fun provideSDKConfiguration(): SDKConfiguration = BwiSDKConfiguration()
    }

    @Binds
    abstract fun bindWorkingTimeService(service: DefaultWorkingTimesService): WorkingTimesService

    @Binds
    abstract fun bindPersonalNotesRoomService(service: DefaultPersonalNotesService): PersonalNotesService

    @Binds
    abstract fun bindHappyBirthdayService(service: DefaultHappyBirthdayService): HappyBirthdayService

    @Binds
    abstract fun bindRecentEmojisService(service: DefaultRecentEmojisService): RecentEmojisService

    @Binds
    abstract fun bindMaintenanceService(service: DefaultMaintenanceService): MaintenanceService

    @Binds
    abstract fun bindReleaseNotesService(service: DefaultReleaseNotesService): ReleaseNotesService

    @Binds
    abstract fun bindAccountRestrictionsService(service: DefaultAccountRestrictionsService): AccountRestrictionsService

    @Binds
    abstract fun bindMaintenanceApiFactory(factory: DefaultMaintenanceApiFactory): MaintenanceApiFactory

    @Binds
    abstract fun bindTrackingService(service: DefaultAnalyticsConsentService): AnalyticsConsentService

    @Binds
    abstract fun bindNotificationsService(service: DefaultNotificationsService): NotificationsService

    @Binds
    abstract fun bindBwiFederationService(service: DefaultBwiFederationService): BwiFederationService

    @Binds
    abstract fun bindBwiRoomAliasService(service: DefaultBwiRoomAliasService): BwiRoomAliasService

    @Binds
    abstract fun bindCustomHeaderService(service: DefaultCustomHeaderService): CustomHeaderService
}
