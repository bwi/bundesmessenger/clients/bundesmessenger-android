/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.customheader

import de.bwi.bwmessenger.features.customheader.CustomHeader
import de.bwi.bwmessenger.features.customheader.CustomHeaderEntity
import de.bwi.bwmessenger.features.customheader.CustomHeaderEntityFields
import de.bwi.bwmessenger.features.customheader.DefaultCustomHeaderService
import io.mockk.verify
import org.amshove.kluent.internal.assertEquals
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import org.matrix.android.sdk.test.fakes.FakeMonarchy
import org.matrix.android.sdk.test.fakes.givenEqualTo
import org.matrix.android.sdk.test.fakes.givenFindAllResults

class DefaultCustomHeaderServiceTest {

    private val fakeMonarchy = FakeMonarchy()
    @Test
    fun `test initialization`() {
        DefaultCustomHeaderService(fakeMonarchy.instance)
        fakeMonarchy.verifyInsertOrUpdate<CustomHeaderEntity> {
            withArg { actual ->
                actual.headerName shouldBeEqualTo CustomHeader.XVersionId.headerName
                actual.headerValue shouldBeEqualTo "1.24.03"
            }
        }
    }

    @Test
    fun `reading header values`() {
        val realmResult = CustomHeaderEntity(CustomHeader.XVersionId.headerName, "1.24.03")
        val query = fakeMonarchy.givenWhereReturns(realmResult)
                .givenEqualTo(CustomHeaderEntityFields.HEADER_NAME, CustomHeader.XVersionId.headerName)

        System.out.println(query)
        val service = DefaultCustomHeaderService(fakeMonarchy.instance)
        assertEquals("1.24.03", service.getCustomHeaderValue(CustomHeader.XVersionId))
    }

    @Test
    fun `clear all values`() {
        val service = DefaultCustomHeaderService(fakeMonarchy.instance)
        val results = fakeMonarchy.givenWhere<CustomHeaderEntity>().givenFindAllResults()
        service.clearAllCustomHeaderValues()
        verify { results.deleteAllFromRealm() }
    }
}
