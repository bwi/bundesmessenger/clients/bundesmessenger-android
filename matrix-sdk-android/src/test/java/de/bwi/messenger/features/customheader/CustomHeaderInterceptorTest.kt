/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.customheader

import de.bwi.bwmessenger.features.customheader.CustomHeader
import de.bwi.bwmessenger.features.customheader.CustomHeaderInterceptor
import de.bwi.bwmessenger.features.customheader.CustomHeaderService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import okhttp3.Interceptor
import okhttp3.Request
import org.junit.Test

class CustomHeaderInterceptorTest {

    val service = mockk<CustomHeaderService>()
    val interceptor = CustomHeaderInterceptor(service)

    @Test
    fun `test adding custom headers to the request`() {
        every { service.getCustomHeaderValue(any()) } returns "test"
        val chain = mockk<Interceptor.Chain>()
        val request = mockk<Request>()
        every { chain.request() } returns request
        every { chain.proceed(any()) } returns mockk()
        val requestBuilder = mockk<Request.Builder>()
        every { request.newBuilder() } returns requestBuilder
        every { requestBuilder.build() } returns request
        every { requestBuilder.header(any(), any()) } returns requestBuilder
        interceptor.intercept(chain)
        verify { requestBuilder.header(CustomHeader.XVersionId.headerName, "test") }
    }
}
