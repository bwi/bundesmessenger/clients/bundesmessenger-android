/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.workingtime

import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.internal.assertEquals
import org.junit.Before
import org.junit.Test
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataEvent
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.events.model.toContent
import org.matrix.android.sdk.internal.session.user.accountdata.AccountDataAPI
import org.matrix.android.sdk.internal.session.user.accountdata.UserAccountDataDataSource
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalTime

const val USER_ID = "123"
const val ROOM_ID = "345"

@ExperimentalCoroutinesApi
internal class WorkingTimeServiceTest {

    val userAccountDataSource : UserAccountDataDataSource = mockk()
    val accountDataAPI : AccountDataAPI = mockk()

    val workingTimesService = DefaultWorkingTimesService(
            userAccountDataSource,
            accountDataAPI,
            USER_ID
    )

    val sendedContent = slot<WorkingTimeContent>()

    @Before
    fun setUp() {
        sendedContent.clear()
        accountDataAPI.apply {
            coEvery { setAccountData(any(), any(), params = capture(sendedContent)) } returns Unit
        }
        userAccountDataSource.apply {
            every { getAccountDataEvent(any()) } returns null
        }
    }

    @Test
    fun `given no exiting notification times configuration, return default notification`() {
        val defaultEntries = DayOfWeek.values().map { WorkingTimeContentEntry( it != DayOfWeek.SATURDAY && it != DayOfWeek.SUNDAY, 8,0, 17, 0) }
        assertEquals(DayOfWeek.values().associateWith { defaultEntries[it.ordinal].asDomain() }, workingTimesService.getWorkingTimeConfiguration())
        assertEquals(false, workingTimesService.isWorkingTimeEnabled())
    }


    @Test
    fun  `given enabled configuration outside working hours, block notifications`() {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns createEnabledConfigurationForOutsideWorkingTime(listOf(
                    WorkingTimeContentRoom(ROOM_ID, true)
            ))
        }

        assertEquals(true, workingTimesService.shouldWorkingTimeBlockNotification(null))
    }

    @Test
    fun  `given disabled configuration outside working hours, don't block notifications`() {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns createDisabledConfigurationForOutsideWorkingTime()
        }

        assertEquals(false, workingTimesService.shouldWorkingTimeBlockNotification(null))
    }

    @Test
    fun  `given enabled configuration with days off, block notifications`() {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns createDisabledForTodayConfigurationInsideWorkingHours()
        }

        assertEquals(true, workingTimesService.shouldWorkingTimeBlockNotification(null))
    }

    @Test
    fun  `given enabled configuration inside working hours, do not block notifications`() {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns createEnabledConfigurationInsideWorkingHours()
        }

        assertEquals(false, workingTimesService.shouldWorkingTimeBlockNotification(null))
    }

    @Test
    fun  `given enabled configuration inside working hours, do not block notifications for normal rooms`() {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns createEnabledConfigurationInsideWorkingHours()
        }

        assertEquals(false, workingTimesService.shouldWorkingTimeBlockNotification(ROOM_ID, false))
    }

    @Test
    fun  `given the current time outside working hours, don't block notifications for a DM`() {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns createEnabledConfigurationForOutsideWorkingTime()
        }

        assertEquals(false, workingTimesService.shouldWorkingTimeBlockNotification(ROOM_ID, true))
    }

    @Test
    fun  `given the current time outside working hours, block notifications for a DM if it is listed`() {

        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns
                    createEnabledConfigurationForOutsideWorkingTime(listOf(WorkingTimeContentRoom(ROOM_ID, true)))
        }

        assertEquals(true, workingTimesService.shouldWorkingTimeBlockNotification(ROOM_ID, true))
    }

    @Test
    fun  `given the current time outside working hours, don't block notifications for a normal room if it is listed`() {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns
                    createEnabledConfigurationForOutsideWorkingTime(listOf(WorkingTimeContentRoom(ROOM_ID, false)))
        }

        assertEquals(false, workingTimesService.shouldWorkingTimeBlockNotification(ROOM_ID, false))
    }

    @Test
    fun  `given the current time outside working hours, block notifications for a normal room`() {

        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns createEnabledConfigurationForOutsideWorkingTime()
        }

        assertEquals(true, workingTimesService.shouldWorkingTimeBlockNotification(ROOM_ID, false))
    }

    @Test
    fun `test a change to the notification times configuration is saved to the backend`() = runTest {
        val newEntries = workingTimesService.getWorkingTimeConfiguration()
        newEntries[DayOfWeek.SUNDAY]?.isEnabled = true
        newEntries[DayOfWeek.SUNDAY]?.fromTime = LocalTime.of(1, 23)
        newEntries[DayOfWeek.SUNDAY]?.toTime = LocalTime.of(4, 56)
        workingTimesService.setWorkingTimeConfiguration(newEntries)
        val changedEntry = sendedContent.captured.entries[DayOfWeek.SUNDAY.ordinal]
        assertEquals(true, changedEntry.isEnabled)
        assertEquals(1, changedEntry.fromHour)
        assertEquals(23, changedEntry.fromMinute)
        assertEquals(4, changedEntry.toHour)
        assertEquals(56, changedEntry.toMinute)
    }

    @Test
    fun `test enabled global notification times setting is saved to the backend`() = runTest {
        workingTimesService.setWorkingTimeEnabled(true)
        assertEquals(true, sendedContent.captured.isEnabled)
    }

    @Test
    fun `test disabled global notification times setting is saved to the backend`() = runTest {
        workingTimesService.setWorkingTimeEnabled(false)
        assertEquals(false, sendedContent.captured.isEnabled)
    }

    @Test
    fun `given enabling the notification times for a DM, then the room is included in the list with enabled flag set to true`() = runTest {
        workingTimesService.setWorkingTimeEnabledForRoom(ROOM_ID, isEnabled = true, isDirect = true)
        assertEquals(ROOM_ID, sendedContent.captured.rooms[0].roomId)
        assertEquals(true, sendedContent.captured.rooms[0].isEnabled)
    }

    @Test
    fun `given disabling the notification times for a DM, then the room is removed from the list`() = runTest {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns
                    createEnabledConfigurationForOutsideWorkingTime(listOf(WorkingTimeContentRoom(ROOM_ID, true)))
        }
        workingTimesService.setWorkingTimeEnabledForRoom(ROOM_ID, isEnabled = false, isDirect = true)
        assertEquals(true, sendedContent.captured.rooms.isEmpty())
    }

    @Test
    fun `given disabling the notification times for a normal room, then the room is included in the list with enabled flag set to false`() = runTest {
        workingTimesService.setWorkingTimeEnabledForRoom(ROOM_ID, isEnabled = false, isDirect = false)
        assertEquals(ROOM_ID, sendedContent.captured.rooms[0].roomId)
        assertEquals(false, sendedContent.captured.rooms[0].isEnabled)
    }

    @Test
    fun `given enabling the notification times for a normal room, then the room is removed from the list`() = runTest {
        userAccountDataSource.apply {
            every { getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES) } returns
                    createEnabledConfigurationForOutsideWorkingTime(listOf(WorkingTimeContentRoom(ROOM_ID, false)))
        }
        workingTimesService.setWorkingTimeEnabledForRoom(ROOM_ID, isEnabled = true, isDirect = false)
        assertEquals(true, sendedContent.captured.rooms.isEmpty())
    }

    @Test
    fun `test WorkingTimeEntity`() = runTest {
        val wte = WorkingTimeEntity("", 1, 2, 3, 4, 5)
        assertEquals("", wte.userId)
        assertEquals(1, wte.dayOfWeek)
        assertEquals(2, wte.fromHour)
        assertEquals(3, wte.fromMinute)
        assertEquals(4, wte.toHour)
        assertEquals(5, wte.toMinute)
    }

    @Test
    fun `test WorkingTimeRoomEntity`() = runTest {
        val wtre = WorkingTimeRoomEntity("", "", true)
        assert(wtre.userId.isEmpty())
        assert(wtre.roomId.isEmpty())
        assert(wtre.isWorkingTimeEnabled)
    }

    private fun createEnabledConfigurationForOutsideWorkingTime(roomsToInclude: List<WorkingTimeContentRoom> = emptyList()): UserAccountDataEvent {
        val entries = DayOfWeek.values().map {
            WorkingTimeContentEntry(
                    true,
                    LocalTime.now().hour,
                    LocalTime.now().plusMinutes(1).minute,
                    LocalTime.now().plusHours(1).hour,
                    0)
        }
        return UserAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES,
                WorkingTimeContent(isEnabled = true, entries = entries, rooms = roomsToInclude).toContent())
    }

    private fun createEnabledConfigurationInsideWorkingHours(): UserAccountDataEvent {
        val entries = DayOfWeek.values().map {
            WorkingTimeContentEntry(
                    true,
                    LocalTime.now().minusHours(1).hour,
                    0,
                    LocalTime.now().plusHours(1).hour,
                    0)
        }
        return UserAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES,
                WorkingTimeContent(isEnabled = true, entries = entries).toContent())
    }

    private fun createDisabledConfigurationForOutsideWorkingTime(): UserAccountDataEvent {
        val entries = DayOfWeek.values().map {
            WorkingTimeContentEntry(
                    true,
                    LocalTime.now().hour,
                    LocalTime.now().plusMinutes(1).minute,
                    LocalTime.now().plusHours(1).hour,
                    0)
        }
        return UserAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES,
                WorkingTimeContent(isEnabled = false, entries = entries).toContent())
    }

    private fun createDisabledForTodayConfigurationInsideWorkingHours(): UserAccountDataEvent {
        val entries = DayOfWeek.values().map {
            WorkingTimeContentEntry(
                    false,
                    LocalTime.now().minusHours(1).hour,
                    0,
                    LocalTime.now().plusHours(1).hour,
                    0)
        }
        return UserAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATION_TIMES,
                WorkingTimeContent(isEnabled = true, entries = entries).toContent())
    }
}
