/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance.api

import de.bwi.messenger.features.maintenance.MaintenanceService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import org.amshove.kluent.internal.assertFailsWith
import org.junit.Before
import org.junit.Test
import org.threeten.bp.ZonedDateTime
import java.io.IOException

class BlockingMaintenanceInterceptorTest {

    private val now = ZonedDateTime.now()
    private val maintenanceService: MaintenanceService = mockk()
    private val chain: Interceptor.Chain = mockk()
    private val request: Request = mockk()
    private val url: HttpUrl = mockk()
    private val underTest = BlockingMaintenanceInterceptor(maintenanceService)

    @Before
    fun setUp() {
        every { url.encodedPath } returns "url"
        every { request.url } returns url
        every { chain.request() } returns request
        every { chain.proceed(request) } returns mockk()
        every { maintenanceService.getCurrentDowntimeInfo() } returns DowntimeInfo(
                startTime = now.minusHours(1),
                endTime = now.plusHours(1),
                warningStartTime = now.minusHours(2),
                type = "MAINTENANCE",
                description = "description",
                blocking = true,
        )
    }

    @Test
    fun `proceed with request`() {
        every { maintenanceService.shouldBlockOnBlockingDowntime() } returns false
        underTest.intercept(chain)
        verify(exactly = 1) { chain.proceed(request) }
    }

    @Test
    fun `block request`() {
        every { maintenanceService.shouldBlockOnBlockingDowntime() } returns true
        assertFailsWith(IOException::class) {
            underTest.intercept(chain)
        }
        verify(exactly = 0) { chain.proceed(request) }
    }
}
