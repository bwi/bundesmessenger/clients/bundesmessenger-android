/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.customheader

import de.bwi.bwmessenger.features.customheader.CustomHeaderEntityFields
import de.bwi.messenger.database.BwiRealmMigration
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.realm.DynamicRealm
import io.realm.RealmObjectSchema
import io.realm.RealmSchema
import org.junit.Test

class DBMigrationTest {
    @Test
    fun `test DB migration for custom headers`() {
        val realm = mockk<DynamicRealm>()
        val schema = mockk<RealmSchema>()
        val objSchema = mockk<RealmObjectSchema>()
        every { realm.schema } returns schema
        every { schema.create(any()) } returns objSchema
        every { objSchema.addField(any(), any()) } returns objSchema
        every { objSchema.addPrimaryKey(any()) } returns objSchema
        every { objSchema.setRequired(any(), any()) } returns objSchema
        BwiRealmMigration.migrate(realm, 1, 2)
        BwiRealmMigration.migrate(realm, 2, 3)
        verify(exactly = 1) { schema.create("CustomHeaderEntity") }
        verify(exactly = 1) { objSchema.addField(CustomHeaderEntityFields.HEADER_NAME, String::class.java) }
        verify(exactly = 1) { objSchema.addPrimaryKey(CustomHeaderEntityFields.HEADER_NAME) }
        verify(exactly = 1) { objSchema.setRequired(CustomHeaderEntityFields.HEADER_NAME, true) }
        verify(exactly = 1) { objSchema.addField(CustomHeaderEntityFields.HEADER_VALUE, String::class.java) }
        verify(exactly = 1) { objSchema.setRequired(CustomHeaderEntityFields.HEADER_VALUE, true) }
    }
}
