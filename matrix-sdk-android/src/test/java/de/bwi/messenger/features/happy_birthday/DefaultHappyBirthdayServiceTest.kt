/*
 * Copyright (c) 2025 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.happy_birthday

import de.bwi.messenger.features.notifications.BirthdayCampaign
import de.bwi.messenger.features.notifications.DefaultNotificationsService
import de.bwi.messenger.features.notifications.NotificationsContent
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.internal.assertEquals
import org.junit.Test

class DefaultHappyBirthdayServiceTest {
    private val defaultNotificationsService: DefaultNotificationsService = mockk()
    private val defaultHappyBirthdayService: DefaultHappyBirthdayService = DefaultHappyBirthdayService(defaultNotificationsService)

    @Test
    fun `should show Happy Birthday in 2025`() = runTest {
        coEvery {
            defaultNotificationsService.getNotificationsContent().shouldShowHappyBirthday
        }.returns(BirthdayCampaign())

        val result = defaultHappyBirthdayService.shouldShowHappyBirthday(2025)
        assertEquals(true, result)
    }

    @Test
    fun `should not show Happy Birthday in 2024`() = runTest {
        coEvery {
            defaultNotificationsService.getNotificationsContent().shouldShowHappyBirthday
        }.returns(BirthdayCampaign())

        val result = defaultHappyBirthdayService.shouldShowHappyBirthday(2024)
        assertEquals(false, result)
    }

    @Test
    fun `is setNotification in defaultNotificationsService called`() {
        val notificationContent = NotificationsContent(
                shouldShowHappyBirthday = BirthdayCampaign(year2025 = false)
        )
        runTest {
            coEvery {
                defaultNotificationsService.getNotificationsContent()
            }.returns(NotificationsContent())

            coEvery {
                defaultNotificationsService.setNotificationsContent(notificationContent)
            }.returns(Unit)

            defaultHappyBirthdayService.setShouldShowHappyBirthday(false, 2025)

            coVerify {
                defaultNotificationsService.setNotificationsContent(notificationContent)
            }
        }
    }
}
