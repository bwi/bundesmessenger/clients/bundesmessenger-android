/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.federation

import de.bwi.messenger.features.room_alias.BwiRoomAliasService
import de.bwi.messenger.features.room_alias.DefaultBwiRoomAliasService
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.internal.assertEquals
import org.junit.Test
import org.matrix.android.sdk.api.session.events.model.Event
import org.matrix.android.sdk.api.session.events.model.toContent
import org.matrix.android.sdk.api.session.room.RoomService
import org.matrix.android.sdk.api.session.room.alias.AliasService
import org.matrix.android.sdk.api.session.room.model.RoomServerAclContent
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.state.StateService

@ExperimentalCoroutinesApi
class DefaultBwiFederationServiceTest {
    private val federatedRoomSummary = RoomSummary("federated", encryptionEventTs = null, isEncrypted = true, typingUsers = emptyList())
    private val notFederatedRoomSummary = RoomSummary("not_federated", encryptionEventTs = null, isEncrypted = true, typingUsers = emptyList())
    private val notSetfederatedRoomSummary = RoomSummary("not_set", encryptionEventTs = null, isEncrypted = true, typingUsers = emptyList())
    private val federatedDmRoomSummary = RoomSummary("dm", encryptionEventTs = null, isEncrypted = true, typingUsers = emptyList(), isDirect = true, directUserId = "@user:otherhost.com")
    private val notFederatedDmRoomSummary = RoomSummary("dm", encryptionEventTs = null, isEncrypted = true, typingUsers = emptyList(), isDirect = true, directUserId = "@user:myhost.com")
    private val federatedDmNoUserIdRoomSummary = RoomSummary("dm", encryptionEventTs = null, isEncrypted = true, typingUsers = emptyList(), isDirect = true, directUserId = null)
    private val notFederatedDmNoUserIdRoomSummary = RoomSummary("dm", encryptionEventTs = null, isEncrypted = true, typingUsers = emptyList(), isDirect = true, directUserId = null)
    private val nullRoomSummary = RoomSummary("null", encryptionEventTs = null, isEncrypted = true, typingUsers = emptyList(), isDirect = true, directUserId = null)
    private val allowListAllServer = listOf(RoomServerAclContent.ALL)
    private val allowListOnlyMyHomeServer = listOf("myhost.com")

    private val aliasService = mockk<AliasService> {
        coEvery { getRoomAliases() } returns emptyList()
        coEvery { addAlias(any()) } returns Unit
    }

    private val stateService = mockk<StateService>() {
        every { getStateEvent(any(), any()) } returns null
        coEvery { sendStateEvent(any(), any(), any()) } returns "event-id"
    }

    private val roomService = mockk<RoomService> {
        every { getRoom("null") } returns null

        every { getRoom("federated")  } returns mockk {
            every { roomSummary()?.name } returns "test"
            every { aliasService() } returns aliasService
            every { stateService() } returns mockk {
                every { getStateEvent(any(), any()) } returns Event(content = RoomServerAclContent(allowList = allowListAllServer).toContent())
            }
        }
        every { getRoom("alias_test")  } returns mockk {
            every { roomSummary()?.name } returns "test"
            every { aliasService() } returns aliasService
            every { roomSummary() } returns mockk {
                every { name } returns "alias_test"
            }
            every { stateService() } returns mockk {
                every { getStateEvent(any(), any()) } returns Event(content = RoomServerAclContent(allowList = allowListAllServer).toContent())
                coEvery { sendStateEvent(any(), any(), any()) } returns "event-id"
            }
        }
        every { getRoom("not_federated")  } returns mockk {
            every { roomSummary()?.name } returns "test"
            every { aliasService() } returns aliasService
            every { stateService() } returns mockk {
                every { getStateEvent(any(), any()) } returns Event(content = RoomServerAclContent(allowList = allowListOnlyMyHomeServer).toContent())
            }
        }
        every { getRoom("not_set")  } returns mockk {
            every { roomSummary()?.name } returns "test"
            every { aliasService() } returns aliasService
            every { stateService() } returns stateService
        }
    }

    private val bwiRoomAliasService = mockk<BwiRoomAliasService> {
        every { getAliasForRoomName(any()) } returns "alias"
    }

    private val bwiFederationService = DefaultBwiFederationService(roomService, bwiRoomAliasService, "@test:myhost.com")

    @Test
    fun `test if room is federated`() {
        assertEquals(true, bwiFederationService.isRoomFederated(federatedRoomSummary))
        assertEquals(false, bwiFederationService.isRoomFederated(notFederatedRoomSummary))
        assertEquals(true, bwiFederationService.isRoomFederated(notSetfederatedRoomSummary))
        assertEquals(true, bwiFederationService.isFederationSetForRoom(federatedRoomSummary.roomId))
        assertEquals(true, bwiFederationService.isFederationSetForRoom(notFederatedRoomSummary.roomId))
        assertEquals(false, bwiFederationService.isFederationSetForRoom(notSetfederatedRoomSummary.roomId))
        assertEquals(false, bwiFederationService.isFederationSetForRoom(nullRoomSummary.roomId))
        assertEquals(true, bwiFederationService.isRoomFederated(federatedDmRoomSummary))
        assertEquals(false, bwiFederationService.isRoomFederated(notFederatedDmRoomSummary))
        assertEquals(false, bwiFederationService.isRoomFederated(notFederatedDmNoUserIdRoomSummary))
        assertEquals(false, bwiFederationService.isRoomFederated(federatedDmNoUserIdRoomSummary))
        assertEquals(false, bwiFederationService.isRoomFederated(nullRoomSummary))
    }

    @Test
    fun `test change federation state of room`() = runTest {
        bwiFederationService.setFederationForRoom("not_set", true)
        coVerify { stateService.sendStateEvent(any(), any(),
                body = RoomServerAclContent(allowIpLiterals = false, allowList = allowListAllServer, denyList = null).toContent()) }
        bwiFederationService.setFederationForRoom("not_set", false)
        coVerify { stateService.sendStateEvent(
                any(), any(),
                body = RoomServerAclContent(allowIpLiterals = false, allowList = allowListOnlyMyHomeServer, denyList = null).toContent()) }
    }

    @Test
    fun `test set room alias on federation`() = runTest {
        // don't set an alias if federation disabled
        bwiFederationService.setFederationForRoom("alias_test", false)
        coVerify(exactly = 0) { aliasService.addAlias(any()) }

        // set an alias, if no aliases exist and the federation is enabled
        bwiFederationService.setFederationForRoom("alias_test", true)
        coVerify(exactly = 1) { bwiRoomAliasService.getAliasForRoomName("alias_test") }
        coVerify(exactly = 1) { aliasService.addAlias(any()) }


        // if an alias already exists, don't set an alias again
        coEvery { aliasService.getRoomAliases() } returns listOf("alias")
        bwiFederationService.setFederationForRoom("alias_test", true)
        coVerify(exactly = 1) { bwiRoomAliasService.getAliasForRoomName("alias_test") }
        coVerify(exactly = 1) { aliasService.addAlias(any()) }
    }

    @Test
    fun `test alias service`() {
        val aliasService = DefaultBwiRoomAliasService()
        assert(aliasService.getAliasForRoomName(null).isNotEmpty())
        assert(aliasService.getAliasForRoomName("test").startsWith("test"))
    }
}
