/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.maintenance

import de.bwi.messenger.features.maintenance.api.BwiVersionsInfo
import de.bwi.messenger.features.maintenance.api.DowntimeInfo
import de.bwi.messenger.features.maintenance.api.MaintenanceApi
import de.bwi.messenger.features.maintenance.api.MaintenanceApiFactory
import de.bwi.messenger.features.maintenance.api.MaintenanceResponse
import de.bwi.messenger.features.maintenance.api.VersionsInfo
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldNotBe
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.threeten.bp.ZonedDateTime

class DefaultMaintenanceServiceTest {

    private val maintenanceApiFactory: MaintenanceApiFactory = mockk()
    private val maintenanceApi: MaintenanceApi = mockk()
    private val underTest = DefaultMaintenanceService(maintenanceApiFactory)
    private val now = ZonedDateTime.now()

    @Before
    fun setUp() {
        every { maintenanceApiFactory.createMaintenanceApi(homeServerUrl) } returns maintenanceApi
    }

    @After
    fun tearDown() {
        DefaultMaintenanceService.invalidateCache()
    }

    @Test
    fun `valid maintenance`() {
        runBlocking {
            coEvery { maintenanceApi.getMaintenanceData() } returns MaintenanceResponse(
                    downtime = listOf(
                            DowntimeInfo(
                                    startTime = now.minusHours(1),
                                    endTime = now.plusHours(1),
                                    warningStartTime = now.minusHours(2),
                                    type = "MAINTENANCE",
                                    description = "description",
                                    blocking = true,
                            ),
                    ),
                    versionsInfo = BwiVersionsInfo(
                            versions = VersionsInfo("2.0.0", "3.0 0")
                    ),
            )
            underTest.fetchMaintenanceData(homeServerUrl)
            underTest.getCurrentDowntimeInfo() shouldNotBe null
        }
    }

    @Test
    fun `valid info message`() {
        runBlocking {
            coEvery { maintenanceApi.getMaintenanceData() } returns MaintenanceResponse(
                    downtime = listOf(
                            DowntimeInfo(
                                    startTime = now.minusDays(1),
                                    endTime = now.plusHours(1),
                                    warningStartTime = now.minusDays(2),
                                    type = "ADHOC_MESSAGE",
                                    description = "description",
                                    blocking = true,
                            ),
                    ),
                    versionsInfo = null,
            )
            underTest.fetchMaintenanceData(homeServerUrl)
            underTest.getCurrentDowntimeInfo() shouldNotBe null
            underTest.getCurrentDowntimeInfo()?.isActive() shouldBe true
            underTest.getCurrentDowntimeInfo()?.isStartAndEndOnSameDay() shouldBe false
        }
    }

    @Test
    fun `invalid type`(): Unit = runBlocking {
        coEvery { maintenanceApi.getMaintenanceData() } returns MaintenanceResponse(
                downtime = listOf(
                        DowntimeInfo(
                                startTime = now.minusHours(1),
                                endTime = now.plusHours(1),
                                warningStartTime = now,
                                type = "invalid",
                                description = "description",
                                blocking = true,
                        ),
                ),
                versionsInfo = null,
        )
        underTest.fetchMaintenanceData(homeServerUrl)
        underTest.getCurrentDowntimeInfo() shouldBe null
    }

    @Test
    fun `parse error`(): Unit = runBlocking {
        coEvery { maintenanceApi.getMaintenanceData() } throws Exception("parse error")
        underTest.fetchMaintenanceData(homeServerUrl)
        underTest.getCurrentDowntimeInfo() shouldBe null
    }

    @Test
    fun `warning start time greater than start time`() {
        runBlocking {
            coEvery { maintenanceApi.getMaintenanceData() } returns MaintenanceResponse(
                    downtime = listOf(
                            DowntimeInfo(
                                    startTime = now.minusHours(1),
                                    endTime = now.plusHours(1),
                                    warningStartTime = now,
                                    type = "MAINTENANCE",
                                    description = "description",
                                    blocking = true,
                            ),
                    ),
                    versionsInfo = null,
            )
            underTest.fetchMaintenanceData(homeServerUrl)
            underTest.getCurrentDowntimeInfo() shouldBe null
        }
    }

    @Test
    fun `start time greater than end time`() {
        runBlocking {
            coEvery { maintenanceApi.getMaintenanceData() } returns MaintenanceResponse(
                    downtime = listOf(
                            DowntimeInfo(
                                    startTime = now.plusHours(2),
                                    endTime = now.plusHours(1),
                                    warningStartTime = now.minusHours(2),
                                    type = "MAINTENANCE",
                                    description = "description",
                                    blocking = true,
                            ),
                    ),
                    versionsInfo = null,
            )
            underTest.fetchMaintenanceData(homeServerUrl)
            val downTime = underTest.getCurrentDowntimeInfo()
            downTime shouldBe null
        }
    }

    @Test
    fun `adhoc message with no description`() {
        runBlocking {
            coEvery { maintenanceApi.getMaintenanceData() } returns MaintenanceResponse(
                    downtime = listOf(
                            DowntimeInfo(
                                    startTime = now.minusHours(1),
                                    endTime = now.plusHours(1),
                                    warningStartTime = now.minusHours(2),
                                    type = "ADHOC_MESSAGE",
                                    description = null,
                                    blocking = true,
                            ),
                    ),
                    versionsInfo = null,
            )
            underTest.fetchMaintenanceData(homeServerUrl)
            val downTime = underTest.getCurrentDowntimeInfo()
            downTime shouldBe null
        }
    }

    @Test
    fun `adhoc message with empty description`() {
        runBlocking {
            coEvery { maintenanceApi.getMaintenanceData() } returns MaintenanceResponse(
                    downtime = listOf(
                            DowntimeInfo(
                                    startTime = now.minusHours(1),
                                    endTime = now.plusHours(1),
                                    warningStartTime = now.minusHours(2),
                                    type = "ADHOC_MESSAGE",
                                    description = "",
                                    blocking = true,
                            ),
                    ),
                    versionsInfo = null,
            )
            underTest.fetchMaintenanceData(homeServerUrl)
            underTest.getCurrentDowntimeInfo() shouldBe null
        }
    }

    @Test
    fun `adhoc message with blank description`() {
        runBlocking {
            coEvery { maintenanceApi.getMaintenanceData() } returns MaintenanceResponse(
                    downtime = listOf(
                            DowntimeInfo(
                                    startTime = now.minusHours(1),
                                    endTime = now.plusHours(1),
                                    warningStartTime = now.minusHours(2),
                                    type = "ADHOC_MESSAGE",
                                    description = " ",
                                    blocking = true,
                            ),
                    ),
                    versionsInfo = null,
            )
            underTest.fetchMaintenanceData(homeServerUrl)
            underTest.getCurrentDowntimeInfo() shouldBe null
        }
    }

    @Test
    fun `check version info`() {
        val vi = VersionsInfo("2.0.0", "3.0 0")
        assert(vi.isOutdated("2.0.0") == false)
        assert(vi.isOutdated("1.5.3") == true)
        assert(vi.isOutdated("2.1.0") == false)

        assert(vi.shouldUpdate("3.0.0") == false)
        assert(vi.shouldUpdate("3.5.3") == false)
        assert(vi.shouldUpdate("2.1.0") == true)
    }
}

private const val homeServerUrl = "URL"
