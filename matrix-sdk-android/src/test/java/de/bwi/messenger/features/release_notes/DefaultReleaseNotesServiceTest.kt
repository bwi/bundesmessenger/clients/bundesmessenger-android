/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.bwi.messenger.features.release_notes

import de.bwi.messenger.features.notifications.DefaultNotificationsService
import de.bwi.messenger.features.notifications.NotificationsContent
import de.bwi.messenger.features.notifications.ReleaseNotesHintEntry
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.amshove.kluent.internal.assertEquals
import org.junit.Before
import org.junit.Test
import org.matrix.android.sdk.api.session.accountdata.UserAccountDataTypes
import org.matrix.android.sdk.api.session.events.model.toContent
import org.matrix.android.sdk.internal.session.user.accountdata.AccountDataAPI
import org.matrix.android.sdk.internal.session.user.accountdata.UserAccountDataDataSource

@OptIn(ExperimentalCoroutinesApi::class)
class DefaultReleaseNotesServiceTest {

    private val accountAPI = mockk<AccountDataAPI>()
    private val accountDataSource = mockk<UserAccountDataDataSource>()
    private val notificationsService = DefaultNotificationsService(accountAPI, accountDataSource, "userId")
    private val releaseNotesService = DefaultReleaseNotesService(notificationsService)
    private val sendedContent = slot<NotificationsContent>()

    @Before
    fun setUp() {
        sendedContent.clear()
        accountAPI.apply {
            coEvery { setAccountData(any(), any(), params = capture(sendedContent)) } returns Unit
        }
    }

    @Test
    fun `show release notes for new version`() = runTest {
        var content = NotificationsContent().toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content
        assertEquals(true, releaseNotesService.shouldShowReleaseNotesHint("2.0.0"))

        // test with API error
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } throws Exception()
        assertEquals(true, releaseNotesService.shouldShowReleaseNotesHint("2.0.0"))

        coVerify(exactly = 1) { accountDataSource.getAccountDataEvent(UserAccountDataTypes.TYPE_NOTIFICATIONS) }

        // test with existing data
        content = NotificationsContent(releaseNotesEntries = mutableListOf(
                ReleaseNotesHintEntry(
                        version = "1.0",
                        showHint = false
                ))).toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content

        assertEquals(true, releaseNotesService.shouldShowReleaseNotesHint("2.0.0"))
    }

    @Test
    fun `don't show release notes if already shown`() = runTest {
        val content = NotificationsContent(releaseNotesEntries = mutableListOf(
                        ReleaseNotesHintEntry(
                                version = "2.0",
                                showHint = false
                        ))).toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content
        assertEquals(false, releaseNotesService.shouldShowReleaseNotesHint("2.0.1"))
    }

    @Test
    fun `save release notes hint structure`() = runTest {
        var content = NotificationsContent().toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content

        releaseNotesService.setShouldShowReleaseNotesHint("2.0.1", false)

        assertEquals(1, sendedContent.captured.releaseNotesEntries.size)
        assertEquals("2.0", sendedContent.captured.releaseNotesEntries[0].version)
        assertEquals(false, sendedContent.captured.releaseNotesEntries[0].showHint)

        content = NotificationsContent(releaseNotesEntries = mutableListOf(
                ReleaseNotesHintEntry(
                        version = "2.0",
                        showHint = false
                ))).toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content
        sendedContent.clear()
        releaseNotesService.setShouldShowReleaseNotesHint("2.0.1", true)
        assertEquals(1, sendedContent.captured.releaseNotesEntries.size)
        assertEquals("2.0", sendedContent.captured.releaseNotesEntries[0].version)
        assertEquals(true, sendedContent.captured.releaseNotesEntries[0].showHint)
    }

    @Test
    fun `initial value for federation announcement is true by default`() = runTest {
        val content = NotificationsContent().toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content

        assertEquals(true, releaseNotesService.shouldShowFederationAnnouncement())
    }

    @Test
    fun `save federation announcement new value`() = runTest {
        val content = NotificationsContent().toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content
        releaseNotesService.setShouldShowFederationAnnouncement(false)
        assertEquals(false, sendedContent.captured.shouldShowFederationAnnouncementDialog)
    }

    @Test
    fun `initial value for federation introduction is true by default`() = runTest {
        val content = NotificationsContent().toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content

        assertEquals(true, releaseNotesService.shouldShowFederationIntroduction())
    }

    @Test
    fun `save federation introduction new value`() = runTest {
        val content = NotificationsContent().toContent()
        coEvery { accountAPI.getAccountData("userId", UserAccountDataTypes.TYPE_NOTIFICATIONS) } returns content
        releaseNotesService.setShouldShowFederationIntroduction(false)
        assertEquals(false, sendedContent.captured.shouldShowFederationIntroductionDialog)
    }
}
